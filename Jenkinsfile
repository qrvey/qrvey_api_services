pipeline {
  agent {
    docker {
      image 'node:erbium-alpine'
    }
  }
  triggers {
    cron('0 9 * * *')
  }
  environment {
    NPM_CONFIG_CACHE = "${WORKSPACE}/.npm"
    
    NODE_ENV='development'
    MYSQL_DEFAULT_PORT = '3306'
    MSSQL_DEFAULT_PORT = '1433'
    ORACLE_DEFAULT_PORT = '1521'

    SERVER_PREFIX='adminqa'
    AWS_DEFAULT_REGION=credentials('adminqa_AWS_DEFAULT_REGION')

    AWS_ACCESS_KEY_ID=credentials('adminqa_AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY=credentials('adminqa_AWS_SECRET_ACCESS_KEY')

    GENERAL_COMPOSER_USER_PASSWORD=credentials('GENERAL_COMPOSER_USER_PASSWORD')
    GENERAL_COMPOSER_USER_EMAIL=credentials('GENERAL_COMPOSER_USER_EMAIL')

    GENERAL_USER_ID=credentials('adminqa_GENERAL_USER_ID')
    COMPOSER_DOMAIN=credentials('adminqa_COMPOSER_DOMAIN')
    COMPOSER_API_KEY=credentials('GENERAL_API_KEY')
    GENERAL_API_KEY=credentials('GENERAL_API_KEY')

    ADMIN_DOMAIN=credentials('adminqa_ADMIN_DOMAIN')
    ADMIN_BACKEND_DOMAIN=credentials('adminqa_ADMIN_BACKEND_DOMAIN')
    ADMIN_USER=credentials('adminqa_ADMIN_USER')
    ADMIN_PASSWORD=credentials('adminqa_ADMIN_PASSWORD')

    MYSQL_HOST=credentials('GENERAL_MYSQL_HOST')
    MYSQL_USER=credentials('GENERAL_MYSQL_USER')
    MYSQL_PASSWORD=credentials('GENERAL_MYSQL_PASSWORD')
    MYSQL_GENERAL_DATABASE_NAME=credentials('MYSQL_GENERAL_DATABASE_NAME')

    MSSQL_HOST=credentials('GENERAL_MSSQL_HOST')
    MSSQL_USER=credentials('GENERAL_MSSQL_USER')
    MSSQL_PASSWORD=credentials('GENERAL_MSSQL_PASSWORD')
    MSSQL_GENERAL_DATABASE_NAME=credentials('MSSQL_GENERAL_DATABASE_NAME')

    ORACLE_HOST=credentials('GENERAL_ORACLE_HOST')
    ORACLE_USER=credentials('GENERAL_ORACLE_USER')
    ORACLE_SERVICE_NAME=credentials('GENERAL_ORACLE_SERVICE_NAME')
    ORACLE_PASSWORD=credentials('GENERAL_ORACLE_PASSWORD')
    ORACLE_GENERAL_DATABASE_NAME=credentials('ORACLE_GENERAL_DATABASE_NAME')

    ATHENA_USER=credentials('GENERAL_ATHENA_USER')
    ATHENA_PASSWORD=credentials('GENERAL_ATHENA_PASSWORD')
    ATHENA_GENERAL_CATALOG_NAME=credentials('ATHENA_GENERAL_CATALOG_NAME')
    ATHENA_GENERAL_DATABASE=credentials('ATHENA_GENERAL_DATABASE')
    ATHENA_GENERAL_OUTPUT_LOCATION=credentials('ATHENA_GENERAL_OUTPUT_LOCATION')
    ATHENA_GENERAL_REGION=credentials('ATHENA_GENERAL_REGION')
    ATHENA_GENERAL_WORKGROUP=credentials('ATHENA_GENERAL_WORKGROUP')
  }

  stages {
    stage('Test') {
      steps {
        sh 'node --version'
        sh 'npm install'
        sh 'npm run test:integration:coverage:all'
      }
    }
  }

  post {
        always {
            junit 'reports/**/*.xml'
        }
    }
}