'use strict';

const { URLSearchParams } = require('url');
const { axios, getRequestOptions } = require('./helpers/axiosHelper');

/**
 * Create user
 * @param {Object} userDetails User details
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6Mjc1MTMwNTU-create-user | Create user API}
 * @returns {Promise<Object>} Returns an user 
 */
const createUser = async (userDetails, params) => {
    const url = '/devapi/v4/core/user';
    let options = getRequestOptions(params);

    return axios.post(url, userDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete user by user Id
 * @param {string} userId User identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjE-get-user | Get user API}
 * @returns {Promise<Object>} Returns an user 
 */
const deleteUser = async (userId, params) => {
    const url = `/devapi/v4/core/user/${userId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get user by user Id
 * @param {string} userId User identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjE-get-user | Get user API}
 * @returns {Promise<Object>} Returns an user 
 */
const getUser = async (userId, params) => {
    const url = `/devapi/v4/core/user/${userId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * User authentication using email and password
 * @param {Object} data Crendentails
 * @param {string} data.email User email
 * @param {string} data.password User password
 * @returns {Promise<Object>} Returns an authentication details object
 */
const userLogin = async (data) => {
    const url = '/devapi/v4/core/login';

    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    };

    let body = new URLSearchParams(data);
    return axios.post(url, body.toString(), { headers })
        .then(response => response.data)
        .catch(error => {
            return Promise.reject(error.response.data);
        });
};

/**
 * Generate login token
 * @param {Object} data Crendentails
 * @param {string} data.userid User indentifier
 * @param {string} data.expirationIn Expiration time 5s, 1m, 2h
 * @returns {Promise<Object>} 
 */
const generateLoginToken = async (data, params) => {
    const url = '/devapi/v4/core/login/token';

    let options = getRequestOptions(params);

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => {
            return Promise.reject(error.response.data);
        });
};

/**
 * Generate login token
 * @param {Object} data Crendentails
 * @param {string} data.userid User indentifier
 * @param {string} data.expirationIn Expiration time 5s, 1m, 2h
 * @returns {Promise<Object>} 
 */
const apiLogin = async (data, params) => {
    const url = '/devapi/v4/core/login/token/session';

    let options = getRequestOptions(params);

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => {
            return Promise.reject(error.response.data);
        });
};

/**
 * Verify composer email
 * @param {Object} data Crendentails
 * @param {string} data.email User indentifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} 
 */
const verifyEmails = async (data, params) => {
    const url = '/devapi/v4/core/user/email/verify';

    let options = getRequestOptions(params);

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => {
            return Promise.reject(error.response.data);
        });
};

const qrveyLogin = async (userId, body, options = { expiresIn: '5s' }) => {
    let { token } = await generateLoginToken({
        userid: userId,
        expiresIn: '5s',
    });

    let params = {
        headers: {
            Authorization: `jwt ${token}`,
        },
    };

    return apiLogin(body, params);
};

module.exports = {
    apiLogin,
    createUser,
    deleteUser,
    generateLoginToken,
    getUser,
    userLogin,
    qrveyLogin,
    verifyEmails,
};