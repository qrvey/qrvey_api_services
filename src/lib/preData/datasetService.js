'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_datasources_proxyApiGateway`;

/**
 * This service will allow the user to apply changes made in a dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2OQ-apply-changes | Apply dataset changes API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const applyDatasetChanges = async (userId, appId, datasetId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/apply`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, {}, options);
    }

    return axios.post(url, {}, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Create dataset using connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} body Request body
 * @param {string} body.connectionId Target connection identifier
 * @param {string} [body.tableName] Table name
 * @param {string} [body.database] Database name
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns a dataset item
 */
const createDatasetFromDataSource = async (userId, appId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/datasource`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Create dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} datasetDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ1OQ-create-a-dataset | Create dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const createDataset = async (userId, appId, datasetDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(datasetDetails)) ? {} : datasetDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Create dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} connectionId Parent connection id associated to the shared dataset.
 * @param {Object} body Request body
 * @param {string} [body.appId] App id associated to the shared dataset
 * @param {string} [body.userId] User id associated to the shared dataset
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ1OQ-create-a-dataset | Create dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const createDatasetView = async (userId, appId, connectionId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/clone/connection/${connectionId}`;

    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => {
            return Promise.reject(error.response.data)
        });
};

/**
 * Delete dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2NA-delete-a-dataset | Delete dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const deleteDataset = async (userId, appId, datasetId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get datasets
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} [body] Query option
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2NQ-get-all-datasets | Get dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const getDatasets = async (userId, appId, body = {}, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get datasets
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Mg-get-a-dataset | Get dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const getDataset = async (userId, appId, datasetId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * This service will let you load an existing dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Ng-load-dataset | Load dataset API}
 * @returns {Promise<Object>} Returns { ok:true } if the loading process was sucessull
 */
const loadDataset = async (userId, appId, datasetId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/load`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, {}, options);
    }

    return axios.post(url, {}, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get loading status
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Ng-load-dataset | Load dataset API}
 * @returns {Promise<Object>} Returns { ok:true } if the loading process was sucessull
 */
const getLoadingStatus = async (userId, appId, datasetId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/loading/status`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, {}, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Publish dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} datasetDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Mw-update-a-dataset | Update dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const updatePublishedDatasetVersion = async (userId, appId, datasetId, datasetDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/publishVersion`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, datasetDetails, options);
    }

    return axios.put(url, datasetDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update dataset
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} datasetDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Mw-update-a-dataset | Update dataset API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const updateDataset = async (userId, appId, datasetId, datasetDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, datasetDetails, options);
    }

    return axios.put(url, datasetDetails, options)
        .then(response => {
            return response.data
        })
        .catch(error => {
            return Promise.reject(error.response.data)
        });
};

module.exports = {
    applyDatasetChanges,
    createDatasetFromDataSource,
    createDatasetView,
    createDataset,
    deleteDataset,
    getDatasets,
    getDataset,
    getLoadingStatus,
    loadDataset,
    updateDataset,
    updatePublishedDatasetVersion,
};
