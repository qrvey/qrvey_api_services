'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_database-connection_proxyApiGateway`;

/**
 * Update connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} connectionDetails Connection details
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQyNg-create-connection | Update connection API}
 * @returns {Promise<Object>} Returns a connection item
 */
const createConnection = async (userId, appId, connectionDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(connectionDetails)) ? {} : connectionDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} connectionId Connection identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQyOQ-delete-connection | Delete connection API}
 * @returns {Promise<Object>} Returns a connection item
 */
const deleteConnection = async (userId, appId, connectionId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections/${connectionId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} connectionId Connection identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDM3Mw-get-connection | Get connection API}
 * @returns {Promise<Object>} Returns a connection item
 */
const getConnection = async (userId, appId, connectionId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections/${connectionId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get connections
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} body Request body
 * @param {number} [body.limit] Limit of records
 * @param {Boolean} [body.loadAll] Load all datasets
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQzMA-get-all-connections | Get connections API}
 * @returns {Promise<Object>} Returns a connection list item
 */
const getConnections = async (userId, appId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Test existing connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} connectionId Connection identifier
 * @param {Object} connectionDetails Connection details
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQzMQ-test-existing-connection | Test connection API}
 * @returns {Promise<Object>} Returns a connection item
 */
const testConnectionByConnectionId = async (userId, appId, connectionId, connectionDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections/${connectionId}/databases/test`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(connectionDetails)) ? {} : connectionDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update connection
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} connectionId Connection identifier
 * @param {Object} connectionDetails Connection details
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQyNw-update-connection | Get connection API}
 * @returns {Promise<Object>} Returns a connection item
 */
const updateConnection = async (userId, appId, connectionId, connectionDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/connections/${connectionId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, connectionDetails, options);
    }

    return axios.put(url, connectionDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createConnection,
    deleteConnection,
    getConnections,
    getConnection,
    testConnectionByConnectionId,
    updateConnection,
};
