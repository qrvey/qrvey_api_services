'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_datasources_proxyApiGateway`;

/**
 * Get taker URL per recipient
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Web form identifier
 * @param {Object} body Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjU-create-qrvey | Create web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const getTakerUrlByRecipient = async (userId, appId, qrveyId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/lookup/recipients`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    getTakerUrlByRecipient,
};