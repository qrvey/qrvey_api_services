'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_datasources_proxyApiGateway`;

/**
 * Create web form (Quiz, form or survey)
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} qrveyDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjU-create-qrvey | Create web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const createWebForm = async (userId, appId, qrveyDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(qrveyDetails)) ? {} : qrveyDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update web form (Quiz, form or survey)
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Qrvey identifier
 * @param {Object} qrveyDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjM-update-qrvey | Update web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const updateWebForm = async (userId, appId, qrveyId, qrveyDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(qrveyDetails)) ? {} : qrveyDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, data, options);
    }

    return axios.put(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete web form
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Web form identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjQ-delete-a-qrvey | Delete web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const deleteWebForm = async (userId, appId, qrveyId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Activate web form
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Web form identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjQ-delete-a-qrvey | Delete web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const activateWebForm = async (userId, appId, qrveyId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/activate`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, null, options);
    }

    return axios.put(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get web form
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Web form identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjI-get-qrvey | Get web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const getWebForm = async (userId, appId, qrveyId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get web form list
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Web form identifier
 * @param {Object} [body] Query option
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjg-get-qrvey-all | Get web forms API}
 * @returns {Promise<Object>} Returns a web form item
 */
const getWebForms = async (userId, appId, qrveyId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    activateWebForm,
    createWebForm,
    updateWebForm,
    deleteWebForm,
    getWebForm,
    getWebForms,
};