'use strict';
const { getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_dataload_drStatus`;

/**
 * Get dataset job status
 * @param {string} jobId Job identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQxNA-get-job-status | Get Job status API}
 * @returns {Promise<Object>} Returns a job item
 */
const getJobStatus = async (jobId, params) => {

    const url = `/dataload/${jobId}/status`;
    let options = getRequestOptions(params);

    options.pathParameters = { jobId };
    options.resourcePath = '/dataload/{jobId}/status';

    return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);

    // if (params && params.invoke === true) {
    //     return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    // }

    // return axios.get(url, options)
    //     .then(response => response.data)
    //     .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    getJobStatus,
};
