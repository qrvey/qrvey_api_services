'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_datasources_proxyApiGateway`;

/**
 * Create dataset transformation
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} body Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/7e6025ad95913-create-a-transformation | Create a transformation API}
 * @returns {Promise<Object>} Returns a transformation item
 */
const createTransformation = async (userId, appId, datasetId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/transformation`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete dataset transformation
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {string} transformationId Transformation identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/6e286226332b6-delete-testing-transformation | Delete transformation API}
 * @returns {Promise<Object>} Returns a transformation item
 */
const deleteTransformation = async (userId, appId, datasetId, transformationId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/transformation/${transformationId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get transformations by datasetId
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {Object} [body] Query option
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/11e18520b05ce-get-transformations-by-dataset-id | Get transformations by datasetId API}
 * @returns {Promise<Object>} Returns a dataset item
 */
const getTransformations = async (userId, appId, datasetId, body = {}, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/transformation/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get single transformation
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {string} transformationId Transformation identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDQ2Mg-get-a-dataset | Get single transformation API}
 * @returns {Promise<Object>} Returns a transformation item
 */
const getTransformation = async (userId, appId, datasetId, transformationId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/transformation/${transformationId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update transformation
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} datasetId Dataset identifier
 * @param {string} transformationId Dataset identifier
 * @param {Object} body Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/3a44603781b86-update-transformation | Update transformation API}
 * @returns {Promise<Object>} Returns a transformation item
 */
const updateTransformation = async (userId, appId, datasetId, transformationId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qollect/dataset/${datasetId}/transformation/${transformationId}`;

    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, data, options);
    }

    return axios.put(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createTransformation,
    deleteTransformation,
    getTransformations,
    getTransformation,
    updateTransformation,
};
