'use strict';

const axios = require('axios');
const { LambdaClient, InvokeCommand } = require('@aws-sdk/client-lambda');
const _ = require('lodash');

const client = new LambdaClient({
    region: process.env.AWS_DEFAULT_REGION,
});

const DEFAULT_HEADERS = {
    'x-api-key': process.env.COMPOSER_API_KEY || process.env.API_KEY,
    'Content-Type': 'application/json',
    Accept: 'application/json, text/plain, */*',
};

const instance = axios.create({
    baseURL: process.env.COMPOSER_DOMAIN || process.env.DOMAIN,
    headers: {
        ...DEFAULT_HEADERS,
    },
});

const adminCenterAxiosInstance = axios.create({
    baseURL: process.env.COMPOSER_DOMAIN || process.env.DOMAIN,
    headers: {
        'Content-Type': 'application/json',
    },
});

/**
 * Get axios instance options
 * @param {Object} params User identifier
 * @param {string} params.domain Composer domain
 * @param {string} params.apiKey Composer api key
 * @param {string} params.contentType Content type
 * @param {Object} params.headers General headers
 * @param {boolean} params.invoke Invoke lambda
 * @returns {Promise<Object>} Returns an user 
 */
const getRequestOptions = (params) => {
    let options = {};

    if (params && params.invoke === true) {
        options.invoke = true; // Do not remove this line
    }

    if (params && params.domain) {
        options.baseURL = params.domain;
    }

    if (params && params.authorization) {
        if (!options.headers) options.headers = {};
        options.headers['Authorization'] = params.authorization;
    }

    if (params && params.apiKey) {
        if (!options.headers) options.headers = {};
        options.headers['x-api-key'] = params.apiKey;
    }

    if (params && params.contentType) {
        if (!options.headers) options.headers = {};
        options.headers['Content-Type'] = params.contentType;
    }

    // This one will overwrite the duplicated header names
    if (params && params.headers) {
        if (!options.headers) options.headers = {};
        options.headers = {
            ...options.headers,
            ...params.headers,
        };
    }

    return options;
};

const getPayload = (httpMethod, path, body = null, axiosOptions) => {
    let headers = (axiosOptions && axiosOptions.headers) ? { ...axiosOptions.headers } : { ...DEFAULT_HEADERS };
    let queryStringParameters = (axiosOptions && axiosOptions.params) ? { ...axiosOptions.params } : null;
    let pathParameters = (axiosOptions && axiosOptions.pathParameters) ? { ...axiosOptions.pathParameters } : null; // Only for DR requests
    let resourcePath = (axiosOptions && axiosOptions.resourcePath) ? axiosOptions.resourcePath : path; // Only for DR requests

    let options = {
        path,
        headers,
        httpMethod,
        queryStringParameters,
        resource: resourcePath,
        requestContext: {
            httpMethod,
            resourcePath,
            path: `/Prod${path}`,
        },
        isBase64Encoded: false,
        pathParameters,
    };

    if (!body) return options;

    return {
        ...options,
        body: JSON.stringify(body),
    };

};

/**
 * Invoke request
 * @param {string} httpMethod Http method 
 * @param {string} url Url 
 * @param {Object} [body=null] request body
 * @param {Object} axiosOptions
 * @see {@link https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html#apigateway-example-event | Event format - V1}
 */
const invokeRequest = async (functionName, httpMethod, url, body = null, axiosOptions) => {

    const params = {
        FunctionName: functionName,
        InvocationType: 'RequestResponse', // Invoke the function synchronously. Keep the connection open until the function returns a response or times out.
        // LogType: 'TAIL', // Set to Tail to include the execution log in the response. 
        Payload: JSON.stringify(
            getPayload(httpMethod, url, body, axiosOptions)
        ),
    };

    const command = new InvokeCommand(params);
    let invokeResponse = await client.send(command);
    const payload = JSON.parse(Buffer.from(invokeResponse.Payload));
    
    const contentType = _.get(payload.headers, 'content-type', 'application/json');
    const shouldParse = _.includes(contentType, 'application/json');
    if (payload && payload.statusCode === 200) return (payload && payload.body && shouldParse === true) ? JSON.parse(payload.body) : payload.body;
    return Promise.reject((payload && payload.body && shouldParse === true) ? JSON.parse(payload.body) : payload.body);
};

module.exports = {
    adminCenterAxiosInstance,
    getRequestOptions,
    axios: instance,
    invokeRequest,
};