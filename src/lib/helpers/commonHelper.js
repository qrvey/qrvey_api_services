'use strict';

/**
 * Sleep
 * @param {number} ms Sleep time
 * @returns {Promise}
 */
const sleep = async (ms = 3000) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

module.exports = {
    sleep,
};