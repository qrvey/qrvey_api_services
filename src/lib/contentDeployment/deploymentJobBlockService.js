'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');


/**
 * Create a deployment job block
 * @param {string} deploymentJobId Deployment job identifier
 * @param {Object} deploymentJobDetails DeploymentJobBlock details
 * @param {string} deploymentJobDetails.name DeploymentJobBlock name
 * @param {string} [deploymentJobDetails.description] DeploymentJobBlock description
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const createDeploymentJobBlock = async (deploymentJobId, deploymentJobDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(deploymentJobDetails)) ? {} : deploymentJobDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete deployment job block
 * @param {string} deploymentJobId Deployment job identifier
 * @param {string} deploymentBlockJobId Deployment job block identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const deleteDeploymentJobBlock = async (deploymentJobId, deploymentBlockJobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block/${deploymentBlockJobId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get deployment job blocks
 * @param {string} deploymentJobId Deployment job identifier
 * @param {string} deploymentBlockJobId Deployment job block identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const getDeploymentJobBlocks = async (deploymentJobId, params) => {

    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block/all`;
    let options = getRequestOptions(params);

    return axios.post(url, {}, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get deployment job block
 * @param {string} deploymentJobId Deployment job identifier
 * @param {string} deploymentBlockJobId Deployment job block identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const getDeploymentJobBlock = async (deploymentJobId, deploymentBlockJobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block/${deploymentBlockJobId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update deployment job block
 * @param {string} deploymentJobId Deployment job identifier
 * @param {string} deploymentBlockJobId Deployment job block identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const updateDeploymentJobBlock = async (deploymentJobId, deploymentBlockJobId, deploymentJobDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block/${deploymentBlockJobId}`;
    let options = getRequestOptions(params);

    return axios.put(url, deploymentJobDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Add recipient data
 * @param {string} deploymentJobId Deployment job identifier
 * @param {string} deploymentBlockJobId Deployment job block identifier
 * @param {Object} recipientsData Recipients data
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJobBlock item
 */
const updateRecipientJobBlockData = async (deploymentJobId, deploymentBlockJobId, recipientsData, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/block/${deploymentBlockJobId}/user/batch/data`;
    let options = getRequestOptions(params);

    return axios.put(url, recipientsData, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createDeploymentJobBlock,
    deleteDeploymentJobBlock,
    getDeploymentJobBlocks,
    getDeploymentJobBlock,
    updateDeploymentJobBlock,
    updateRecipientJobBlockData,
};