'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');


/**
 * Create a package
 * @param {Object} packageDetails Package details
 * @param {string} packageDetails.name Package name
 * @param {string} [packageDetails.description] Package description
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const createPackage = async (packageDetails, params) => {
    const url = '/prod/api/v4/qrvey/admin/content/deployment/package';
    let options = getRequestOptions(params);

    const data = (_isEmpty(packageDetails)) ? {} : packageDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete a package
 * @param {string} [packageId] Package identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const deletePackage = async (packageId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get a packages
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const getPackages = async (body, params) => {

    const url = '/prod/api/v4/qrvey/admin/content/deployment/package/all';
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get a package
 * @param {string} [packageId] Package identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const getPackage = async (packageId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update a package
 * @param {string} [packageId] Package identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const updatePackage = async (packageId, packageDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}`;
    let options = getRequestOptions(params);

    return axios.put(url, packageDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createPackage,
    deletePackage,
    getPackages,
    getPackage,
    updatePackage,
};