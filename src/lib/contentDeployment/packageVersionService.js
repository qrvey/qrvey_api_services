'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');

/**
 * Create package version
 * @param {string} packageId Package identifier
 * @param {Object} versionDetails Package version details
 * @param {string} versionDetails.name Package version name
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a package version item
 */
const createVersion = async (packageId, versionDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/version`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(versionDetails)) ? {} : versionDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Create package version
 * @param {string} packageId Package identifier
 * @param {Object} versionDetails Package version details
 * @param {string} versionDetails.name Package version name
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a package version item
 */
const createPackageVersion = async (packageId, versionDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/job/version`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(versionDetails)) ? {} : versionDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get package version list
 * @param {string} packageId Package identifier
 * @param {Object} [body] Query options
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a package version item
 */
const getPackageVersionList = async (packageId, body, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/version/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete package version
 * @param {string} packageId Package identifier
 * @param {string} packageVersionId Package version identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const deletePackageVersion = async (packageId, packageVersionId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/version/${packageVersionId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get package version
 * @param {string} packageId Package identifier
 * @param {string} packageVersionId Package version identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const getPackageVersion = async (packageId, packageVersionId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/version/${packageVersionId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update package version
 * @param {string} packageId Package identifier
 * @param {string} packageVersionId Package version identifier
 * @param {Object} versionDetails Package identifier
 * @param {string} versionDetails.name Package version name
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const updatePackageVersion = async (packageId, packageVersionId, versionDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/package/${packageId}/version/${packageVersionId}`;
    let options = getRequestOptions(params);

    return axios.put(url, versionDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get package version job status
 * @param {string} jobId Job tracker identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Package item
 */
const getPackageVersionJobByJobId = async (jobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/worker/job/${jobId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * wait until package version is created
 * @param {string} conditionalStatus Tracker Status
 * @param {string} trackerID tracker ID
 * @param {string} authorization Admin api key
 * @returns {Promise<Object>} Returns tracker status item
 */
const waitUntilPackageVersionIsCreated =  async (conditionalStatus, trackerID, token) => {
    let tracker = await getPackageVersionJobByJobId(trackerID, {authorization: token});
    if (conditionalStatus != 'COMPLETED') {
        conditionalStatus = tracker.status;
        tracker = await waitUntilPackageVersionIsCreated(conditionalStatus, trackerID, token);
    }
    return tracker;
}

module.exports = {
    createVersion,
    createPackageVersion,
    deletePackageVersion,
    getPackageVersionList,
    getPackageVersionJobByJobId,
    getPackageVersion,
    updatePackageVersion,
    waitUntilPackageVersionIsCreated
};