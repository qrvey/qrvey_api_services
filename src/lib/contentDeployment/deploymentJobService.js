'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');


/**
 * Create a deployment job
 * @param {Object} deploymentJobDetails DeploymentJob details
 * @param {string} deploymentJobDetails.name DeploymentJob name
 * @param {string} [deploymentJobDetails.description] DeploymentJob description
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const createDeploymentJob = async (deploymentJobDetails, params) => {
    const url = '/prod/api/v4/qrvey/admin/content/deployment/job';
    let options = getRequestOptions(params);

    const data = (_isEmpty(deploymentJobDetails)) ? {} : deploymentJobDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete deployment job
 * @param {string} deploymentJobId DeploymentJob identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const deleteDeploymentJob = async (deploymentJobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Execute job
 * @param {string} deploymentJobId DeploymentJob identifier
 * @param {Object} body Worker options
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const executeDeploymentJob = async (deploymentJobId, body, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/worker`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get executed job information
 * @param {string} jobTrackerId Job tracker identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const getDeploymentJobExecutionProgressByJobTrackerId = async (jobTrackerId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/worker/job/${jobTrackerId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get deployment jobs
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const getDeploymentJobs = async (params) => {

    const url = '/prod/api/v4/qrvey/admin/content/deployment/job/all';
    let options = getRequestOptions(params);

    return axios.post(url, {}, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get deployment job
 * @param {string} deploymentJobId DeploymentJob identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const getDeploymentJob = async (deploymentJobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update deployment job
 * @param {string} deploymentJobId DeploymentJob identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
const updateDeploymentJob = async (deploymentJobId, deploymentJobDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}`;
    let options = getRequestOptions(params);

    return axios.put(url, deploymentJobDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};


/**
 * Update deployment job
 * @param {string} deploymentJobId DeploymentJob identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a DeploymentJob item
 */
 const getDeploymentJobStatus = async (deploymentJobId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/job/${deploymentJobId}/status`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createDeploymentJob,
    deleteDeploymentJob,
    executeDeploymentJob,
    getDeploymentJobExecutionProgressByJobTrackerId,
    getDeploymentJobStatus,
    getDeploymentJobs,
    getDeploymentJob,
    updateDeploymentJob,
};