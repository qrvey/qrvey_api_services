'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');

const {SERVERS} = require('./../../test/config/data')

/**
 * Create a composer server
 * @param {Object} serverDetails Server deatails
 * @param {string} serverDetails.host Server host
 * @param {string} serverDetails.apiKey Server api key
 * @param {string} serverDetails.name Server name
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjQ-create-a-server | Create server API}
 * @returns {Promise<Object>} Returns a Server item
 */
const createServer = async (serverDetails, params) => {
    const url = '/prod/api/v4/qrvey/admin/features/server';
    let options = getRequestOptions(params);

    const data = (_isEmpty(serverDetails)) ? {} : serverDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete server
 * @param {string} [serverId] Server identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjg-delete-a-server | Delete server API}
 * @returns {Promise<Object>} Returns a Server item
 */
const deleteServer = async (serverId, params) => {
    const url = `/prod/api/v4/qrvey/admin/features/server/${serverId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get servers
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjg-delete-a-server | Delete server API}
 * @returns {Promise<Object>} Returns a Server item
 */
const getServers = async (params) => {

    const url = '/prod/api/v4/qrvey/admin/features/server';
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get servers
 * @param {string} [serverId] Server identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjg-delete-a-server | Delete server API}
 * @returns {Promise<Object>} Returns a Server item
 */
const getServer = async (serverId, params) => {
    const url = `/prod/api/v4/qrvey/admin/features/server/${serverId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get the server name from SERVER.general.domain variable
 * @returns {string} Return the server name
 */
const getServerName = () => {
    return SERVERS.general.domain.split('//')[1].split('.')[0]
}

module.exports = {
    createServer,
    deleteServer,
    getServers,
    getServer,
    getServerName
};