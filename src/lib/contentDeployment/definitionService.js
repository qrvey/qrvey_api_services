'use strict';

const { isEmpty: _isEmpty} = require('lodash');
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');


/**
 * Create a composer server
 * @param {Object} definitionDetails Definition details
 * @param {string} definitionDetails.name Definition name
 * @param {string} [definitionDetails.description] Definition description
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Definition item
 */
const createDefinition = async (definitionDetails, params) => {
    const url = '/prod/api/v4/qrvey/admin/content/deployment/definition';
    let options = getRequestOptions(params);

    const data = (_isEmpty(definitionDetails)) ? {} : definitionDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete server
 * @param {string} definitionId Definition identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Definition item
 */
const deleteDefinition = async (definitionId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/definition/${definitionId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get servers
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Definition item
 */
const getDefinitions = async (body, params) => {

    const url = '/prod/api/v4/qrvey/admin/content/deployment/definition/all';
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get servers
 * @param {string} definitionId Definition identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Definition item
 */
const getDefinition = async (definitionId, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/definition/${definitionId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update servers
 * @param {string} definitionId Definition identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns a Definition item
 */
const updateDefinition = async (definitionId, definitionDetails, params) => {
    const url = `/prod/api/v4/qrvey/admin/content/deployment/definition/${definitionId}`;
    let options = getRequestOptions(params);

    return axios.put(url, definitionDetails, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createDefinition,
    deleteDefinition,
    getDefinitions,
    getDefinition,
    updateDefinition,
};