'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_datasources_proxyApiGateway`;

/**
 * Create web form (Quiz, form or survey)
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} workflowDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjU-create-qrvey | Create web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const createWorkflow = async (userId, appId, workflowDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflow`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(workflowDetails)) ? {} : workflowDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update web form (Quiz, form or survey)
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} workflowId Qrvey identifier
 * @param {Object} workflowDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjM-update-qrvey | Update web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const updateWorkflow = async (userId, appId, workflowId, workflowDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflow/${workflowId}`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(workflowDetails)) ? {} : workflowDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, data, options);
    }

    return axios.put(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Update web form (Quiz, form or survey)
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} workflowId Qrvey identifier
 * @param {Object} [workflowDetails] Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjM-update-qrvey | Update web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const activateWorkflow = async (userId, appId, workflowId, workflowDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflow/${workflowId}/activate`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(workflowDetails)) ? {} : workflowDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, data, options);
    }

    return axios.put(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete web form
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} workflowId Web form identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjQ-delete-a-qrvey | Delete web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const deleteWorkflow = async (userId, appId, workflowId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflow/${workflowId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get web form
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} workflowId Web form identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgyMjcxNjI-get-qrvey | Get web form API}
 * @returns {Promise<Object>} Returns a web form item
 */
const getWorkflow = async (userId, appId, workflowId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflow/${workflowId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get workflow list
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} [body] Query option
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns  workflow item
 */
 const getWorkflows = async (userId, appId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/automatiq/workflows`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    activateWorkflow,
    createWorkflow,
    updateWorkflow,
    deleteWorkflow,
    getWorkflow,
    getWorkflows,
};