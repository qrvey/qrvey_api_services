'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_analytiq_AnalytiqLiveService`;

/**
 * Retrieve a metric information
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDUwMA-get-dataset-model-structure | Get Dataset Model Structure}
 * @returns {Promise<Object>} Returns a metric item
 */
const getMetrics = async (userId, appId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/analytiq/metric`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, null, options); 
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

const deleteMetric = async (userId, appId, datasetId, metricId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${datasetId}/analytiq/metric/${metricId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, null, options); 
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    getMetrics,
    deleteMetric,
};