'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_analytiq_AnalytiqLiveService`;

/**
 * Retrieve the information that describes the Dataset/Webform amount of records and list the Question/Columns plus the virtuals columns as Buckets, Formulas, and Datalinks (if exist).
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {string} qrveyId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDUwMA-get-dataset-model-structure | Get Dataset Model Structure}
 * @returns {Promise<Object>} Returns a dataset item
 */
const getModel = async (userId, appId, qrveyId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/model`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'post', url, body, options);
    }

    return axios.post(url, body, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Retrieve the information of the Columns from a specific Dataset.
 * @param {Object} [identifiers] IDs
 * @param {string} identifiers.userId User identifier
 * @param {string} identifiers.appId Application identifier
 * @param {string} identifiers.qrveyId Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link TODO | Get Columns}
 * @returns {Promise<Object>} Returns the column list
 */
const getColumns = async ({ userId, appId, qrveyId }, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart/question/list`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'post', url, body, options);
    }

    return axios.post(url, body, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    getModel,
    getColumns,
};
