'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_analytiq_AnalytiqLiveService`;

/**
 * Create formula
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} formulaDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns the formula item
 */
const createFormula = async ({ userId, appId, qrveyId }, formulaDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/formula`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(formulaDetails)) ? {} : formulaDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete one formula
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {string} [identifiers.formulaId] Formula identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns the formula item
 */
 const deleteFormula = async ({ userId, appId, qrveyId, formulaId }, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/formula/${formulaId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

module.exports = {
    createFormula,
    deleteFormula
}
