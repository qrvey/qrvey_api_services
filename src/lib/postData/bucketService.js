'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_analytiq_AnalytiqLiveService`;

/**
 * Create bucket
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} bucketDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns the bucket item
 */
const createBucket = async ({ userId, appId, qrveyId }, bucketDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/bucket`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(bucketDetails)) ? {} : bucketDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete one bucket
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {string} [identifiers.bucketId] Bucket identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns the bucket item
 */
 const deleteBucket = async ({ userId, appId, qrveyId, bucketId }, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/bucket/${bucketId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

module.exports = {
    createBucket,
    deleteBucket
}
