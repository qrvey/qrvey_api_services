'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_PREFIX = `${process.env.SERVER_PREFIX}_analytiq_`;
const LAMBDA_NAME = {
    chart: `${LAMBDA_PREFIX}elasticViewChart`,
    metric: `${LAMBDA_PREFIX}ElasticViewMetric`,
    rows: `${LAMBDA_PREFIX}ElasticViewDevRows`,
    boxplot: `${LAMBDA_PREFIX}elasticViewBoxplot`,
    formulaTest: `${LAMBDA_PREFIX}ElasticViewDevFormulaTest`,
}

/**
 * Chart Result is the way to analyze data and return an analysis
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * // TODO set new url @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNTg-get-an-qrvey-universal-chart-results | Chart results}
 * @returns {Promise<Object>} Returns a chart result
 */
const chartResult = async ({ userId, appId, qrveyId }, chartDetails, params) => {
    const url = `/devapi/v5/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytics/results/chart`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME[chart], 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Metric Result is the way to analyze data and return an analysis
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * // TODO set new url @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNTg-get-an-qrvey-universal-chart-results | Chart results}
 * @returns {Promise<Object>} Returns a metric result
 */
 const metricResult = async ({ userId, appId, qrveyId }, chartDetails, params) => {
    const url = `/devapi/v5/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytics/results/metric`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME[metric], 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Rows Result is the way to return an raw data
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * // TODO set new url @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNTg-get-an-qrvey-universal-chart-results | Chart results}
 * @returns {Promise<Object>} Returns a rows (raw) result
 */
 const rowsResult = async ({ userId, appId, qrveyId }, chartDetails, params) => {
    const url = `/devapi/v5/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytics/results/rows`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME[rows], 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Boxplot Result is the way to analyze data and return an analysis
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * // TODO set new url @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNTg-get-an-qrvey-universal-chart-results | Chart results}
 * @returns {Promise<Object>} Returns a boxplot result
 */
 const boxplotResult = async ({ userId, appId, qrveyId }, chartDetails, params) => {
    const url = `/devapi/v5/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytics/results/boxplot`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME[boxplot], 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Formula test is the way to test a formula and return data to review it
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} formulaDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @returns {Promise<Object>} Returns if the formula is valid or not.
 */
const formulaTestResult = async ({ userId, appId, qrveyId }, formulaDetails, params) => {
    const url = `/devapi/v5/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytics/formula/test`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(formulaDetails)) ? {} : formulaDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME[formulaTest], 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    chartResult,
    metricResult,
    rowsResult,
    boxplotResult,
    formulaTestResult,
}
