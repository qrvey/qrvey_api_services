'use strict';
const { isEmpty: _isEmpty } = require('lodash');
const { axios, getRequestOptions, invokeRequest } = require('../helpers/axiosHelper');
const LAMBDA_NAME = `${process.env.SERVER_PREFIX}_analytiq_AnalytiqLiveService`;

/**
 * Create chart
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNzQ-create-a-chart | Create chart API}
 * @returns {Promise<Object>} Returns a chart item
 */
const createChart = async ({ userId, appId, qrveyId }, chartDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'POST', url, data, options);
    }

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get the chart list
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNjk-get-all-charts | Get all charts}
 * @returns {Promise<Object>} Returns a chart item
 */
const getChartList = async ({ userId, appId, qrveyId }, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart/all`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

/**
 * Get one chart
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {string} [identifiers.chartId] Chart identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNzA-get-a-chart | Get a chart}
 * @returns {Promise<Object>} Returns a chart item
 */
const getChart = async ({ userId, appId, qrveyId, chartId }, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart/${chartId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'GET', url, options);
    }

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

/**
 * Update a chart
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {string} [identifiers.chartId] Chart identifier
 * @param {Object} chartDetails Request body
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNjk-get-all-charts | Update a chart}
 * @returns {Promise<Object>} Returns a chart item
 */
const updateChart = async ({ userId, appId, qrveyId, chartId }, chartDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart/${chartId}`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(chartDetails)) ? {} : chartDetails;

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'PUT', url, data, options);
    }

    return axios.put(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

/**
 * Get one chart
 * @param {Object} [identifiers] IDs
 * @param {string} [identifiers.userId] User identifier
 * @param {string} [identifiers.appId] Application identifier
 * @param {string} [identifiers.qrveyId] Dataset identifier
 * @param {string} [identifiers.chartId] Chart identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @param {boolean} params.invoke Invoke lambda function
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MjgzNjkwNzE-delete-a-chart | Delete a chart}
 * @returns {Promise<Object>} Returns a chart item
 */
const deleteChart = async ({ userId, appId, qrveyId, chartId }, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}/qrvey/${qrveyId}/analytiq/chart/${chartId}`;
    let options = getRequestOptions(params);

    if (params && params.invoke === true) {
        return invokeRequest(LAMBDA_NAME, 'DELETE', url, options);
    }

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
}

module.exports = {
    createChart,
    getChartList,
    getChart,
    updateChart,
    deleteChart
}
