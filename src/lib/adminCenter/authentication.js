'use strict';
const { adminCenterAxiosInstance: axios, getRequestOptions } = require('../helpers/axiosHelper');


/**
 * Admin Login
 * @param {string} username Username
 * @param {string} password User pasword
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Admin center domain
 * @param {string} [params.authorization] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6MTQ5MzMzMjQ-create-a-server | Create server API}
 * @returns {Promise<Object>} Returns an object with a JWT token
 */
const login = async (username, password, params) => {
    const url = '/admin/api/v5/login';
    let options = getRequestOptions(params);

    return axios.post(url, {username, accessKey: password}, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    login,
};