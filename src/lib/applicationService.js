'use strict';
const { isEmpty: _isEmpty} = require('lodash');
const { axios, getRequestOptions } = require('./helpers/axiosHelper');


/**
 * Create application
 * @param {string} userId User identifier
 * @param {Object} applicationDetails Application details
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDM3Ng-create-application | Create application API}
 * @returns {Promise<Object>} Returns an application item
 */
const createApplication = async (userId, applicationDetails, params) => {
    const url = `/devapi/v4/user/${userId}/app`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(applicationDetails)) ? {} : applicationDetails;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Create application
 * @param {string} userId User identifier
 * @param {Object} body Application details
 * @param {string[]} body.userIds User Id list
 * @param {string} [body.appId] Application Id
 * @param {Boolean} [body.isPublic = false] Is public app
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @returns {Promise<Object>} Returns an application item
 */
const shareApplication = async (userId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/share`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Delete application
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDM3NA-delete-application | Delete application API}
 * @returns {Promise<Object>} Returns an application item
 */
const deleteApplication = async (userId, appId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}`;
    let options = getRequestOptions(params);

    return axios.delete(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get application
 * @param {string} userId User identifier
 * @param {string} appId Application identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDM3Mw-get-application | Get application API}
 * @returns {Promise<Object>} Returns an application item
 */
const getApplication = async (userId, appId, params) => {
    const url = `/devapi/v4/user/${userId}/app/${appId}`;
    let options = getRequestOptions(params);

    return axios.get(url, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

/**
 * Get applications
 * @param {string} userId User identifier
 * @param {Object} [params] Request params
 * @param {string} [params.domain] Composer domain
 * @param {string} [params.apiKey] Composer api key
 * @param {string} params.contentType Content type
 * @see {@link https://qrvey.stoplight.io/docs/qrvey-api-doc/b3A6ODExMDM3NQ-get-applications | Get applications API}
 * @returns {Promise<Object>} Returns an application list item
 */
const getApplications = async (userId, body, params) => {
    const url = `/devapi/v4/user/${userId}/app/all`;
    let options = getRequestOptions(params);

    const data = (_isEmpty(body)) ? {} : body;

    return axios.post(url, data, options)
        .then(response => response.data)
        .catch(error => Promise.reject(error.response.data));
};

module.exports = {
    createApplication,
    deleteApplication,
    getApplications,
    getApplication,
    shareApplication,
};