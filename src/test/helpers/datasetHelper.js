const DatasetService = require('../../lib/preData/datasetService');
const { sleep } = require('../../lib/helpers/commonHelper');

class DatasetHelper {

    static async loadDataset(userId, appId, datasetId, options = { wait: false }, params) {
        let { confirmedLoadedStatusTime: initialconfirmedLoadedStatusTime } = await DatasetService.getDataset(userId, appId, datasetId, params);
        await DatasetService.loadDataset(userId, appId, datasetId, params);

        if (options.wait === false) return DatasetService.getDataset(userId, appId, datasetId, params);
        let stop = false, errorCount = 0;

        do {
            await sleep(3000);

            try {
                let { status, confirmedLoadedStatusTime } = await DatasetService.getDataset(userId, appId, datasetId, params);
                if (status === 'LOADED' || status === 'FAILED' || confirmedLoadedStatusTime !== initialconfirmedLoadedStatusTime) stop = true;
            } catch (error) {
                console.error('DATASET_HELPER', 'loadDataset', error);
                if (10 < errorCount) stop = true;
                errorCount++;
            }

        } while (!stop);

        return DatasetService.getDataset(userId, appId, datasetId, params);
    }
}

module.exports = DatasetHelper;
