class preloadDataTest {
    constructor(data) {
        this.data = data??[]
    }

    validateLCConnections(arr) {
        const requiredNames = ['SNOWFLAKE', 'REDSHIFT', 'PostgreSQL'];
        const namesInArray = arr.map(obj => obj.name);
        
        const hasRequiredProperties = arr.every(obj => obj.hasOwnProperty('connectionId') && obj.hasOwnProperty('name'));
        const containsRequiredNames = namesInArray.every( (name) => {
            const result = requiredNames.includes(name)
            return result
        } );
        
        return hasRequiredProperties && containsRequiredNames;
    }

    validateLCDatasets(arr) {
        const requiredNames = ['Snowflake', 'Redshift', 'Postgresql'];
        const namesInArray = arr.map(obj => obj.name);
        
        const hasRequiredProperties = arr.every(obj => obj.hasOwnProperty('datasetId') && obj.hasOwnProperty('name'));
        const containsRequiredNames = namesInArray.every( (name) => {
            const result = requiredNames.includes(name)
            return result
        } );
        
        return hasRequiredProperties && containsRequiredNames;
    }
    
/**
   @param {Array} connections 
*/ 
    setConnections(connections) {
        const result = this.validateLCConnections(connections);
        if(result) this.data.connections = connections;
    }

    getConnections() {
        return this.data.connections
    }
/**
   @param {Array} datasets
*/ 
    setDatasets(datasets) {
        const result = this.validateLCDatasets(datasets)
        this.data.datasets =  datasets;
    }

    getDatasets () {
        return this.data.datasets
    }
    
    getDataToLoad(mainComposer = this.data) {
        const composer = { ...mainComposer }
        const { userId } = composer
        const options = composer
        delete options.userId
        return { userId, options }
    }
}

module.exports = preloadDataTest;