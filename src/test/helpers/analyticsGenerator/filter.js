class FilterGenerator {
    constructor(body) {
        this._body = body
        this._filterList = []
    }

    get() {
        const logic = this.filterLogic()
        return { logic }
    }

    filterLogic() {
        const elements = this.body.filter(element => element.type === 'root')
        const filterList = elements.map(item => {
            const expressionList = this.expressionList(item.expressions)
            const obj = {
                enabled: item.enabled,
                filters: [
                    {
                        operator: item.operator,
                        expressions: expressionList
                    }
                ]
            }
            if (item?.scope) obj.scope = item.scope
            return obj
        })
        return filterList
    }

    expressionList(expressions) {
        const list = expressions.map(element => {
            return this.expressions(element)
        })
        return list.filter(element => element)
    }

    expressions(expression) {
        const func = this[expression.type]
        if (!func) return
        return func.call(this, expression)
    }

    operator(item) {
        return {
            operator: item.operator,
            expressions: this.expressionList(item.expressions)
        }
    }

    expression(item) {
        const questionid = item.column.id
        const validationType = item.validation
        delete item.type
        delete item.column
        delete item.validation
        return { questionid, validationType, ...item }
    }

    get body() {
        return this._body;
    }

    get filterList() {
        return this._filterList;
    }

    set filterList(filterList) {
        this._filterList = filterList;
    }

}

module.exports = FilterGenerator
