const DEFAULT_NAMES = [
    {
        name: 'bloodType',
        type: 'strings'
    },
    {
        name: 'gender',
        type: 'strings'
    },
    {
        name: 'carOrigin',
        type: 'strings'
    },
    {
        name: 'carAcceleration',
        type: 'numerics'
    },
    {
        name: 'carMilesPerGalon',
        type: 'numerics'
    },
    {
        name: 'carCylinders',
        type: 'numerics'
    },
    {
        name: 'birthdate',
        type: 'dates'
    }
]

module.exports = {
    DEFAULT_NAMES
}
