class RowsGenerator {
    constructor(body) {
        this._body = body
        this._sort = []
    }

    get() {
        const select = this.select()
        const aggregates = this.aggregates()
        const root = this.root()
        const sort = this.sort
        return { select, aggregates, ...root, sort }
    }

    select() {
        const elements = this.body.filter(element => element.type === 'dimension')
        const list = elements.map(item => {
            if (item?.sort) {
                this.sort.push({
                    questionid: item.column.id,
                    direction: item.sort
                })
            }
            return {
                questionid: item.column.id
            }
        })
        return list
    }

    aggregates() {
        const elements = this.body.filter(element => element.type === 'summary')
        const list = {}, result = []
        elements.forEach(item => {
            const questionid = item.column.id
            if (!list[questionid]) {
                list[questionid] = {
                    questionid,
                    aggFunctions: [
                        item.agg
                    ]
                }
            } else {
                list[questionid].aggFunctions.push(item.agg)
            }
        });
        for (const key in list) {
            result.push(list[key])
        }
        return result
    }

    root() {
        const elements = this.body.filter(element => element.type === 'root')
        let result = {}
        elements.forEach(item => {
            delete item.type
            result = { ...result, ...item }
        })
        return result
    }

    get body() {
        return this._body;
    }

    get sort() {
        return this._sort;
    }
}

module.exports = RowsGenerator
