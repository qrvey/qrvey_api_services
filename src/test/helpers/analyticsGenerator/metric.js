class MetricGenerator {
    constructor(body) {
        this._body = body
    }

    get() {
        const value = this.value()
        const more = this.rootElements()
        return { value, ...more }
    }

    value() {
        const elements = this.body.filter(element => element.type === 'summary')
        const item = elements[0]
        return {
            questionid: item.column.id,
            aggregate: item.agg
        }
    }

    rootElements() {
        const elements = this.body.filter(element => element.type === 'root')
        const item = elements[0]
        if (item) delete item.type
        return item
    }

    get body() {
        return this._body;
    }
}

module.exports = MetricGenerator
