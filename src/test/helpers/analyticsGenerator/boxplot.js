class BoxplotGenerator {
    constructor(body) {
        this._body = body
    }

    get() {
        const dimensions = this.dimensions()
        const summaries = this.summaries()
        return { chart: { dimensions, summaries } }
    }

    dimensions() {
        const elements = this.body.filter(element => element.type === 'dimension')
        const item = elements[0]
        if (!item) return []
        const obj = {
            questionid: item.column.id
        }
        if (item?.size) obj.maxDataPoints = item.size
        if (item?.sort) {
            obj.sort = {
                by: 'CATEGORY',
                direction: item.sort
            }
        }
        return [obj]
    }

    summaries() {
        const elements = this.body.filter(element => element.type === 'summary')
        const item = elements[0]
        const obj = {
            questionid: item.column.id
        }
        if (item?.size) obj.maxDataPoints = item.size
        if (item?.sort) {
            obj.sort = {
                by: 'CATEGORY',
                direction: item.sort
            }
        }
        return [obj]
    }

    get body() {
        return this._body;
    }
}

module.exports = BoxplotGenerator
