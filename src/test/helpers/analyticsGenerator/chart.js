class ChartGenerator {
    constructor(body) {
        this._body = body
        this._dimensionList = []
        this._summariesList = []
    }

    get() {
        const dimensions = this.dimensions()
        const summaries = this.summaries()
        return { charts: [{ dimensions, summaries }] }
    }

    dimensions() {
        const elements = this.body.filter(element => element.type === 'dimension')
        const list = elements.map(item => {
            const obj = {
                questionid: item.column.id
            }
            if (item?.dateGroup) obj.groupValue = item.dateGroup
            if (item?.size) obj.maxDataPoints = item.size
            if (item?.numberAsRange) obj.numberAsRange = item.numberAsRange
            if (item?.sort) {
                obj.sort = {
                    by: 'CATEGORY',
                    direction: item.sort
                }
            }
            return obj
        })
        this.dimensionList = list
        return this.dimensionList
    }

    summaries() {
        const elements = this.body.filter(element => element.type === 'summary')
        const list = elements.map((item, index) => {
            const obj = {
                questionid: item.column.id,
                aggregate: item.agg,
                pointer: item.pointer,
                formula: item.formula
            }

            const funSorting = (order) => {
                const sort = {
                    by: 'VALUE',
                    direction: item.sort,
                    order: []
                }
                const last = this.dimensionList.length - 1
                if (!this.dimensionList[last]?.sort) this.dimensionList[last].sort = sort
                this.dimensionList[last].sort.order.push(order)
            }

            if (item?.hidden) {
                const order = {
                    hidden: true,
                    direction: item.sort,
                    ...obj
                }
                funSorting(order)
                return
            }
            if (item?.sort) {
                const order = {
                    direction: item.sort,
                    summaryIndex: index
                }
                funSorting(order)
            }

            if (item?.inner) {
                const copy = [...item.inner]
                delete item.inner
                copy.push(item)
                const chart = new ChartGenerator(copy).get()
                obj.innerCharts = chart.charts
            }

            if (item?.totals) obj.totals = item.totals
            if (item?.calculations) obj.calculations = item.calculations
            return obj
        })

        this.summariesList = list.filter(item => item)
        return this.summariesList
    }

    get body() {
        return this._body;
    }

    get dimensionList() {
        return this._dimensionList;
    }

    set dimensionList(dimensionList) {
        this._dimensionList = dimensionList;
    }

    get summariesList() {
        return this._summariesList;
    }

    set summariesList(summariesList) {
        this._summariesList = summariesList;
    }

}

module.exports = ChartGenerator
