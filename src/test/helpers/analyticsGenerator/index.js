const { DEFAULT_NAMES } = require('./constants');
const FilterGenerator = require('./filter');
const ChartGenerator = require('./chart');
const MetricGenerator = require('./metric');
const BoxplotGenerator = require('./boxplot');
const RowsGenerator = require('./rows');

class BodyGenerator {
    constructor(type, columns) {
        this._type = type
        this._columns = columns
        this._body = []
        this._filters = []
        this._defaults = this.defaultColumns()
    }

    defaultColumns() {
        const defaults = {
            strings: [],
            numerics: [],
            dates: []
        }

        DEFAULT_NAMES.forEach(item => {
            const found = this.columns.find(column => {
                const regexp = new RegExp(item.name, 'gi')
                const text = column.text.toLowerCase()
                return regexp.test(text)
            })
            if (found) defaults[item.type].push(found)
        })

        return defaults
    }

    get() {
        return { body: this.body, filters: this.filters }
    }

    toBody() {
        const classGetter = this._bodyTypes(this.type)
        const chart = new classGetter(this.body).get()
        const filters = new FilterGenerator(this.filters).get()
        return { ...chart, ...filters }
    }

    _bodyTypes(type) {
        const types = {
            chart: ChartGenerator,
            metric: MetricGenerator,
            boxplot: BoxplotGenerator,
            rows: RowsGenerator
        }
        return types[type]
    }

    //types = stirngs, numerics, dates
    dimension({ id = 0, customId = null, type = 'strings', dateGroup = null, numberAsRange = undefined } = {}) {
        const column = (customId) ? { id: customId } : this.defaults[type][id]
        this.body.push({
            type: 'dimension',
            column,
            dateGroup,
            numberAsRange
        })
        return this
    }

    //types = stirngs, numerics, dates
    summary({ agg = 'SUM', customId = null, id = 0, type = 'numerics', pointer = false, formula = false} = {}) {
        const column = (customId) ? { id: customId } : this.defaults[type][id]
        formula = formula ? `[${this.defaults[type][id].id}SUMDEFAULT] + 1` : undefined
        pointer = pointer ? `${this.defaults[type][id].id}SUMDEFAULT`: undefined
        this.body.push({
            type: 'summary',
            column,
            agg,
            pointer,
            formula
        })
        return this
    }

    sort({ direction = 'ASC', hidden = false } = {}) {
        const last = this.body.length - 1
        this.body[last]['sort'] = direction
        if (hidden) this.body[last]['hidden'] = hidden
        return this
    }

    size(len = 5) {
        const last = this.body.length - 1
        this.body[last]['size'] = len
        return this
    }

    addTotals({ grand = true, sub = true } = {}) {
        const last = this.body.length - 1
        this.body[last]['totals'] = {
            grand, sub
        }
        return this
    }

    calculation({ aggregate = "AVG", reset = null, direction = null } = {}) {
        const last = this.body.length - 1
        this.body[last]['calculations'] = {
            aggregate, reset, direction
        }
        return this
    }

    inner({ id = 0, customId = null, type = 'numerics', dateGroup = null, direction = 'ASC' } = {}) {
        const last = this.body.length - 1
        const generator = new BodyGenerator('chart', this.columns)
        const inner = generator.dimension({ id, customId, type, dateGroup }).sort({ direction }).get()
        this.body[last]['inner'] = inner.body
        return this
    }

    addToRoot(params) {
        this.body.push({
            type: 'root',
            ...params
        })
        return this
    }

    filter({ scope = null, enabled = true, operator = 'AND' } = {}) {
        const obj = {
            type: 'root',
            enabled, operator,
            expressions: []
        }
        if (scope) obj['scope'] = scope
        this.filters.push(obj)
        return this
    }

    operator(operator = 'AND') {
        const last = this.filters.length - 1

        const check = this.filters[last]?.expressions
        if (!check) throw 'Add a filter first > .filter'

        this.filters[last]['expressions'].push({
            type: 'operator',
            operator,
            expressions: []
        })
        return this
    }

    expression({ id = 0, customId = null, type = 'strings',
        validation = 'EQUAL', dateGroup = null, values = [], enabled = true, anchor = undefined } = {}) {

        const column = (customId) ? { id: customId } : this.defaults[type][id]
        const obj = {
            type: 'expression',
            column,
            value: values,
            validation, enabled, anchor
        }
        if (dateGroup) obj.groupValue = dateGroup

        const last = this.filters.length - 1
        const check = this.filters[last]?.expressions
        if (!check) throw 'Add a filter first > .filter'

        const lastExp = this.filters[last]['expressions'].length - 1
        const hasType = this.filters[last]['expressions'][lastExp]?.type
        if (hasType && hasType === 'operator') {
            this.filters[last]['expressions'][lastExp]['expressions'].push(obj)
        } else {
            this.filters[last]['expressions'].push(obj)
        }
        return this
    }

    _relative({ cursor = "the_last", number = 30, unit = "day",
        includeCurrent = true, isCalendarDate = true,
        enabled = true, displayed = true, anchor = undefined } = {}) {

        const values = [{ cursor, number, unit, includeCurrent, isCalendarDate, displayed, enabled, anchor }]
        return { validation: 'RELATIVE', values }
    }

    relative({ id = 0, type = 'dates',
        cursor = "the_last", number = 30, unit = "day",
        includeCurrent = true, isCalendarDate = true,
        enabled = true, displayed = true, anchor = undefined } = {}) {

        const obj = {
            id, type,
            cursor, number, unit, enabled,
            includeCurrent, isCalendarDate, displayed, anchor
        }
        return this.expression({ ...obj, ...this._relative(obj) })
    }

    get type() {
        return this._type;
    }

    get columns() {
        return this._columns;
    }

    get defaults() {
        return this._defaults;
    }

    get body() {
        return this._body;
    }

    set body(body) {
        this._body = body;
    }

    get filters() {
        return this._filters;
    }

    set filters(filters) {
        this._filters = filters;
    }

}

module.exports = {
    BodyGenerator
}
