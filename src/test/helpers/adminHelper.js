'use strict';
const { get: _get } = require('lodash');
/**
 * Mapped connection item
 * @typedef mappedConnectionItem
 * @type {object}
 * @property {string} name Connection name
 * @property {string} connectionId Connection id
*/

const ComposerHelper = require('./composerHelper');
const DefinitionHelper = require('./definitionHelper');

const {
    serverService: ServerService,
    packageService: PackageService,
    packageVersionService: PackageVersionService,
    definitionService: DefinitionService,
} = require('../../lib/contentDeployment');

const UserService = require('../../lib/userService');
const CommonHelper = require('../../lib/helpers/commonHelper');

class AdminHelper {

    /**
     * 
     * @param {string} serverId server identifier
     */
    constructor(serverId) {
        if (!serverId) throw new Error('Missing serverId');
        this._serverId = serverId;
        this._serverItem = serverId;
        this._sourceComposerItem = null;
        this._packageItem = null;
        this._packageVersionId = null;
        this._packageId = null;
        this._definitionId = null;
        this._appName = null;
    }

    get appName() {
        return this._appName;
    }

    set appName(appName) {
        this._appName = appName;
    }

    get definitionId() {
        return this._definitionId;
    }

    set definitionId(definitionId) {
        this._definitionId = definitionId;
    }

    get serverId() {
        return this._serverId;
    }

    set serverId(serverId) {
        this._serverId = serverId;
    }

    get serverItem() {
        return this._serverItem;
    }

    set serverItem(serverItem) {
        this._serverItem = serverItem;
    }

    get packageItem() {
        return this._packageItem;
    }

    set packageItem(packageItem) {
        this._packageItem = packageItem;
    }

    get packageId() {
        return this._packageId;
    }

    set packageId(packageId) {
        this._packageId = packageId;
    }

    get packageVersionId() {
        return this._packageVersionId;
    }

    set packageVersionId(packageVersionId) {
        this._packageVersionId = packageVersionId;
    }

    get sourceComposerItem() {
        return this._sourceComposerItem;
    }

    set sourceComposerItem(sourceComposerItem) {
        this._sourceComposerItem = sourceComposerItem;
    }
    /**
     * Delete all
     */
    async deleteAll(params) {
        let self = this;

        if (self.sourceComposerItem) await self.sourceComposerItem.deleteAll();
        if (self.definitionId) await DefinitionService.deleteDefinition(self.definitionId, params);
        if (self.packageId) await PackageService.deletePackage(self.packageId, params);

        return {
            message: 'Package has been deleted',
        };
    }

    /**
     * 
     * @param {Object} serverDetails Server deatails
     * @param {string} serverDetails.host Server host
     * @param {string} serverDetails.apiKey Server api key
     * @param {string} serverDetails.name Server name
     * @param {Object} [params] Request params
     * @param {string} [params.domain] Admin center domain
     * @param {string} [params.authorization] Composer api key
     * @param {string} params.contentType Content type
     * @returns {AdminHelper} Returns an Admin server item
     */
    static async findOrCreateServer(serverDetails, params) {
        if (!serverDetails) throw new Error('Missing server domain');
        if (!serverDetails.host) throw new Error('Missing server domain');
        let serverList = await ServerService.getServers(params);

        if (Array.isArray(serverList)) {
            let serverItem = serverList.find(serverItem => serverItem.host === serverDetails.host);
            if (serverItem) return new AdminHelper(serverItem.adminserverid);
        }

        let serverItem = await ServerService.createServer(serverDetails, params);
        return new AdminHelper(serverItem.adminserverid);
    }

    // Basic case
    static async createBasicPackage(serverDetails, packageDetails, params) {
        if (!serverDetails) throw new Error('serverDetails is required');
        if (!packageDetails.userId) throw new Error('packageDetails.userId is required');
        if (!packageDetails.appName) throw new Error('packageDetails.appName is required');
        if (!packageDetails.packageName) throw new Error('packageDetails.packageName is required');

        let sourceAppName = (packageDetails.sourceAppName) ? packageDetails.sourceAppName : packageDetails.appName;

        let userItem = await UserService.getUser(packageDetails.userId); // Get user details
        let serverItem = await AdminHelper.findOrCreateServer(serverDetails, params); // Get server information

        let adminHelperItem = new AdminHelper(serverItem.serverId);

        adminHelperItem.appName = packageDetails.appName;
        adminHelperItem.serverItem = serverItem;
        adminHelperItem.sourceComposerItem = await ComposerHelper.createBasicApplication(packageDetails.userId, { name: sourceAppName });

        let packageItem = await PackageService.createPackage({
            appName: packageDetails.appName,
            name: packageDetails.packageName,
            adminServerId: adminHelperItem.serverId,
            applicationId: adminHelperItem.sourceComposerItem.appId,
            userId: packageDetails.userId,
            email: userItem.email,
        }, params);


        adminHelperItem.packageItem = packageItem;
        adminHelperItem.packageId = packageItem.packageId;

        const { packageVersionId } = await AdminHelper.createPackageVersion(adminHelperItem.packageItem.packageId, { name: 'v1' }, { wait: true }, params);
        adminHelperItem.packageVersionId = packageVersionId;
        return adminHelperItem;
    }

    static async createPackageVersion(packageId, versionDetails, options, params) {
        if (!versionDetails) throw new Error('versionDetails is required');
        if (!versionDetails.name) throw new Error('Version name is required');

        let packageJobItem = await PackageVersionService.createPackageVersion(packageId, versionDetails, params);

        let stopProcess = false, count = 0;

        if (options && options.wait === true) {
            while (stopProcess !== true) {
                let loadDatasetTrackerItem = await PackageVersionService.getPackageVersionJobByJobId(packageJobItem.jobTrackerId, params);

                if (loadDatasetTrackerItem.status === 'COMPLETED' || loadDatasetTrackerItem.status === 'FAILED') {
                    stopProcess = true;
                }

                if (count === 18) {
                    stopProcess = true;
                }

                count++;
                await CommonHelper.sleep(3000);
            }

            let loadDatasetTrackerItem = await PackageVersionService.getPackageVersionJobByJobId(packageJobItem.jobTrackerId, params);
            const packageVersionId = loadDatasetTrackerItem.packageVersionId;
            if (!packageVersionId) throw new Error('Could not find package version');
            
            let result = await PackageVersionService.getPackageVersion(packageId, packageVersionId, params);
            let packageVersionItem = _get(result, '[0]');

            return { ...packageJobItem, packageVersionId, packageVersionItem };
        }

        return packageJobItem;
    }

    static async createBasicDefinition(serverDetails, definitionDetails, params) {
        if (!serverDetails) throw new Error('serverDetails is required');
        if (!definitionDetails.userId) throw new Error('definitionDetails.userId is required');
        if (!definitionDetails.name) throw new Error('definitionDetails.name is required');

        let packageDetails = {
            userId: definitionDetails.userId,
            appName: `TG - ${definitionDetails.name}`,
            packageName: `PK - ${definitionDetails.name}`,
            sourceAppName: definitionDetails.name,
        };

        let adminItem = await AdminHelper.createBasicPackage(serverDetails, packageDetails, params);

        let definitionItem = await DefinitionService.createDefinition({ name: definitionDetails.name, description: definitionDetails.description }, params);
        let packageVersionResult = await PackageVersionService.getPackageVersion(adminItem.packageId, adminItem.packageVersionId, params);
        let packageVersionItem = packageVersionResult[0];

        let contentPackageVersionSelected = DefinitionHelper.mapAssetsByPackageVersionItem(packageVersionItem);

        definitionItem = {
            ...definitionItem,
            contentPackageVersionSelected,
            appName: packageDetails.appName,
            recipientApp: 'NEW',
            packageId: adminItem.packageId,
            packageVersionId: adminItem.packageVersionId,
        };

        adminItem.definitionId = definitionItem.definitionId;

        await DefinitionService.updateDefinition(definitionItem.definitionId, definitionItem, params);

        return adminItem;
    }

    static async createFullPackage(serverDetails, packageDetails, params) {
        if (!serverDetails) throw new Error('serverDetails is required');
        if (!packageDetails.userId) throw new Error('packageDetails.userId is required');
        if (!packageDetails.appName) throw new Error('packageDetails.appName is required');
        if (!packageDetails.packageName) throw new Error('packageDetails.packageName is required');

        let sourceAppName = (packageDetails.sourceAppName) ? packageDetails.sourceAppName : packageDetails.appName;

        let userItem = await UserService.getUser(packageDetails.userId); // Get user details
        let serverItem = await AdminHelper.findOrCreateServer(serverDetails, params); // Get server information

        let adminHelperItem = new AdminHelper(serverItem.serverId);

        adminHelperItem.appName = packageDetails.appName;
        adminHelperItem.serverItem = serverItem;
        adminHelperItem.sourceComposerItem = await ComposerHelper.createFullApplication(packageDetails.userId, { name: sourceAppName });

        let packageItem = await PackageService.createPackage({
            appName: packageDetails.appName,
            name: packageDetails.packageName,
            adminServerId: adminHelperItem.serverId,
            applicationId: adminHelperItem.sourceComposerItem.appId,
            userId: packageDetails.userId,
            email: userItem.email,
        }, params);


        adminHelperItem.packageItem = packageItem;
        adminHelperItem.packageId = packageItem.packageId;

        const { packageVersionId } = await AdminHelper.createPackageVersion(adminHelperItem.packageItem.packageId, { name: 'v1' }, { wait: true }, params);
        adminHelperItem.packageVersionId = packageVersionId;
        return adminHelperItem;
    }
    
    static async createFullDefinition(serverDetails, definitionDetails, params) {
        if (!serverDetails) throw new Error('serverDetails is required');
        if (!definitionDetails.userId) throw new Error('definitionDetails.userId is required');
        if (!definitionDetails.name) throw new Error('definitionDetails.name is required');

        let packageDetails = {
            userId: definitionDetails.userId,
            appName: `TG - ${definitionDetails.name}`,
            packageName: `PK - ${definitionDetails.name}`,
            sourceAppName: definitionDetails.name,
        };

        let adminItem = await AdminHelper.createFullPackage(serverDetails, packageDetails, params);

        let definitionItem = await DefinitionService.createDefinition({ name: definitionDetails.name, description: definitionDetails.description }, params);
        let packageVersionResult = await PackageVersionService.getPackageVersion(adminItem.packageId, adminItem.packageVersionId, params);
        let packageVersionItem = packageVersionResult[0];

        let contentPackageVersionSelected = DefinitionHelper.mapAssetsByPackageVersionItem(packageVersionItem);

        definitionItem = {
            ...definitionItem,
            contentPackageVersionSelected,
            appName: packageDetails.appName,
            recipientApp: 'NEW',
            packageId: adminItem.packageId,
            packageVersionId: adminItem.packageVersionId,
        };

        adminItem.definitionId = definitionItem.definitionId;

        await DefinitionService.updateDefinition(definitionItem.definitionId, definitionItem, params);

        return adminItem;
    }
}

module.exports = AdminHelper;