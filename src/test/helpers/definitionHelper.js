const {map: _map} = require('lodash');

class DefinitionHelper {

    static mapConnections (connectionList) {
        if (!connectionList) throw new Error('connectionList is required');
        if (!Array.isArray(connectionList)) throw new Error('Invalid connection list');

        let result = connectionList.map(item => {
            return {
                ...item,

                _type: 'connection',
                _assetId: item.connectionId,
                connectionClassType: 'database',
                saveStatus: 'new',
                updateToken: '',
            };
        });

        return result;
    }
    
    static mapDatasets (datasetList) {
        if (!datasetList) throw new Error('datasetList is required');
        if (!Array.isArray(datasetList)) throw new Error('Invalid dataset list');

        let result = datasetList.map(item => {
            return {
                ...item,

                _type: 'dataset',
                _assetId: item.datasetId,
                datasetType: 'DATASET',
                saveStatus: 'new',
                updateToken: '',
                loadData: false,
                
                charts: _map(item.charts, (chartItem) => {
                    return {
                        ...chartItem,
                        _assetId: chartItem.chartId,
                        _type: 'chart',
                        chartType: 'chart',
                        _loadedAsDependency: false,
                        _isLocalDependency: false,
                    };
                }),
                metrics: _map(item.metrics, (metricItem) => {
                    return {
                        ...metricItem,
                        _assetId: metricItem.chartId,
                        _type: 'chart',
                        chartType: 'metric',
                        _loadedAsDependency: false,
                        _isLocalDependency: false,
                    };
                }),
                formulas: [],
                buckets: [],
                transformations: [],
            };
        });

        return result;
    }

    static mapAssetsByPackageVersionItem (packageVersionItem) {
        if (!packageVersionItem) throw new Error('packageVersionItem is required');
        if (!packageVersionItem.contentPackageVersion) throw new Error('packageVersionItem.contentPackageVersion is required');

        let connections = DefinitionHelper.mapConnections(packageVersionItem.contentPackageVersion.connections);
        let datasets = DefinitionHelper.mapDatasets(packageVersionItem.contentPackageVersion.datasets);

        return {
            datasets,
            connections,
        };
    }
}

module.exports = DefinitionHelper;