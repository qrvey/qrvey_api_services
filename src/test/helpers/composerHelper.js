'use strict';

/**
 * Mapped connection item
 * @typedef mappedConnectionItem
 * @type {object}
 * @property {string} name Connection name
 * @property {string} connectionId Connection id
*/

/**
 * Mapped dataset item
 * @typedef mappedDataseItem
 * @type {object}
 * @property {string} name Connection name
 * @property {string} datasetId Dataset id
*/

const {
    find: _find,
    isEmpty: _isEmpty
} = require('lodash');

const chance = require('chance').Chance();

const ApplicationService = require('../../lib/applicationService');
const ConnectionService = require('../../lib/preData/connectionService');
const DatasetService = require('../../lib/preData/datasetService');
const PageService = require('../../lib/postData/pageService');
const DatasetHelper = require('./datasetHelper');

const {
    CONNECTIONS,
} = require('../config/data');
// const { getDataToLoad } = require('../config/preload');
const data = require('./../config/preload').COMPOSER
const preloadHelper = require('./../helpers/preloadHelper')
const dataHelper = new preloadHelper(data)

class ComposerHelper {
    /**
     * Create a Composer Helper class, with a User, and App, some connections and Datasets
     * @param {string} userId
     * @param {Object} options To create a Pre loaded Composer class
     * @param {String} options.appId
     * @param {Array<Object>} options.connections
     * @param {String} options.connections.connectionId
     * @param {String} options.connections.name
     * @param {Array<Object>} options.datasets
     * @param {String} options.datasets.datasetId
     * @param {String} options.datasets.name
     */
    constructor(userId, options = null) {
        if (!userId) throw new Error('Missing user id');
        this._userId = userId;
        this._isPreloaded = !_isEmpty(options);
        const { appId, connections, datasets } = options || {};
        this._appId = appId || null;
        this._connections = connections || [];
        this._datasets = datasets || [];
        this._datasetId = '';

    }

    get appId() {
        return this._appId;
    }

    set appId(appId) {
        this._appId = appId;
    }

    set datasetId(datasetId) {
        this._datasetId = datasetId;
    }

    get datasetId() {
        return this._datasetId;
    }

    get isPreloaded() {
        return this._isPreloaded;
    }

    /**
     * Get mapped connections
     * @returns {mappedConnectionItem[]}
     */
    get connections() {
        return this._connections;
    }

    set connections(connections) {
        this._connections = connections;
    }

    /**
     * Get mapped datasets
     * @returns {mappedDataseItem[]}
     */
    get datasets() {
        return this._datasets;
    }

    set datasets(datasets) {
        this._datasets = datasets;
    }

    get userId() {
        return this._userId;
    }

    set userId(userId) {
        this._userId = userId;
    }

    /**
     * Add connection data
     * @param {mappedConnectionItem} connectionItem
     */
    addConnection(connectionItem) {
        this._connections = [...this.connections, connectionItem];
    }

    /**
     * Add dataset data
     * @param {mappedDataseItem} datasetItem
     */
    addDataset(datasetItem) {
        this._datasets = [...this.datasets, datasetItem];
    }

    /**
     *
     * @param {*} userId
     * @param {Object} appDetails Application details
     * @param {string} appDetails.name Application name
     * @param {string} [appDetails.description] Application description
     */
    static async createBasicApplication(userId, appDetails) {
        let composerHelperItem = new ComposerHelper(userId);

        if (!appDetails) throw new Error('Missing app details');
        if (!appDetails.name) throw new Error('App name is required');

        let applicationItem = await ApplicationService.createApplication(userId, appDetails);
        composerHelperItem.appId = applicationItem.appid;

        const mysqlConnection = await composerHelperItem.createMysqlConnection('Mysql connection');
        const athenaConnection = await composerHelperItem.createAthenaConnection('Athena connection');
        const mssqlConnection = await composerHelperItem.createMssqlConnection('Athena connection');
        const oracleConnection = await composerHelperItem.createOracleConnection('Oracle connection');

        await composerHelperItem.createDatasetFromConnection('Mysql Dataset', {
            name: 'Mysql Dataset',
            connectionId: mysqlConnection.connectorid,
            connectorType: mysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        });

        await composerHelperItem.createDatasetFromConnection('Athena Dataset', {
            name: 'Athena Dataset',
            connectionId: athenaConnection.connectorid,
            connectorType: athenaConnection.connectorType,
            database: CONNECTIONS.athena.databases.general.name,
            tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
            catalogName: CONNECTIONS.athena.databases.general.catalogName,
        });

        await composerHelperItem.createDatasetFromConnection('Mssql Dataset', {
            name: 'Mssql Dataset',
            connectionId: mssqlConnection.connectorid,
            connectorType: mssqlConnection.connectorType,
            database: CONNECTIONS.mssql.databases.general.name,
            tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
            tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
        });

        await composerHelperItem.createDatasetFromConnection('Oracle Dataset', {
            name: 'Oracle Dataset',
            connectionId: oracleConnection.connectorid,
            connectorType: oracleConnection.connectorType,
            database: CONNECTIONS.oracle.databases.general.name,
            tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
        });

        await composerHelperItem.createDatasetFromConnection('Mysql Dataset - Clients', {
            name: 'Mysql Dataset - Clients',
            connectionId: mysqlConnection.connectorid,
            connectorType: mysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.clients.tableName,
        }, {
            securityColumns: [{
                columnName: 'company',
                securityName: 'company',
            }],
        });

        return composerHelperItem;
    }

    static async preLoadApplication() {
        const { userId, options } = dataHelper.getDataToLoad()
        const composerHelperItem = new ComposerHelper(userId, options);

        //Try to get the info, to review if the pre loaded data is right
        const app = ApplicationService.getApplication(userId, composerHelperItem.appId)
        const dataset = DatasetService.getDataset(userId, composerHelperItem.appId, composerHelperItem.datasets[0].datasetId)
        await Promise.all([app, dataset])

        return composerHelperItem
    }

    /**
     *
     * @param {*} userId
     * @param {Object} appDetails Application details
     * @param {string} appDetails.name Application name
     * @param {string} [appDetails.description] Application description
     */
    static async createFullApplication(userId, appDetails) {
        let composerHelperItem = new ComposerHelper(userId);

        if (!appDetails) throw new Error('Missing app details');
        if (!appDetails.name) throw new Error('App name is required');

        let applicationItem = await ApplicationService.createApplication(userId, appDetails);
        composerHelperItem.appId = applicationItem.appid;

        const mysqlConnection = await composerHelperItem.createMysqlConnection('Mysql connection');
        const athenaConnection = await composerHelperItem.createAthenaConnection('Athena connection');
        const mssqlConnection = await composerHelperItem.createMssqlConnection('Athena connection');
        const oracleConnection = await composerHelperItem.createOracleConnection('Oracle connection');

        const advancedMysqlConnection = await composerHelperItem.createMysqlConnection('Advanced Mysql connection', { advanced: true });
        const advancedMssqlConnection = await composerHelperItem.createMssqlConnection('Advanced Athena connection', { advanced: true });
        const advancedOracleConnection = await composerHelperItem.createOracleConnection('Advanced Oracle connection', { advanced: true });

        await composerHelperItem.createDatasetFromConnection('Mysql Dataset', {
            name: 'Mysql Dataset',
            connectionId: mysqlConnection.connectorid,
            connectorType: mysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        });

        await composerHelperItem.createDatasetFromConnection('Athena Dataset', {
            name: 'Athena Dataset',
            connectionId: athenaConnection.connectorid,
            connectorType: athenaConnection.connectorType,
            database: CONNECTIONS.athena.databases.general.name,
            tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
            catalogName: CONNECTIONS.athena.databases.general.catalogName,
        });

        await composerHelperItem.createDatasetFromConnection('Mssql Dataset', {
            name: 'Mssql Dataset',
            connectionId: mssqlConnection.connectorid,
            connectorType: mssqlConnection.connectorType,
            database: CONNECTIONS.mssql.databases.general.name,
            tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
            tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
        });

        await composerHelperItem.createDatasetFromConnection('Oracle Dataset', {
            name: 'Mysql Dataset',
            connectionId: oracleConnection.connectorid,
            connectorType: oracleConnection.connectorType,
            database: CONNECTIONS.oracle.databases.general.name,
            tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
        });

        await composerHelperItem.createDatasetFromConnection('Advanced Mysql Dataset', {
            name: 'Advanced Mysql Dataset',
            connectionId: advancedMysqlConnection.connectorid,
            connectorType: advancedMysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        });

        await composerHelperItem.createDatasetFromConnection('Advanced Mssql Dataset', {
            name: 'Advanced Mssql Dataset',
            connectionId: advancedMssqlConnection.connectorid,
            connectorType: advancedMssqlConnection.connectorType,
            database: CONNECTIONS.mssql.databases.general.name,
            tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
            tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
        });

        await composerHelperItem.createDatasetFromConnection('Advanced Oracle Dataset', {
            name: 'Advanced oracle Dataset',
            connectionId: advancedOracleConnection.connectorid,
            connectorType: advancedOracleConnection.connectorType,
            database: CONNECTIONS.oracle.databases.general.name,
            tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
        });

        return composerHelperItem;

    }

    /**
     *
     * @param {*} userId
     * @param {Object} appDetails Application details
     * @param {string} appDetails.name Application name
     * @param {string} [appDetails.description] Application description
     */
    static async postDataApp(userId, appDetails) {
        const composerHelperItem = new ComposerHelper(userId);
        if (!appDetails) throw new Error('Missing app details');
        if (!appDetails.name) throw new Error('App name is required');

        const applicationItem = await ApplicationService.createApplication(userId, appDetails);
        composerHelperItem.appId = applicationItem.appid;

        const mysqlConnection = await composerHelperItem.createMysqlConnection('Mysql connection');

        const ESdataset = composerHelperItem.createDatasetFromConnection('Mysql Dataset', {
            name: 'Mysql Dataset',
            connectionId: mysqlConnection.connectorid,
            connectorType: mysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        }, { load: true, wait: true });

        const LIVEdataset = composerHelperItem.createDatasetFromConnection('Snowflake Dataset', {
            name: 'Snowflake Dataset',
            connectionId: mysqlConnection.connectorid,
            connectorType: mysqlConnection.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        }, { load: true, wait: true });

        await Promise.all([ESdataset, LIVEdataset])

        return composerHelperItem;
    }

    /**
     * Create mysql connection
     * @param {string} connectionName
     * @param {Object} options
     * @param {boolean} [options.advanced=false] Create advanced connection, false by default
     * @returns {Object} Connection details
     */
    async createMysqlConnection(connectionName, options = {}) {
        const isAdvancedConnection = (options && options.advanced) ? options.advanced : false;
        let self = this;

        let connectionDetails = {
            name: connectionName,
            connectorType: CONNECTIONS.mysql.connectorType,
            port: CONNECTIONS.mysql.port,
        };

        if (isAdvancedConnection === false) {
            connectionDetails = {
                ...connectionDetails,
                host: CONNECTIONS.mysql.host,
                user: CONNECTIONS.mysql.user,
                password: CONNECTIONS.mysql.password,
            };
        } else {
            connectionDetails = {
                ...connectionDetails,
                configType: 'advanced',
                connectionConfig: {
                    host: CONNECTIONS.mysql.host,
                    user: CONNECTIONS.mysql.user,
                    password: CONNECTIONS.mysql.password,
                },
            };
        }
        let connectionItem = await ConnectionService.createConnection(self.userId, self.appId, connectionDetails);

        self.addConnection({
            connectionId: connectionItem.connectorid,
            name: connectionItem.name,
        });

        return connectionItem;
    }

    /**
     * Create mssql connection
     * @param {string} connectionName
     * @param {Object} options
     * @param {boolean} [options.advanced=false] Create advanced connection, false by default
     * @returns {Object} Connection details
     */
    async createMssqlConnection(connectionName, options = {}) {
        const isAdvancedConnection = (options && options.advanced) ? options.advanced : false;
        let self = this;

        let connectionDetails = {
            name: connectionName,
            connectorType: CONNECTIONS.mssql.connectorType,
            port: CONNECTIONS.mssql.port,
        };

        if (isAdvancedConnection === false) {
            connectionDetails = {
                ...connectionDetails,
                host: CONNECTIONS.mssql.host,
                password: CONNECTIONS.mssql.password,
                user: CONNECTIONS.mssql.user,
            };
        } else {
            connectionDetails = {
                ...connectionDetails,
                configType: 'advanced',
                connectionConfig: {
                    server: CONNECTIONS.mssql.host,
                    password: CONNECTIONS.mssql.password,
                    user: CONNECTIONS.mssql.user,
                }
            };
        }
        let connectionItem = await ConnectionService.createConnection(self.userId, self.appId, connectionDetails);

        self.addConnection({
            connectionId: connectionItem.connectorid,
            name: connectionItem.name,
        });

        return connectionItem;
    }

    /**
     * Create oracle connection
     * @param {string} connectionName
     * @param {Object} options
     * @param {boolean} [options.advanced=false] Create advanced connection, false by default
     * @returns {Object} Connection details
     */
    async createOracleConnection(connectionName, options) {
        const isAdvancedConnection = (options && options.advanced) ? options.advanced : false;
        let self = this;
        let connectionDetails = {
            name: connectionName,
            connectorType: CONNECTIONS.oracle.connectorType,
        };

        if (isAdvancedConnection === false) {
            connectionDetails = {
                ...connectionDetails,
                port: CONNECTIONS.oracle.port,
                host: CONNECTIONS.oracle.host,
                password: CONNECTIONS.oracle.password,
                user: CONNECTIONS.oracle.user,
                serviceName: CONNECTIONS.oracle.serviceName,
            };
        } else {
            connectionDetails = {
                ...connectionDetails,
                configType: 'advanced',
                connectionConfig: {
                    connectString: `${CONNECTIONS.oracle.host}:${CONNECTIONS.oracle.port}/${CONNECTIONS.oracle.serviceName}`,
                    password: CONNECTIONS.oracle.password,
                    user: CONNECTIONS.oracle.user,
                }
            };
        }

        let connectionItem = await ConnectionService.createConnection(self.userId, self.appId, connectionDetails);

        self.addConnection({
            connectionId: connectionItem.connectorid,
            name: connectionItem.name,
        });

        return connectionItem;
    }

    /**
     * Create athena connection
     * @param {string} connectionName
     * @returns {Object} Connection details
     */
    async createAthenaConnection(connectionName) {
        let self = this;
        let connectionDetails = {
            name: connectionName,

            accessKey: CONNECTIONS.athena.user,
            awsRegion: CONNECTIONS.athena.awsRegion,
            catalogName: CONNECTIONS.athena.catalogName,
            connectorType: CONNECTIONS.athena.connectorType,
            hasHeader: true,

            secretKey: CONNECTIONS.athena.password,
            workGroup: CONNECTIONS.athena.workGroup,

            outputLocation: CONNECTIONS.athena.outputLocation,
        };

        let connectionItem = await ConnectionService.createConnection(self.userId, self.appId, connectionDetails);

        self.addConnection({
            connectionId: connectionItem.connectorid,
            name: connectionItem.name,
        });

        return connectionItem;
    }

    /**
     * Create dataset from connection
     * @param {string} datasetName Dataset name
     * @param {Object} body Datasource information
     * @param {Object} options General options
     * @param {Boolean} [options.load=false] True if you want to load the dataset as soon as it is created
     * @param {Boolean} [options.wait=false] True if you want to load and wait for the process to being done
     * @param {object[]} [options.securityColumns] Security columns
     * @param {string} [options.securityColumns[].columnName] Security column name
     * @param {string} [options.securityColumns[].securityName] Security name
     * @param {Object} [params] Request params
     */
    async createDatasetFromConnection(datasetName, body, options = { load: false }, params) {
        let self = this;
        let datasetItem = await DatasetService.createDatasetFromDataSource(self.userId, self.appId, body, params);

        if (Array.isArray(options?.securityColumns)) {
            for (let securityColumn of options.securityColumns) {
                let column = _find(datasetItem.columns, { columnName: securityColumn.columnName });
                column.securityName = securityColumn.securityName;
            }
        }

        datasetItem = await DatasetService.updateDataset(self.userId, self.appId, datasetItem.datasetId, { ...datasetItem, name: datasetName }, params);
        if (options.load === true) {
            await DatasetHelper.loadDataset(self.userId, self.appId, datasetItem.datasetId, options, params);
        }

        self.addDataset({
            datasetId: datasetItem.datasetId,
            name: datasetName,
        });

        return DatasetService.getDataset(self.userId, self.appId, datasetItem.datasetId, params);
    }

    /**
     * Create dataset from connection
     * @param {string} datasetName Dataset name
     * @param {string} connectionId Parent connection id associated to the shared dataset.
     * @param {Object} body Datasource information
     * @param {Object} options General options
     * @param {Boolean} [options.load=false] True if you want to load the dataset as soon as it is created
     * @param {Boolean} [options.wait=false] True if you want to load and wait for the process to being done
     * @param {object[]} [options.securityColumns] Security columns
     * @param {string} [options.securityColumns[].columnName] Security column name
     * @param {string} [options.securityColumns[].securityName] Security name
     * @param {Object} [params] Request params
     */
    async createDatasetView(datasetName, connectionId, body, options, params) {
        let self = this;
        let datasetItem = await DatasetService.createDatasetView(self.userId, self.appId, connectionId, body, params);

        if (Array.isArray(options?.securityColumns)) {
            for (let securityColumn of options.securityColumns) {
                let column = _find(datasetItem.columns, { columnName: securityColumn.columnName });
                column.securityName = securityColumn.securityName;
            }
        }

        datasetItem = await DatasetService.updateDataset(self.userId, self.appId, datasetItem.datasetId, { ...datasetItem, name: datasetName }, params);

        self.addDataset({
            datasetId: datasetItem.datasetId,
            name: datasetName,
        });

        return DatasetService.getDataset(self.userId, self.appId, datasetItem.datasetId, params);
    }

    /**
     * Delete all
     * @param {string} connectionName
     * @returns {Object} Connection details
     */
    async deleteAll() {
        if (this.isPreloaded) {
            return { message: 'Preload application could not be deleted' }
        }

        let self = this;
        await self.deleteConnections();
        await ApplicationService.deleteApplication(self.userId, self.appId);

        return {
            message: `The app ${self.appId} was successfully deleted`,
        };
    }

    /**
     * Delete all connections
     * @returns {Object} Connection details
     */
    async deleteConnections() {
        if (this.isPreloaded) {
            return { message: 'Preload application could not be deleted' }
        }

        let self = this;
        for (let connectionItem of self.connections) {
            await ConnectionService.deleteConnection(self.userId, self.appId, connectionItem.connectionId);
        }

        return {
            message: 'Connections were deleted',
        };
    }

    /**
     * Get All Connections
     * @returns {Object} Connection list
     */
    async getAllConnections() {
        const userId = this.userId;
        const appId = this.appId
        const connections = await ConnectionService.getConnections(userId, appId);
        return connections 
    }

    /**
     * Get All Datasets
     * @returns {Object} Dataset list
     */
    async getAllDatasets() {
        const userId = this.userId;
        const appId = this.appId;
        const body = {
            "projectionExpression": [
                "rowCount",
                "status",
                "datasources",
                "modifyDate",
                "confirmedLoadedStatusTime",
                "datasetId",
                "columnsCount",
                "userEmail",
                "statusVersion",
                "createDate",
                "appId",
                "lastStatusFailed",
                "datasetType",
                "scope",
                "favorite",
                "isPublic"
            ],
            "nestedProjectionExpression": {
                "dataSync": {
                    "dataSyncStatus": true,
                    "dataSyncFrequency": true
                }
            },
            "filteredProperties": "datasources",
            "limit": 40,
            "warning": false
        };
        
        const datasets = await DatasetService.getDatasets(userId, appId, body);
        return datasets;
    }
    
    /**
     * Get All Application by userId
     * @returns {object} Application list
     */
    async getAllApplications() {
        const userId = this.userId;
        const applications = await ApplicationService.getApplications(userId);
        return applications;
    }
    /**
     * Get a Application by userId and AppId
     * @returns {object} Application by Id 
     */
    async getApplicationById() {
        const userId = this.userId;
        const appId = this.appId;
        const application = await ApplicationService.getApplication(userId, appId);
        return application;
    }

    async publishDataset() {
        const self = this;
        const userId = self.userId;
        const appId = self.appId;
        const datasetId = self.datasetId;
        const datasetDetail = {
            "isPublic": true
        };

        const datasetPublished = await DatasetService.updatePublishedDatasetVersion(userId, appId, datasetId, datasetDetail);
        return datasetPublished;
    }

    
    async createContentAndDataApplication() {
        const self = this;
        const userId = self.userId;
        const appDetails = {
            name: `App Data ${chance.animal()}`,
            description: 'Application Data, created by automated test'
        };
        let applicationData = await ApplicationService.createApplication(userId, appDetails);
        self.appId = applicationData.appid;

        const connection = await self.createMysqlConnection('Mysql connection');
        const connectionId = connection.connectorid;
        const datasetName = `Dataset data ${chance.natural()}`
        let dataset = await self.createDatasetFromConnection(datasetName, {
            name: datasetName,
            connectionId: connectionId,
            connectorType: CONNECTIONS.mysql.connectorType,
            database: CONNECTIONS.mysql.databases.general.name,
            tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
        }, {
            load: true, wait: true
        });
        self.datasetId = dataset.datasetId;
        const isShared = await self.publishDataset();
        
        const parentDatasetData = isShared.isPublic ? {
            appId: self.appId,
            userId: self.userId,
        }: {};
        const internalConnectionId = dataset.connectionId;
        const appDetail2 = {
            name: `App Content ${chance.animal()}`,
            description: 'Application Content, created by automated test'
        };
        
        let applicationContent = await ApplicationService.createApplication(userId, appDetail2);
        self.appId = applicationContent.appid;
        const datasetViewName = `Dataset Content ${chance.natural()}`
        const datasetView = await self.createDatasetView(datasetViewName, internalConnectionId, parentDatasetData);
        if(!datasetView) throw new Error(`It wasn't possible to create a dataset view`);  
        return {applicationData, applicationContent};
    }

    /**
     * Add records to a dataset type Push API
     * @param {object} data Data to add in dataset type Push API 
     */
    async addRecordToPushAPI(data){
        const self = this;
        const userId = self.userId;
        const appId = self.appId;
        const datasetId = 'wgosrcipo'
        const payload = {datasetId, data: [data]};
        const pushApi = await DatasetService.addPushAPIRecord(userId, appId, datasetId, payload);
    }
    
    async getAllPages() {
        const self = this;
        const userId = self.userId;
        const appId = self.appId;
        const pages = await PageService.getPages(userId, appId);
        return pages;
    }
}

module.exports = ComposerHelper;
