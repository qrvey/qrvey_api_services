// General composer data
const VERSION =  process.env.VERSION;
const GENERAL_API_KEY = process.env.GENERAL_API_KEY;
const GENERAL_USER_ID = process.env.GENERAL_USER_ID;
const GENERAL_APP_ID = process.env.GENERAL_APP_ID;
const GENERAL_COMPOSER_USER_PASSWORD = process.env.GENERAL_COMPOSER_USER_PASSWORD;
const GENERAL_COMPOSER_USER_EMAIL = process.env.GENERAL_COMPOSER_USER_EMAIL;

// General admin center data
const ADMIN_BACKEND_DOMAIN = process.env.ADMIN_BACKEND_DOMAIN;
const GENERAL_ADMIN_USER_USERNAME = process.env.ADMIN_USER;
const GENERAL_ADMIN_USER_PASSWORD = process.env.ADMIN_PASSWORD;

const SERVERS = {
    general: {
        name: 'Server: General',
        domain: process.env.COMPOSER_DOMAIN,
        apiKey: process.env.COMPOSER_API_KEY,
    },
    manual: {
        name: 'Server: Manual stg',
        domain: process.env.MANUAL_STG_COMPOSER_DOMAIN,
        apiKey: process.env.MANUAL_STG_COMPOSER_API_KEY,
    },
    oemStg: {
        name: 'Server: oem stg',
        domain: process.env.OEM_STG_COMPOSER_DOMAIN,
        apiKey: process.env.OEM_STG_COMPOSER_API_KEY,
    },
    contentDeployment1: {
        name: 'Server: Content deployment',
        domain: process.env.CD_1_COMPOSER_DOMAIN,
        apiKey: process.env.CD_1_COMPOSER_API_KEY,
    },
    adminQA: {
        name: 'Server: Admin QA',
        domain: process.env.ADMIN_QA_COMPOSER_DOMAIN,
        apiKey: process.env.ADMIN_QA_COMPOSER_API_KEY,
    },
};

// WRONG DATA
const WRONG_API_KEY = 'WRONG_API_KEY';
const WRONG_APP_ID = 'WRONG_APP_ID';
const WRONG_USER_ID = 'WRONG_USER_ID';

// Connection data

const CONNECTIONS = {
    mysql: {
        host: process.env.MYSQL_HOST,
        port: parseInt(process.env.MYSQL_DEFAULT_PORT),
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        connectorType: 'MYSQL_LIVE',
        databases: {
            general: {
                name: process.env.MYSQL_GENERAL_DATABASE_NAME,
                tables: {
                    wineQuality: {
                        tableName: 'Wine_Quality',
                    },
                    clients: {
                        tableName: 'clients',
                    },
                    orders: {
                        tableName: 'orders',
                    }
                },
            },
        },
    },
    mssql: {
        host: process.env.MSSQL_HOST,
        port: parseInt(process.env.MSSQL_DEFAULT_PORT),
        user: process.env.MSSQL_USER,
        password: process.env.MSSQL_PASSWORD,
        connectorType: 'MS_SQL_LIVE',
        databases: {
            general: {
                name: process.env.MSSQL_GENERAL_DATABASE_NAME,
                tables: {
                    wineQuality: {
                        tableName: 'Wine_Quality',
                        tableSchema: 'dbo',
                    }
                },
            }
        },
    },
    oracle: {
        host: process.env.ORACLE_HOST,
        port: parseInt(process.env.ORACLE_DEFAULT_PORT),
        user: process.env.ORACLE_USER,
        password: process.env.ORACLE_PASSWORD,
        serviceName: process.env.ORACLE_SERVICE_NAME,
        connectString: `${process.env.ORACLE_HOST}:${process.env.ORACLE_DEFAULT_PORT}/${process.env.ORACLE_SERVICE_NAME}`,
        connectorType: 'ORACLE_LIVE',
        databases: {
            general: {
                name: process.env.ORACLE_GENERAL_DATABASE_NAME,
                tables: {
                    wineQuality: {
                        tableName: 'Wine_Quality'
                    },
                },
            }
        },
    },
    athena: {
        awsRegion: process.env.ATHENA_GENERAL_REGION,
        connectorType: 'ATHENA',
        outputLocation: process.env.ATHENA_GENERAL_OUTPUT_LOCATION,
        password: process.env.ATHENA_PASSWORD,
        user: process.env.ATHENA_USER,
        workGroup: process.env.ATHENA_GENERAL_WORKGROUP,

        databases: {
            general: {
                name: process.env.ATHENA_GENERAL_DATABASE,
                catalogName: process.env.ATHENA_GENERAL_CATALOG_NAME,
                tables: {
                    client: {
                        tableName: 'client',
                        tableKey: 'client',
                        hasHeader: true,
                    }
                }
            }
        }
    }
};

const REPEAT_SCENARIO = process.env.REPEAT_SCENARIO
module.exports = {
    VERSION,

    ADMIN_BACKEND_DOMAIN,
    GENERAL_COMPOSER_USER_PASSWORD,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    GENERAL_COMPOSER_USER_EMAIL,

    SERVERS,

    GENERAL_USER_ID,
    GENERAL_APP_ID,
    GENERAL_API_KEY,

    WRONG_API_KEY,
    WRONG_APP_ID,
    WRONG_USER_ID,

    CONNECTIONS,

    REPEAT_SCENARIO
};
