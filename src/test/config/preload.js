const COMPOSER = {
    userId: 'cAtOT2t',
    appId: 'uav5wyKkE', // AppName: "General app 🤚🏼 don't delete this!"
    connections: [
        {
            connectionId: '8qJB_DSRO',
            name: 'MySQL'
        },
        {
            connectionId: 'o6ld0Ic3B',
            name: 'SNOWFLAKE'
        },
        {
            connectionId: 'BIyWQkLkL',
            name: 'REDSHIFT'
        },
        {
            connectionId: 'eo0sFnAEB',
            name: 'PostgreSQL'
        },
    ],
    datasets: [
        {
            datasetId: 'EsKEspBD7t',
            name: 'Managed'
        },
        {
            datasetId: 'd6tf4oaxm',
            name: 'Snowflake'
        },
        {
            datasetId: 'gnctcbipu',
            name: 'Redshift'
        },
        {
            datasetId: 'xtikt7qhw',
            name: 'Postgresql'
        },
    ]
}



module.exports = {
    COMPOSER
}
