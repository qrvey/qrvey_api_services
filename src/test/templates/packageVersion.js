const getBasicPackageVersionTemplate = () => {
    return {
        dependencies: {
            datasets: [
                {
                    updateDate: '2021-12-04T15:56:12.000Z',
                    name: 'Oracle Dataset',
                    datasetId: 'KBa5RLgiA',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:12.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'ORACLE_LIVE',
                                database: 'QAUSER',
                                name: 'Mysql Dataset',
                                datasourceId: 'OcOZV06w1',
                                connectionId: 'ufKvjd0vo',
                                __connectorTypeSaved: 'true',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:10.000Z',
                    name: 'Mssql Dataset',
                    datasetId: 'aD6QgYxZc',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:10.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'MS_SQL_LIVE',
                                database: 'QA_Database',
                                name: 'Mysql Dataset',
                                datasourceId: 'sUwjP1U2i',
                                connectionId: 'CR7RmRLKk',
                                __connectorTypeSaved: 'true',
                                tableSchema: 'dbo',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:08.000Z',
                    name: 'Athena Dataset',
                    datasetId: 'rLHBebTeR',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:07.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                catalogName: 'AwsDataCatalog',
                                connectorType: 'ATHENA',
                                database: 'backend_test',
                                name: 'Mysql Dataset',
                                datasourceId: 'G2HWMDg8p',
                                connectionId: 'D1cgJUE41',
                                __connectorTypeSaved: 'true',
                                tableName: 'client'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:05.000Z',
                    name: 'Mysql Dataset',
                    datasetId: 'inc8DecVB',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:04.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'MYSQL_LIVE',
                                database: 'northwind',
                                name: 'Mysql Dataset',
                                datasourceId: 'AiiYDKrUC',
                                connectionId: 'lnHSWbNYi',
                                __connectorTypeSaved: 'true',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                }
            ],
            flows: [],
            connections: [
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1433,
                    name: 'Athena connection',
                    connectionId: 'CR7RmRLKk',
                    hostUrl: 'myhost.com',
                    userName: 'user1',
                    connectionQueryType: 'none',
                    connectionType: 'MS_SQL_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    outputLocation: 's3://bucker/path/',
                    awsRegion: 'us-east-1',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    hasHeader: true,
                    workGroup: 'primary',
                    secretKey: 'SECRET_KEY',
                    connectionType: 'ATHENA',
                    searchSubFolders: false,
                    accessKey: 'ACCESS_KEY',
                    name: 'Athena connection',
                    connectionId: 'D1cgJUE41',
                    connectionQueryType: 'none',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1521,
                    name: 'Athena connection',
                    connectionId: 'ufKvjd0vo',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    serviceName: 'xe',
                    connectionType: 'ORACLE_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:55:59.000Z',
                    searchSubFolders: false,
                    port: 3306,
                    name: 'Mysql connection',
                    connectionId: 'lnHSWbNYi',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    connectionType: 'MYSQL_LIVE',
                    createDate: '2021-12-04T15:55:59.000Z'
                }
            ],
            forms: [],
            datalinks: []
        },
        nameFilter: 'v1',
        updateDate: '2021-12-04T15:56:30.044Z',
        version: 1,
        packageVersionId: 'DmHAN_-oWe',
        contentPackageVersion: {
            datasets: [
                {
                    updateDate: '2021-12-04T15:56:12.000Z',
                    name: 'Oracle Dataset',
                    datasetId: 'KBa5RLgiA',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:12.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false
                },
                {
                    updateDate: '2021-12-04T15:56:10.000Z',
                    name: 'Mssql Dataset',
                    datasetId: 'aD6QgYxZc',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:10.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false
                },
                {
                    updateDate: '2021-12-04T15:56:08.000Z',
                    name: 'Athena Dataset',
                    datasetId: 'rLHBebTeR',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:07.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false
                },
                {
                    updateDate: '2021-12-04T15:56:05.000Z',
                    name: 'Mysql Dataset',
                    datasetId: 'inc8DecVB',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:04.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false
                }
            ],
            flows: [],
            connections: [
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1433,
                    name: 'Athena connection',
                    connectionId: 'CR7RmRLKk',
                    hostUrl: 'myhost.com',
                    userName: 'user1',
                    connectionQueryType: 'none',
                    connectionType: 'MS_SQL_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    outputLocation: 's3://bucker/path/',
                    awsRegion: 'us-east-1',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    hasHeader: true,
                    workGroup: 'primary',
                    secretKey: 'SECRET_KEY',
                    connectionType: 'ATHENA',
                    searchSubFolders: false,
                    accessKey: 'ACCESS_KEY',
                    name: 'Athena connection',
                    connectionId: 'D1cgJUE41',
                    connectionQueryType: 'none',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1521,
                    name: 'Athena connection',
                    connectionId: 'ufKvjd0vo',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    serviceName: 'xe',
                    connectionType: 'ORACLE_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:55:59.000Z',
                    searchSubFolders: false,
                    port: 3306,
                    name: 'Mysql connection',
                    connectionId: 'lnHSWbNYi',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    connectionType: 'MYSQL_LIVE',
                    createDate: '2021-12-04T15:55:59.000Z'
                }
            ],
            datalinks: []
        },
        packageName: 'PK - CD2.0 basic app 1638633354607',
        name: 'v1',
        exportedApp: {
            uploadResult: {
                Bucket: 'my_bucket',
                Key: 'my/path/file.zip'
            },
            userInfo: {
                email: 'alberto.valle+test@qrvey.com'
            },
            appInfo: {
                originalAppName: 'CD2.0 basic app 1638633354607',
                reports: 0,
                originAppid: 'r6SkuOOdc',
                pages: 0,
                currentProcessed: 0,
                datalinks: [],
                name: 'CD2.0 basic app 1638633354607',
                description: '',
                qrveys: 0,
                metrics: 0,
                workflows: 0,
                totalToProcess: '0'
            }
        },
        nameLower: 'v1',
        latestVersion: true,
        createDate: '2021-12-04T15:56:30.044Z',
        packageId: '4VQjw7_Dmz'
    };
};

const getSharedDatasetPackageVersionTemplate = () => {
    return {
        dependencies: {
            charts: [
                {
                    updateDate: '2021-12-13T16:24:46.210Z',
                    qrveyid: 'GePlAccao',
                    chartId: 'YBxLmdhz9',
                    name: 'Bar Chart',
                    title: 'Bar Chart',
                    type: 'BAR_CHART',
                    createDate: '2021-12-13T16:24:46.210Z',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql with shared data',
                                datasetId: 'GePlAccao'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-13T16:25:20.829Z',
                    qrveyid: 'OEpCUmM8c',
                    chartId: 'EeN5573r4',
                    name: 'Bar Chart',
                    title: 'Bar Chart',
                    type: 'BAR_CHART',
                    createDate: '2021-12-13T16:25:20.829Z',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql Dataset - View',
                                datasetId: 'OEpCUmM8c'
                            }
                        ]
                    }
                }
            ],
            datasets: [
                {
                    updateDate: '2021-12-13T16:24:54.000Z',
                    charts: [
                        {
                            updateDate: '2021-12-13T16:25:20.829Z',
                            qrveyid: 'OEpCUmM8c',
                            chartId: 'EeN5573r4',
                            name: 'Bar Chart',
                            title: 'Bar Chart',
                            type: 'BAR_CHART',
                            createDate: '2021-12-13T16:25:20.829Z'
                        }
                    ],
                    isView: true,
                    indexName: 'dataset_7ngai3v98_jxe5ohvvd',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql Dataset',
                                hasLocalPublicParentDataset: false,
                                datasetId: '7NGAI3V98'
                            }
                        ]
                    },
                    hasPublicParent: true,
                    datasources: [
                        {
                            name: 'Mysql Dataset',
                            datasourceId: 'BGLBWoWWn',
                            connectionId: 'VvT0EG5JT',
                            connectorType: 'ELASTICSEARCH',
                            __connectorTypeSaved: 'true'
                        }
                    ],
                    name: 'Mysql Dataset - View',
                    datasetId: 'OEpCUmM8c',
                    connectionId: 'VvT0EG5JT',
                    isPublic: false,
                    datasetType: 'DATASET_VIEW',
                    parentConnection: {
                        connectionId: 'FQWBlGAOz',
                        userId: 'aiv0QbkP4',
                        scope: 'PUBLIC',
                        appId: '6Z72p8TFa'
                    },
                    createDate: '2021-12-13T16:09:19Z',
                    status: 'LOADED'
                },
                {
                    updateDate: '2021-12-13T16:24:13.000Z',
                    charts: [
                        {
                            updateDate: '2021-12-13T16:24:46.210Z',
                            qrveyid: 'GePlAccao',
                            chartId: 'YBxLmdhz9',
                            name: 'Bar Chart',
                            title: 'Bar Chart',
                            type: 'BAR_CHART',
                            createDate: '2021-12-13T16:24:46.210Z'
                        }
                    ],
                    indexName: 'dataset_geplaccao_ufrjg2x1g',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql Dataset',
                                hasLocalPublicParentDataset: false,
                                datasetId: '7NGAI3V98'
                            }
                        ]
                    },
                    hasPublicParent: true,
                    datasources: [
                        {
                            name: 'Mysql Dataset',
                            datasourceId: 'kOC5jv3th',
                            connectionId: 'Eqf8Sxvvz',
                            __connectorTypeSaved: 'true'
                        }
                    ],
                    name: 'Mysql with shared data',
                    datasetId: 'GePlAccao',
                    connectionId: 'DoSxGwO8s',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-13T16:12:07Z',
                    status: 'LOADED'
                },
                {
                    name: 'Mysql Dataset',
                    hasLocalPublicParentDataset: false,
                    datasetId: '7NGAI3V98'
                }
            ],
            flows: [],
            connections: [
                {
                    updateDate: '2021-12-13T16:24:10.000Z',
                    searchSubFolders: false,
                    name: 'Mysql with shared data',
                    connectionId: 'DoSxGwO8s',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:15:18.000Z'
                },
                {
                    updateDate: '2021-12-13T16:06:48.000Z',
                    scope: 'PUBLIC',
                    name: 'Mysql Dataset',
                    connectionId: 'FQWBlGAOz',
                    connectionType: 'ELASTICSEARCH',
                    publicParentId: '7NGAI3V98',
                    createDate: '2021-12-13T15:41:13.000Z'
                },
                {
                    updateDate: '2021-12-13T16:12:06.000Z',
                    searchSubFolders: false,
                    name: 'Mysql Dataset',
                    connectionId: 'Eqf8Sxvvz',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:12:06.000Z'
                },
                {
                    updateDate: '2021-12-13T16:09:18.000Z',
                    searchSubFolders: false,
                    name: 'Mysql Dataset',
                    connectionId: 'VvT0EG5JT',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:09:18.000Z'
                }
            ],
            forms: [],
            datalinks: []
        },
        nameFilter: 'v1',
        updateDate: '2021-12-13T16:29:46.968Z',
        version: 1,
        packageVersionId: 'Q3YLQ2FIfI',
        contentPackageVersion: {
            datasets: [
                {
                    updateDate: '2021-12-13T16:24:54.000Z',
                    charts: [
                        {
                            updateDate: '2021-12-13T16:25:20.829Z',
                            qrveyid: 'OEpCUmM8c',
                            chartId: 'EeN5573r4',
                            name: 'Bar Chart',
                            title: 'Bar Chart',
                            type: 'BAR_CHART',
                            createDate: '2021-12-13T16:25:20.829Z'
                        }
                    ],
                    isView: true,
                    indexName: 'dataset_7ngai3v98_jxe5ohvvd',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql Dataset',
                                hasLocalPublicParentDataset: false,
                                datasetId: '7NGAI3V98'
                            }
                        ]
                    },
                    hasPublicParent: true,
                    name: 'Mysql Dataset - View',
                    datasetId: 'OEpCUmM8c',
                    connectionId: 'VvT0EG5JT',
                    isPublic: false,
                    datasetType: 'DATASET_VIEW',
                    parentConnection: {
                        connectionId: 'FQWBlGAOz',
                        userId: 'aiv0QbkP4',
                        scope: 'PUBLIC',
                        appId: '6Z72p8TFa'
                    },
                    createDate: '2021-12-13T16:09:19Z',
                    status: 'LOADED'
                },
                {
                    updateDate: '2021-12-13T16:24:13.000Z',
                    charts: [
                        {
                            updateDate: '2021-12-13T16:24:46.210Z',
                            qrveyid: 'GePlAccao',
                            chartId: 'YBxLmdhz9',
                            name: 'Bar Chart',
                            title: 'Bar Chart',
                            type: 'BAR_CHART',
                            createDate: '2021-12-13T16:24:46.210Z'
                        }
                    ],
                    indexName: 'dataset_geplaccao_ufrjg2x1g',
                    name: 'Mysql with shared data',
                    datasetId: 'GePlAccao',
                    connectionId: 'DoSxGwO8s',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-13T16:12:07Z',
                    status: 'LOADED',
                    dependencies: {
                        datasets: [
                            {
                                name: 'Mysql Dataset',
                                hasLocalPublicParentDataset: false,
                                datasetId: '7NGAI3V98'
                            }
                        ]
                    },
                    hasPublicParent: true
                }
            ],
            flows: [],
            connections: [
                {
                    updateDate: '2021-12-13T16:24:10.000Z',
                    searchSubFolders: false,
                    name: 'Mysql with shared data',
                    connectionId: 'DoSxGwO8s',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:15:18.000Z'
                },
                {
                    updateDate: '2021-12-13T16:06:48.000Z',
                    scope: 'PUBLIC',
                    name: 'Mysql Dataset',
                    connectionId: 'FQWBlGAOz',
                    connectionType: 'ELASTICSEARCH',
                    publicParentId: '7NGAI3V98',
                    createDate: '2021-12-13T15:41:13.000Z'
                },
                {
                    updateDate: '2021-12-13T16:12:06.000Z',
                    searchSubFolders: false,
                    name: 'Mysql Dataset',
                    connectionId: 'Eqf8Sxvvz',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:12:06.000Z'
                },
                {
                    updateDate: '2021-12-13T16:09:18.000Z',
                    searchSubFolders: false,
                    name: 'Mysql Dataset',
                    connectionId: 'VvT0EG5JT',
                    connectionQueryType: 'none',
                    connectionType: 'ELASTICSEARCH',
                    createDate: '2021-12-13T16:09:18.000Z'
                }
            ],
            datalinks: []
        },
        packageName: 'PK - My new app',
        name: 'v1',
        exportedApp: {
            uploadResult: {
                Bucket: 'my_bucker',
                Key: 'my/path/file.zip'
            },
            userInfo: {
                email: 'alberto.valle+cx@qrvey.com'
            },
            appInfo: {
                originalAppName: 'New App',
                reports: 0,
                originAppid: 'mBLnFbiGA',
                pages: 0,
                currentProcessed: 0,
                datalinks: [],
                name: 'New App',
                description: '',
                qrveys: 2,
                metrics: 0,
                workflows: 0,
                totalToProcess: 2
            }
        },
        nameLower: 'v1',
        latestVersion: true,
        createDate: '2021-12-13T16:29:46.968Z',
        packageId: 'eiTx_XPI5'
    };
};

const getInvalidPackageVersionTemplate = () => {
    return {
        dependencies: {
            datasets: [
                {
                    updateDate: '2021-12-04T15:56:12.000Z',
                    name: 'Oracle Dataset',
                    datasetId: 'KBa5RLgiA',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:12.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'ORACLE_LIVE',
                                database: 'QAUSER',
                                name: 'Mysql Dataset',
                                datasourceId: 'OcOZV06w1',
                                connectionId: 'ufKvjd0vo',
                                __connectorTypeSaved: 'true',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:10.000Z',
                    name: 'Mssql Dataset',
                    datasetId: 'aD6QgYxZc',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:10.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'MS_SQL_LIVE',
                                database: 'QA_Database',
                                name: 'Mysql Dataset',
                                datasourceId: 'sUwjP1U2i',
                                connectionId: 'CR7RmRLKk',
                                __connectorTypeSaved: 'true',
                                tableSchema: 'dbo',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:08.000Z',
                    name: 'Athena Dataset',
                    datasetId: 'rLHBebTeR',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:07.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                catalogName: 'AwsDataCatalog',
                                connectorType: 'ATHENA',
                                database: 'backend_test',
                                name: 'Mysql Dataset',
                                datasourceId: 'G2HWMDg8p',
                                connectionId: 'D1cgJUE41',
                                __connectorTypeSaved: 'true',
                                tableName: 'client'
                            }
                        ]
                    }
                },
                {
                    updateDate: '2021-12-04T15:56:05.000Z',
                    name: 'Mysql Dataset',
                    datasetId: 'inc8DecVB',
                    isPublic: false,
                    datasetType: 'DATASET',
                    createDate: '2021-12-04T15:56:04.000Z',
                    status: 'DRAFT',
                    hasPublicParent: false,
                    dependencies: {
                        connections: [
                            {
                                connectorType: 'MYSQL_LIVE',
                                database: 'northwind',
                                name: 'Mysql Dataset',
                                datasourceId: 'AiiYDKrUC',
                                connectionId: 'lnHSWbNYi',
                                __connectorTypeSaved: 'true',
                                tableName: 'Wine_Quality'
                            }
                        ]
                    }
                }
            ],
            flows: [],
            connections: [
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1433,
                    name: 'Athena connection',
                    connectionId: 'CR7RmRLKk',
                    hostUrl: 'myhost.com',
                    userName: 'user1',
                    connectionQueryType: 'none',
                    connectionType: 'MS_SQL_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    outputLocation: 's3://bucker/path/',
                    awsRegion: 'us-east-1',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    hasHeader: true,
                    workGroup: 'primary',
                    secretKey: 'SECRET_KEY',
                    connectionType: 'ATHENA',
                    searchSubFolders: false,
                    accessKey: 'ACCESS_KEY',
                    name: 'Athena connection',
                    connectionId: 'D1cgJUE41',
                    connectionQueryType: 'none',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:56:00.000Z',
                    searchSubFolders: false,
                    port: 1521,
                    name: 'Athena connection',
                    connectionId: 'ufKvjd0vo',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    serviceName: 'xe',
                    connectionType: 'ORACLE_LIVE',
                    createDate: '2021-12-04T15:56:00.000Z'
                },
                {
                    password: 'PASSWORD',
                    updateDate: '2021-12-04T15:55:59.000Z',
                    searchSubFolders: false,
                    port: 3306,
                    name: 'Mysql connection',
                    connectionId: 'lnHSWbNYi',
                    hostUrl: 'myhost.com',
                    userName: 'user',
                    connectionQueryType: 'none',
                    connectionType: 'MYSQL_LIVE',
                    createDate: '2021-12-04T15:55:59.000Z'
                }
            ],
            forms: [],
            datalinks: []
        },
        nameFilter: 'v1',
        updateDate: '2021-12-04T15:56:30.044Z',
        version: 1,
        packageVersionId: 'DmHAN_-oWe',
        packageName: 'PK - CD2.0 basic app 1638633354607',
        name: 'v1',
        exportedApp: {
            uploadResult: {
                Bucket: 'my_bucket',
                Key: 'my/path/file.zip'
            },
            userInfo: {
                email: 'alberto.valle+test@qrvey.com'
            },
            appInfo: {
                originalAppName: 'CD2.0 basic app 1638633354607',
                reports: 0,
                originAppid: 'r6SkuOOdc',
                pages: 0,
                currentProcessed: 0,
                datalinks: [],
                name: 'CD2.0 basic app 1638633354607',
                description: '',
                qrveys: 0,
                metrics: 0,
                workflows: 0,
                totalToProcess: '0'
            }
        },
        nameLower: 'v1',
        latestVersion: true,
        createDate: '2021-12-04T15:56:30.044Z',
        packageId: '4VQjw7_Dmz'
    };
};

module.exports = {
    getInvalidPackageVersionTemplate,
    getBasicPackageVersionTemplate,
    getSharedDatasetPackageVersionTemplate,
};