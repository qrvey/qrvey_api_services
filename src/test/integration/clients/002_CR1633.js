'use strict';

/* 
    covered cases
        - https://qrveydev.atlassian.net/browse/CR-1633
*/

const { expect } = require('chai');
const ApplicationService = require('../../../lib/applicationService');
const ComposerHelper = require('../../helpers/composerHelper');
const Server = require('../../../lib/contentDeployment/serverService')
const Package =  require('../../../lib/contentDeployment/packageService')
const PackageVersion =  require('../../../lib/contentDeployment/packageVersionService')
const Admin = require('../../../lib/adminCenter/authentication')
const chance = require('chance').Chance()
const {
    GENERAL_USER_ID,
    CONNECTIONS,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    GENERAL_COMPOSER_USER_EMAIL,
    REPEAT_SCENARIO

} = require('../../config/data');

describe('CR-1633', function() {
    this.timeout(1000 * 10 * 10 * 15);
    
    describe('the user creates an application and create a package on Content Deployment', ()=> {
        const apps = {
            data: {
                appId: null,
                name: 'Data application',
                connections: {
                    // Basic connections
                    mysql: {
                        name: 'Mysql connection',
                        connectionId: undefined,
                    },
    
                    // Advanced connections
                    advancedMysql: {
                        name: 'Advanced Mysql connection',
                        connectionId: undefined,
                    },
                },
                datasets: {
                    // Advanced datasets
                    advancedMysql: {
                        name: 'Advanced MYSQL datasource',
                        datasetId: null,
                        connections: {
                            mysql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mysql.connectorType,
                                database: CONNECTIONS.mysql.databases.general.name,
                                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
                            },
                        },
                        internalConnectionId: null,
                    }
                }
            }
        }
        
        let dataComposerItem = {}
        let token = ''
        let packageId = ''
        let PackageVersionTrackerId = ''
        it('It should create an application', async function () {
    
            const applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, { name: apps.data.name });
    
            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.name);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
    
            apps.data.appId = applicationItem.appid;
    
            return Promise.resolve();
        });
    
        it('It should create a composer helper instance', async function () {
            const item = new ComposerHelper(GENERAL_USER_ID);
            item.appId = apps.data.appId;
    
            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.eq(apps.data.appId);
            expect(item).to.has.property('connections').to.be.a('array').to.be.deep.eq([]);
            expect(item).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);
    
            dataComposerItem = item;
            return Promise.resolve();
        });
        
        it('It should create an advanced mysql connection', async function () {
            let connectionItem = await dataComposerItem.createMysqlConnection(apps.data.connections.advancedMysql.name, {advanced: true});
    
            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq('advanced');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.connections.advancedMysql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mysql.connectorType);
            expect(connectionItem.connectionConfig).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
    
            expect(dataComposerItem).to.be.a('object');
            expect(dataComposerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(dataComposerItem).to.has.property('appId').to.be.eq(apps.data.appId);
            expect(dataComposerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(1);
    
            apps.data.connections.advancedMysql.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });
        
        it('It should create a dataset from a MYSQL connection', async function () {
            const datasetItem = await dataComposerItem.createDatasetFromConnection(apps.data.datasets.advancedMysql.name, {
                name: apps.data.datasets.advancedMysql.name,
                connectionId: apps.data.connections.advancedMysql.connectionId,
                connectorType: CONNECTIONS.mysql.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            });
    
    
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.datasets.advancedMysql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('connectionId').to.be.a('string');
    
            apps.data.datasets.advancedMysql.datasetId = datasetItem.datasetId;
            apps.data.datasets.advancedMysql.connections.mysql.connectionId = apps.data.connections.advancedMysql.connectionId;
            apps.data.datasets.advancedMysql.internalConnectionId = datasetItem.connectionId;
    
            return Promise.resolve();
        });

        it('It should logged as Admin', async function () {
            const authentication = await Admin.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD)
            token = `jwt ${authentication.token}`;
            return Promise.resolve();
        });

        it('It should create a package and package version 100 times and verify that are created correctly', async () => {
            const repeat = REPEAT_SCENARIO
            const serverOrigin = Server.getServerName();
            const serverList = await Server.getServers({ authorization: token});
            const serverFound = serverList.find(server => server.name === serverOrigin);
            expect(serverFound).to.be.a('object');
            expect(serverFound).to.has.property('adminserverid').to.be.a('string');

            for(let i = 0; i < repeat; i++) {
                const packageDetail = {
                    "name": `Test Package CR-1633 ${chance.natural()}` , // Review package name
                    "description": "This is the package created by unit test repository",
                    "adminServerId": serverFound.adminserverid,
                    "userId": GENERAL_USER_ID,
                    "applicationId": apps.data.appId,
                    "email": GENERAL_COMPOSER_USER_EMAIL,
                    "appName": apps.data.name,
                    "nullkey": null // Review this property
                }

                const packageCreated = await Package.createPackage(packageDetail, {authorization: token})
                expect(packageCreated).to.be.a('object');
                expect(packageCreated).to.has.property('packageId').to.be.a('string');
                expect(packageCreated).to.has.property('status').to.be.a('string').to.be.eq('DRAFT');
                expect(packageCreated).to.has.property('appName').to.be.a('string').to.be.eq(apps.data.name);
                expect(packageCreated).to.has.property('userId').to.be.a('string').to.be.eq(GENERAL_USER_ID);
                packageId = packageCreated.packageId

                const versionDetail = {
                    name: 'v1'
                }; 

                const message = "When The version has been created, It will be to see  in the version list."

                const packageVersionCreated = await PackageVersion.createPackageVersion(packageId, versionDetail, {authorization: token})
                expect(packageVersionCreated).to.be.a('object');
                expect(packageVersionCreated).to.has.property('jobTrackerId').to.be.a('string');
                expect(packageVersionCreated).to.has.property('message').to.be.a('string');
                expect(packageVersionCreated).to.has.property('message').to.be.a('string').to.be.eq(message);
                PackageVersionTrackerId = packageVersionCreated.jobTrackerId

                const status = 'RUNNING'
                const trackerId = PackageVersionTrackerId
                const packageStatus = await PackageVersion.waitUntilPackageVersionIsCreated(status, trackerId, token)

                expect(packageStatus).to.be.a('object');
                expect(packageStatus).to.has.property('errorStack').to.be.a('string').to.be.eq('');
                expect(packageStatus).to.has.property('errorMessage').to.be.a('string').to.be.eq('');
                expect(packageStatus).to.has.property('jobTrackerId').to.be.a('string').to.be.eq(trackerId);
                expect(packageStatus).to.has.property('status').to.be.a('string').to.be.eq('COMPLETED');
            }

            return Promise.resolve();
        });

        it.skip('It should create a package', async function () {
            const serverOrigin = Server.getServerName();
            const serverList = await Server.getServers({ authorization: token});
            const serverFound = serverList.find(server => server.name === serverOrigin);
            expect(serverFound).to.be.a('object');
            expect(serverFound).to.has.property('adminserverid').to.be.a('string');
            
            const packageDetail = {
                "name": `Package CR-1633 ${chance.natural()}` , // Review package name
                "description": "This is the package created by unit test repository",
                "adminServerId": serverFound.adminserverid,
                "userId": GENERAL_USER_ID,
                "applicationId": apps.data.appId,
                "email": GENERAL_COMPOSER_USER_EMAIL,
                "appName": apps.data.name,
                "nullkey": null // Review this property
            }
            const packageCreated = await Package.createPackage(packageDetail, {authorization: token})
            expect(packageCreated).to.be.a('object');
            expect(packageCreated).to.has.property('packageId').to.be.a('string');
            expect(packageCreated).to.has.property('status').to.be.a('string').to.be.eq('DRAFT');
            expect(packageCreated).to.has.property('appName').to.be.a('string').to.be.eq(apps.data.name);
            expect(packageCreated).to.has.property('userId').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            packageId = packageCreated.packageId
            console.log(`package created: ${packageCreated}`)
            return Promise.resolve();
        });

        // FIXME: Add a new It: The package must not be created when its name is being used on another one.
        it.skip('It should create a package version', async () => {
            const versionDetail = {
                name: 'v1'
            }; 

            const message = "When The version has been created, It will be to see  in the version list."

            const packageVersionCreated = await PackageVersion.createPackageVersion(packageId, versionDetail, {authorization: token})
            expect(packageVersionCreated).to.be.a('object');
            expect(packageVersionCreated).to.has.property('jobTrackerId').to.be.a('string');
            expect(packageVersionCreated).to.has.property('message').to.be.a('string');
            expect(packageVersionCreated).to.has.property('message').to.be.a('string').to.be.eq(message);
            PackageVersionTrackerId = packageVersionCreated.jobTrackerId
            return Promise.resolve();
        });

        it.skip('It should wait until the package version is completed', async () => {
            const status = 'Progress'
            const trackerId = PackageVersionTrackerId
            const packageStatus = await PackageVersion.waitUntilPackageVersionIsCreated(status, trackerId, token)
            console.log(packageStatus)
            expect(packageStatus).to.be.a('object');
            expect(packageStatus).to.has.property('errorStack').to.be.a('string').to.be.eq('');
            expect(packageStatus).to.has.property('errorMessage').to.be.a('string').to.be.eq('');
            expect(packageStatus).to.has.property('jobTrackerId').to.be.a('string').to.be.eq(trackerId);
            expect(packageStatus).to.has.property('status').to.be.a('string').to.be.eq('COMPLETED');
            return Promise.resolve();
        });
    })
})