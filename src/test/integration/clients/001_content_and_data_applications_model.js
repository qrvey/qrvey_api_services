'use strict';

/* 
    covered cases
        - https://qrveydev.atlassian.net/browse/CR-1565 
*/

const { expect } = require('chai');

const ApplicationService = require('../../../lib/applicationService');
const ConnectionService = require('../../../lib/preData/connectionService');
const ComposerHelper = require('../../helpers/composerHelper');

const {
    GENERAL_USER_ID,
    CONNECTIONS,
} = require('../../config/data');

describe('Composer helper', function () {
    this.timeout(1000 * 6 * 10 * 15);

    describe('Connection cases', function () {
        let apps = {
            data: {
                appId: null,
                name: 'Data application',
                connections: {
                    // Basic connections
                    mysql: {
                        name: 'Mysql connection',
                        connectionId: undefined,
                    },

                    // Advanced connections
                    advancedMysql: {
                        name: 'Advanced Mysql connection',
                        connectionId: undefined,
                    },
                },
                datasets: {
                    // Basic datasets
                    mysql: {
                        name: 'MYSQL datasource',
                        datasetId: null,
                        connections: {
                            mysql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mysql.connectorType,
                                database: CONNECTIONS.mysql.databases.general.name,
                                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
                            },
                        },
                        internalConnectionId: null,
                    },

                    // Advanced datasets
                    advancedMysql: {
                        name: 'Advanced MYSQL datasource',
                        datasetId: null,
                        connections: {
                            mysql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mysql.connectorType,
                                database: CONNECTIONS.mysql.databases.general.name,
                                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
                            },
                        },
                        internalConnectionId: null,
                    },
                },
            },
            content: {
                appId: null,
                name: 'Content application',
                datasets: {
                    // Basic datasets
                    mysqlView: {
                        name: 'MYSQL datasource - View',
                        datasetId: null,
                        internalConnectionId: null,
                    },

                    // Advanced datasets
                    advancedMysqlView: {
                        name: 'Advanced MYSQL datasource  - View',
                        datasetId: null,
                        internalConnectionId: null,
                    },
                },
            },
        };
        /** @type {ComposerHelper} */
        let dataComposerItem; 
        let contentComposerItem;

        // Data application

        it('It should create an application', async function () {

            let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, { name: apps.data.name });

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.name);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            apps.data.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should create a composer helper instance', async function () {
            let item = new ComposerHelper(GENERAL_USER_ID);
            item.appId = apps.data.appId;

            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.eq(apps.data.appId);
            expect(item).to.has.property('connections').to.be.a('array').to.be.deep.eq([]);
            expect(item).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);

            dataComposerItem = item;
            return Promise.resolve();
        });

        it('It should create a mysql connection', async function () {
            let connectionItem = await dataComposerItem.createMysqlConnection(apps.data.connections.mysql.name);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.connections.mysql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mysql.connectorType);

            expect(dataComposerItem).to.be.a('object');
            expect(dataComposerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(dataComposerItem).to.has.property('appId').to.be.eq(apps.data.appId);
            expect(dataComposerItem).to.has.property('connections').to.be.a('array').to.be.deep.eq([{ connectionId: connectionItem.connectorid, name: apps.data.connections.mysql.name }]);
            expect(dataComposerItem).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);

            apps.data.connections.mysql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create an advanced mysql connection', async function () {
            let connectionItem = await dataComposerItem.createMysqlConnection(apps.data.connections.advancedMysql.name, {advanced: true});

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq('advanced');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.connections.advancedMysql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mysql.connectorType);
            expect(connectionItem.connectionConfig).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');

            expect(dataComposerItem).to.be.a('object');
            expect(dataComposerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(dataComposerItem).to.has.property('appId').to.be.eq(apps.data.appId);
            // expect(dataComposerItem).to.has.property('connections').to.be.a('array').to.be.deep.eq([{ connectionId: connectionItem.connectorid, name: apps.data.connections.mysql.name }]);
            // expect(dataComposerItem).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);
            expect(dataComposerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(2);

            apps.data.connections.advancedMysql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        // Datasets

        it('It should create a dataset from a MYSQL connection', async function () {
            let datasetItem = await dataComposerItem.createDatasetFromConnection(apps.data.datasets.mysql.name, {
                name: apps.data.datasets.mysql.name,
                connectionId: apps.data.connections.mysql.connectionId,
                connectorType: CONNECTIONS.mysql.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.datasets.mysql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('connectionId').to.be.a('string');

            apps.data.datasets.mysql.datasetId = datasetItem.datasetId;
            apps.data.datasets.mysql.connections.mysql.connectionId = apps.data.connections.mysql.connectionId;
            apps.data.datasets.mysql.internalConnectionId = datasetItem.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an advance MYSQL connection', async function () {
            let datasetItem = await dataComposerItem.createDatasetFromConnection(apps.data.datasets.advancedMysql.name, {
                name: apps.data.datasets.advancedMysql.name,
                connectionId: apps.data.connections.advancedMysql.connectionId,
                connectorType: CONNECTIONS.mysql.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.data.datasets.advancedMysql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('connectionId').to.be.a('string');

            apps.data.datasets.advancedMysql.datasetId = datasetItem.datasetId;
            apps.data.datasets.advancedMysql.connections.mysql.connectionId = apps.data.connections.mysql.connectionId;
            apps.data.datasets.advancedMysql.internalConnectionId = datasetItem.connectionId;

            return Promise.resolve();
        });

        // Content application

        it('It should create an application', async function () {

            let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, { name: apps.content.name });

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(apps.content.name);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            apps.content.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should create a composer helper instance', async function () {
            let item = new ComposerHelper(GENERAL_USER_ID);
            item.appId = apps.content.appId;

            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.eq(apps.content.appId);
            expect(item).to.has.property('connections').to.be.a('array').to.be.deep.eq([]);
            expect(item).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);

            contentComposerItem = item;
            return Promise.resolve();
        });

        it('It should not create a dataset view if parent dataset data is not being shared', async function () {
            let parentDatasetData = {
                appId: apps.data.appId,
                userId: GENERAL_USER_ID,
            };

            let datasetItem = await contentComposerItem.createDatasetView(apps.content.datasets.mysqlView.name, apps.data.datasets.mysql.internalConnectionId, parentDatasetData);
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(datasetItem).to.has.property('appId').to.be.eq(apps.content.appId);

            return Promise.resolve();
        });

        it('It should create a dataset view if parent dataset data is not being shared', async function () {
            let parentDatasetData = {
                appId: apps.data.appId,
                userId: GENERAL_USER_ID,
            };

            let datasetItem = await contentComposerItem.createDatasetView(apps.content.datasets.mysqlView.name, apps.data.datasets.mysql.internalConnectionId, parentDatasetData);
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(datasetItem).to.has.property('appId').to.be.eq(apps.content.appId);

            return Promise.resolve();
        });

        it('It should delete an application', async function () {

            let result = await contentComposerItem.deleteAll();

            expect(result).to.be.a('object');
            expect(result).to.has.property('message').to.be.a('string').to.be.eq(`The app ${apps.content.appId} was successfully deleted`);

            return Promise.resolve();
        });

        it('It should delete an application', async function () {

            let result = await dataComposerItem.deleteAll();

            expect(result).to.be.a('object');
            expect(result).to.has.property('message').to.be.a('string').to.be.eq(`The app ${apps.data.appId} was successfully deleted`);

            return Promise.resolve();
        });
    });
});
