/**
 *  New composer helper implementation
 */

const { expect } = require('chai');
const { GENERAL_USER_ID, GENERAL_APP_ID, VERSION} = require('../../config/data');
const ComposerHelper = require('../../helpers/composerHelper');

describe('Create application content and data application', function() {
    this.timeout(1000 * 6 * 10 * 15);
    describe('Create application', () => {
        it('It should create an application content and data', async () => {
            const composer1 = new ComposerHelper(GENERAL_USER_ID);
            const applications =  await composer1.createContentAndDataApplication();
            expect(applications.applicationData).to.be.a('object');
            expect(applications.applicationData).to.has.property('endUserLink').to.be.a('string');
            expect(applications.applicationData).to.has.property('appid').to.be.a('string');
            expect(applications.applicationData).to.has.property('name').to.be.a('string');
            expect(applications.applicationData).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
        
            expect(applications.applicationContent).to.be.a('object');
            expect(applications.applicationContent).to.has.property('endUserLink').to.be.a('string');
            expect(applications.applicationContent).to.has.property('appid').to.be.a('string');
            expect(applications.applicationContent).to.has.property('name').to.be.a('string');
            expect(applications.applicationContent).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
        });
    
        return Promise.resolve();
    });

    return Promise.resolve();
});