'use strict';
const chai = require('chai');
const { expect } = require('chai');

const {
    commonHelper: CommonHelper,
} = require('../../../lib/helpers');

const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const ApplicationService = require('../../../lib/applicationService');
const ConnectionService = require('../../../lib/preData/connectionService');
const DatasetService = require('../../../lib/preData/datasetService');
const DataRouterService = require('../../../lib/preData/dataRouterService');

const {
    GENERAL_USER_ID,
    CONNECTIONS,
} = require('../../config/data');

let applications = {
    app1: {
        name: 'My app',
        appId: undefined,
    },
};

let connections = {
    mysqlConnection: {
        name: 'My MYSQL connection',
        host: CONNECTIONS.mysql.host,
        password: CONNECTIONS.mysql.password,
        user: CONNECTIONS.mysql.user,
        connectorType: CONNECTIONS.mysql.connectorType,
        port: CONNECTIONS.mysql.port,
        connectionId: undefined,
    },
    mssqlConnection: {
        name: 'My MSSQL connection',
        host: CONNECTIONS.mssql.host,
        password: CONNECTIONS.mssql.password,
        user: CONNECTIONS.mssql.user,
        connectorType: CONNECTIONS.mssql.connectorType,
        port: CONNECTIONS.mssql.port,
        connectionId: undefined,
    },
    oracleConnection: {
        name: 'My Oracle connection',
        host: CONNECTIONS.oracle.host,
        password: CONNECTIONS.oracle.password,
        user: CONNECTIONS.oracle.user,
        serviceName: CONNECTIONS.oracle.serviceName,
        connectorType: CONNECTIONS.oracle.connectorType,
        port: CONNECTIONS.oracle.port,
        connectionId: undefined,
    },
    athenaConnection: {
        name: 'My Athena connection',
        configType: 'typical',
        accessKey: CONNECTIONS.athena.user,
        awsRegion: CONNECTIONS.athena.awsRegion,
        // catalogName: CONNECTIONS.athena.catalogName,
        connectorType: CONNECTIONS.athena.connectorType,
        hasHeader: true,

        secretKey: CONNECTIONS.athena.password,
        workGroup: CONNECTIONS.athena.workGroup,

        outputLocation: CONNECTIONS.athena.outputLocation,

        connectionId: undefined,

    }
};

describe('Dataset service - Mobile Insight', function () {
    this.timeout(1000 * 6 * 10 * 15);

    before(async function () {

        // Application
        let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);
        applications.app1.appId = applicationItem.appid;

        // Connections
        let mysqlConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection);
        connections.mysqlConnection.connectionId = mysqlConnectionItem.connectorid;

        let athenaConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection);
        connections.athenaConnection.connectionId = athenaConnectionItem.connectorid;

        let mssqlConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection);
        connections.mssqlConnection.connectionId = mssqlConnectionItem.connectorid;

        let oracleConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection);
        connections.oracleConnection.connectionId = oracleConnectionItem.connectorid;
    });

    // MYSQL connection

    describe('Dataset: Dataset using MYSQL connection', function () {
        let datasets = {
            mysql: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
                jobId: undefined,
                indexName: undefined,
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.mysql.name,
                connectionId: connections.mysqlConnection.connectionId,
                connectorType: connections.mysqlConnection.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body);

            expect(datasetItem).to.be.a('object');
            datasets.mysql.datasetId = datasetItem.datasetId;
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('jobId').to.be.a('string');
            expect(datasetItem).to.has.property('metadataId').to.be.a('string');

            datasets.mysql.jobId = datasetItem.jobId;
            datasets.mysql.indexName = datasetItem.metadataId;

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mysql.confirmedLoadedStatusTime);
            expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);

            return Promise.resolve();
        });

        it('It should not set a security column and readFromSourceFlag as false', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].readFromSource = false;
            datasetItem.columns[0].securityName ='id';

            let response = await expect(DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'})).to.be.rejectedWith();
            
            
            // expect(datasetItem).to.be.a('object');
            // expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            // expect(datasetItem).to.has.property('columns').to.be.a('array');
            // expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(false);
            // expect(datasetItem.columns[0]).to.has.property('securityName').to.be.a('string').to.be.eq('id');

            return Promise.resolve();
        });

        it('It should set the readFromSource flag as false if column is not a RLS column', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].readFromSource = false;
            datasetItem = await DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'});
            
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            expect(datasetItem).to.has.property('columns').to.be.a('array');
            expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(false);
            // expect(datasetItem.columns[0]).to.has.property('securityName').to.be.a('string').to.be.eq('id');

            return Promise.resolve();
        });

        it('It should not set a security column if the stored readFromSourceFlag is false', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].securityName ='id';

            let response = await expect(DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'})).to.be.rejectedWith();
            
            
            // expect(datasetItem).to.be.a('object');
            // expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            // expect(datasetItem).to.has.property('columns').to.be.a('array');
            // expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(false);
            // expect(datasetItem.columns[0]).to.has.property('securityName').to.be.a('string').to.be.eq('id');

            return Promise.resolve();
        });

        it('It should set the readFromSourceFlag as true', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].readFromSource = true;
            datasetItem = await DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'});
            
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            expect(datasetItem).to.has.property('columns').to.be.a('array');
            expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('It should set the security column if the stored column has the readFromSourceFlag as true', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].securityName ='id';
            datasetItem = await DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'});
            
            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            expect(datasetItem).to.has.property('columns').to.be.a('array');
            expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(true);
            expect(datasetItem.columns[0]).to.has.property('securityName').to.be.a('string').to.be.eq('id');

            return Promise.resolve();
        });

        it('It should not set a security column and readFromSourceFlag as false', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasetItem.columns[0].readFromSource = false;

            let response = await expect(DatasetService.updateDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, datasetItem, {domain: 'http://localhost:50050', apiKey: 'Qxcl7zvS483Ub6jVmrzPNgE8gIhHWHvbvdNOlNGm'})).to.be.rejectedWith();
            
            
            // expect(datasetItem).to.be.a('object');
            // expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);
            // expect(datasetItem).to.has.property('columns').to.be.a('array');
            // expect(datasetItem.columns[0]).to.has.property('readFromSource').to.be.a('boolean').to.be.eq(false);
            // expect(datasetItem.columns[0]).to.has.property('securityName').to.be.a('string').to.be.eq('id');

            return Promise.resolve();
        });

        it('It should apply dataset changes', async function () {
            let datasetItem = await DatasetService.applyDatasetChanges(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('metadataId').to.be.a('string').to.be.equal(datasets.mysql.indexName);

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('jobId').to.be.a('string');
            expect(datasetItem).to.has.property('metadataId').to.be.a('string');
            
            datasets.mysql.jobId = datasetItem.jobId;
            datasets.mysql.indexName = datasetItem.metadataId;

            return Promise.resolve();
        });

        it('Dataset status should be failed', async function () {

            let stopProcess = false, count = 0;

            while (stopProcess !== true) {

                let loadingStatusResponse = await DatasetService.getLoadingStatus(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

                let jobStatus = loadingStatusResponse?.JobStatistics?.statusJob?.status;
                let datasetStatus = loadingStatusResponse?.status;

                if (jobStatus === 'Complete') {

                    if (datasetStatus !== 'LOADING') {
                        stopProcess = true;
                    } else {            
                        if (count > 5) stopProcess = true;
                        count++;
                    }

                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mysql.confirmedLoadedStatusTime);
            expect(datasetItem).to.has.property('indexName').to.be.a('string').to.be.equal(datasets.mysql.indexName);
            expect(datasetItem).to.has.property('jobId').to.be.a('string').to.be.eq(datasets.mysql.jobId);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            return Promise.resolve();
        });
    });

    after(async function () {
        await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);
    });
});
