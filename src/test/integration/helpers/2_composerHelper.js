'use strict';
const { expect } = require('chai');

const ApplicationService = require('../../../lib/applicationService');
const ConnectionService = require('../../../lib/preData/connectionService');
const ComposerHelper = require('../../helpers/composerHelper');

const {
    GENERAL_USER_ID,
    CONNECTIONS,
} = require('../../config/data');

describe('Composer helper', function () {
    this.timeout(1000 * 6 * 10 * 15);

    describe('Connection cases', function () {
        let apps = {
            basic: {
                appId: null,
                name: 'Basic application',
                connections: {
                    mysql: {
                        name: 'Mysql connection',
                        connectionId: undefined,
                    },
                    athena: {
                        name: 'Athena connection',
                        connectionId: undefined,
                    },
                    oracle: {
                        name: 'Oracle connection',
                        connectionId: undefined,
                    },
                    mssql: {
                        name: 'MSSQL connection',
                        connectionId: undefined,
                    },

                    // Advanced
                    advancedMysql: {
                        name: 'Advanced Mysql connection',
                        connectionId: undefined,
                    },
                    advancedMssql: {
                        name: 'Advanced MSSQL connection',
                        connectionId: undefined,
                    },
                    advancedOracle: {
                        name: 'Advanced Oracle connection',
                        connectionId: undefined,
                    },
                },
                datasets: {
                    athena: {
                        name: 'Athena datasource',
                        datasetId: null,
                        connections: {
                            athena: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.athena.connectorType,
                                database: CONNECTIONS.athena.databases.general.name,
                                tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
                            },
                        },
                    },
                    mysql: {
                        name: 'MYSQL datasource',
                        datasetId: null,
                        connections: {
                            mysql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mysql.connectorType,
                                database: CONNECTIONS.mysql.databases.general.name,
                                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
                            },
                        },
                    },
                    oracle: {
                        name: 'Oracle datasource',
                        datasetId: null,
                        connections: {
                            oracle: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.oracle.connectorType,
                                database: CONNECTIONS.oracle.databases.general.name,
                                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
                            },
                        },
                    },
                    mssql: {
                        name: 'MSSQL datasource',
                        datasetId: null,
                        connections: {
                            mssql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mssql.connectorType,
                                database: CONNECTIONS.mssql.databases.general.name,
                                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
                            },
                        },
                    },

                    // Advanced
                    advancedMysql: {
                        name: 'Advanced MYSQL datasource',
                        datasetId: null,
                        connections: {
                            mysql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mysql.connectorType,
                                database: CONNECTIONS.mysql.databases.general.name,
                                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
                            },
                        },
                    },
                    advancedMssql: {
                        name: 'Advanced MSSQL datasource',
                        datasetId: null,
                        connections: {
                            mssql: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.mssql.connectorType,
                                database: CONNECTIONS.mssql.databases.general.name,
                                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
                            },
                        },
                    },
                    advancedOracle: {
                        name: 'Advanced Oracle datasource',
                        datasetId: null,
                        connections: {
                            oracle: {
                                connectionId: undefined,
                                connectorType: CONNECTIONS.oracle.connectorType,
                                database: CONNECTIONS.oracle.databases.general.name,
                                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
                            },
                        },
                    },
                },
            },
        };
        /** @type {ComposerHelper} */
        let composerItem;

        it('It should create an application', async function () {

            let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, { name: apps.basic.name });

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.name);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            apps.basic.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should create a composer helper instance', async function () {
            let item = new ComposerHelper(GENERAL_USER_ID);
            item.appId = apps.basic.appId;

            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(item).to.has.property('connections').to.be.a('array').to.be.deep.eq([]);
            expect(item).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);

            composerItem = item;
            return Promise.resolve();
        });

        it('It should create a mysql connection', async function () {
            let connectionItem = await composerItem.createMysqlConnection(apps.basic.connections.mysql.name);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.mysql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mysql.connectorType);

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.be.deep.eq([{ connectionId: connectionItem.connectorid, name: apps.basic.connections.mysql.name }]);
            expect(composerItem).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);

            apps.basic.connections.mysql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create an advanced mysql connection', async function () {
            let connectionItem = await composerItem.createMysqlConnection(apps.basic.connections.advancedMysql.name, {advanced: true});

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq('advanced');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.advancedMysql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mysql.connectorType);
            expect(connectionItem.connectionConfig).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            // expect(composerItem).to.has.property('connections').to.be.a('array').to.be.deep.eq([{ connectionId: connectionItem.connectorid, name: apps.basic.connections.mysql.name }]);
            // expect(composerItem).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(2);

            apps.basic.connections.advancedMysql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create a mssql connection', async function () {
            let connectionItem = await composerItem.createMssqlConnection(apps.basic.connections.mssql.name);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.mssql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mssql.connectorType);

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(3);
            apps.basic.connections.mssql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create an advanced mssql connection', async function () {
            let connectionItem = await composerItem.createMssqlConnection(apps.basic.connections.advancedMssql.name, {advanced: true});

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq('advanced');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.advancedMssql.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.mssql.connectorType);

            expect(connectionItem.connectionConfig).to.has.property('server').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(4);

            apps.basic.connections.advancedMssql.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create a oracle connection', async function () {
            let connectionItem = await composerItem.createOracleConnection(apps.basic.connections.oracle.name);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.oracle.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.oracle.connectorType);
            expect(connectionItem).to.has.property('serviceName').to.be.a('string').to.be.eq(CONNECTIONS.oracle.serviceName);

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(5);

            apps.basic.connections.oracle.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create an advanced oracle connection', async function () {
            let connectionItem = await composerItem.createOracleConnection(apps.basic.connections.advancedOracle.name, {advanced: true});

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.advancedOracle.name);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.oracle.connectorType);

            expect(connectionItem.connectionConfig).to.has.property('connectString').to.be.a('string').to.be.eq(`${CONNECTIONS.oracle.host}:${CONNECTIONS.oracle.port}/${CONNECTIONS.oracle.serviceName}`);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(6);

            apps.basic.connections.advancedOracle.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        it('It should create an Athena connection', async function () {
            let connectionItem = await composerItem.createAthenaConnection(apps.basic.connections.athena.name);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.connections.athena.name);

            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(apps.basic.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(CONNECTIONS.athena.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);

            expect(connectionItem).to.not.has.property('secretKey');

            expect(composerItem).to.be.a('object');
            expect(composerItem).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(composerItem).to.has.property('appId').to.be.eq(apps.basic.appId);
            expect(composerItem).to.has.property('connections').to.be.a('array').to.has.lengthOf(7);

            apps.basic.connections.athena.connectionId = connectionItem.connectorid;

            return Promise.resolve();
        });

        // Datasets

        it('It should create a dataset from a MYSQL connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.mysql.name, {
                name: apps.basic.datasets.mysql.name,
                connectionId: apps.basic.connections.mysql.connectionId,
                connectorType: CONNECTIONS.mysql.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.mysql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.mysql.datasetId = datasetItem.datasetId;
            apps.basic.datasets.mysql.connections.mysql.connectionId = apps.basic.connections.mysql.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an advance MYSQL connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.advancedMysql.name, {
                name: apps.basic.datasets.advancedMysql.name,
                connectionId: apps.basic.connections.advancedMysql.connectionId,
                connectorType: CONNECTIONS.mysql.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.advancedMysql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.advancedMysql.datasetId = datasetItem.datasetId;
            apps.basic.datasets.advancedMysql.connections.mysql.connectionId = apps.basic.connections.mysql.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from a MSSQL connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.mssql.name, {
                name: apps.basic.datasets.mssql.name,
                connectionId: apps.basic.connections.mssql.connectionId,
                connectorType: CONNECTIONS.mssql.connectorType,
                database: CONNECTIONS.mssql.databases.general.name,
                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.mssql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.mssql.datasetId = datasetItem.datasetId;
            apps.basic.datasets.mssql.connections.mssql.connectionId = apps.basic.connections.mssql.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an advance MSSQL connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.advancedMssql.name, {
                name: apps.basic.datasets.advancedMssql.name,
                connectionId: apps.basic.connections.advancedMssql.connectionId,
                connectorType: CONNECTIONS.mssql.connectorType,
                database: CONNECTIONS.mssql.databases.general.name,
                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.advancedMssql.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.advancedMssql.datasetId = datasetItem.datasetId;
            apps.basic.datasets.advancedMssql.connections.mssql.connectionId = apps.basic.connections.advancedMssql.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an Oracle connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.oracle.name, {
                name: apps.basic.datasets.oracle.name,
                connectionId: apps.basic.connections.oracle.connectionId,
                connectorType: CONNECTIONS.oracle.connectorType,
                database: CONNECTIONS.oracle.databases.general.name,
                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.oracle.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.oracle.datasetId = datasetItem.datasetId;
            apps.basic.datasets.oracle.connections.oracle.connectionId = apps.basic.connections.oracle.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an advanced Oracle connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.advancedOracle.name, {
                name: apps.basic.datasets.advancedOracle.name,
                connectionId: apps.basic.connections.advancedOracle.connectionId,
                connectorType: CONNECTIONS.oracle.connectorType,
                database: CONNECTIONS.oracle.databases.general.name,
                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.advancedOracle.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.advancedOracle.datasetId = datasetItem.datasetId;
            apps.basic.datasets.advancedOracle.connections.oracle.connectionId = apps.basic.connections.advancedOracle.connectionId;

            return Promise.resolve();
        });

        it('It should create a dataset from an Athena connection', async function () {
            let datasetItem = await composerItem.createDatasetFromConnection(apps.basic.datasets.athena.name, {
                name: apps.basic.datasets.athena.name,
                connectionId: apps.basic.connections.athena.connectionId,
                connectorType: CONNECTIONS.athena.connectorType,
                database: CONNECTIONS.athena.databases.general.name,
                tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
                catalogName: CONNECTIONS.athena.databases.general.catalogName,
            }, {
                load: true, wait: true
            }, {
                invoke: true
            });


            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(apps.basic.datasets.athena.name);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');

            apps.basic.datasets.athena.datasetId = datasetItem.datasetId;
            apps.basic.datasets.athena.connections.athena.connectionId = apps.basic.connections.athena.connectionId;

            return Promise.resolve();
        });

        it('It should delete an application', async function () {

            let result = await composerItem.deleteAll();

            expect(result).to.be.a('object');
            expect(result).to.has.property('message').to.be.a('string').to.be.eq(`The app ${apps.basic.appId} was successfully deleted`);

            return Promise.resolve();
        });
    });

    describe('Basic application', function () {
        let apps = {
            basic: {
                appId: null,
                name: 'Basic application',
            }
        };

        /** @type {ComposerHelper} */
        let composerItem;

        it('It should throw an error if missing user id', async function () {
            expect(() => new ComposerHelper()).to.throw('Missing user id');
            return Promise.resolve();
        });

        it('It should throw an error if missing user id', async function () {
            expect(() => new ComposerHelper(null)).to.throw('Missing user id');
            return Promise.resolve();
        });

        it('It should create a composer helper instance', async function () {
            let item = new ComposerHelper(GENERAL_USER_ID);
            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.eq(null);
            expect(item).to.has.property('connections').to.be.a('array').to.be.deep.eq([]);
            expect(item).to.has.property('datasets').to.be.a('array').to.be.deep.eq([]);
            return Promise.resolve();
        });

        it('It should create an application', async function () {
            let item = await ComposerHelper.createBasicApplication(GENERAL_USER_ID, {
                name: apps.basic.name,
            });

            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.a('string');
            expect(item).to.has.property('connections').to.be.a('array').to.have.lengthOf(4);
            expect(item).to.has.property('datasets').to.be.a('array').to.have.lengthOf(5);

            apps.basic.appId = item.appId;
            composerItem = item;

            return Promise.resolve();
        });

        it('testing connections', async function () {

            for (let connectionItem of composerItem.connections) {
                let connectionDetails = await ConnectionService.getConnection(GENERAL_USER_ID, apps.basic.appId, connectionItem.connectionId);
                let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, apps.basic.appId, connectionItem.connectionId, { connectionData: { ...connectionDetails } });

                expect(result).to.be.a('object');

                expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
                // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection test was successful.');
            }

            return Promise.resolve();
        });

        it('It should delete an application', async function () {
            let result = await composerItem.deleteAll();

            expect(result).to.be.a('object');
            expect(result).to.has.property('message').to.be.a('string').to.be.eq(`The app ${apps.basic.appId} was successfully deleted`);

            return Promise.resolve();
        });
    });

    describe('Complete application', function () {
        let apps = {
            full: {
                appId: null,
                name: 'Complete application',
            }
        };

        /** @type {ComposerHelper} */
        let composerItem;

        it('It should create an application', async function () {
            let item = await ComposerHelper.createFullApplication(GENERAL_USER_ID, {
                name: apps.full.name,
            });

            expect(item).to.be.a('object');
            expect(item).to.has.property('userId').to.be.eq(GENERAL_USER_ID);
            expect(item).to.has.property('appId').to.be.a('string');
            expect(item).to.has.property('connections').to.be.a('array').to.have.lengthOf(7);
            expect(item).to.has.property('datasets').to.be.a('array').to.have.lengthOf(7);

            apps.full.appId = item.appId;
            composerItem = item;

            return Promise.resolve();
        });

        it('testing connections', async function () {

            for (let connectionItem of composerItem.connections) {
                let connectionDetails = await ConnectionService.getConnection(GENERAL_USER_ID, apps.full.appId, connectionItem.connectionId);
                let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, apps.full.appId, connectionItem.connectionId, { connectionData: { ...connectionDetails } });

                expect(result).to.be.a('object');

                expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
                // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection test was successful.');
            }

            return Promise.resolve();
        });

        it('It should delete an application', async function () {
            let result = await composerItem.deleteAll();

            expect(result).to.be.a('object');
            expect(result).to.has.property('message').to.be.a('string').to.be.eq(`The app ${apps.full.appId} was successfully deleted`);

            return Promise.resolve();
        });
    });
});
