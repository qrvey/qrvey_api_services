'use strict';
const { expect } = require('chai');

const DefinitionHelper = require('../../helpers/definitionHelper');

const { getBasicPackageVersionTemplate, getInvalidPackageVersionTemplate } = require('../../templates/packageVersion');

describe('Definition helper', function () {
    describe('mapAssetsByPackageVersionItem: Definition helper: Definition assets mapper', function () {
        it('It should throw an error if packageVersionItem is missing', async function () {
            expect(() => DefinitionHelper.mapAssetsByPackageVersionItem()).to.throw('packageVersionItem is required');
        });

        it('mapAssetsByPackageVersionItem: It should throw an error if contentPackageVersion is missing', async function () {

            let invalidPackageVersionItem = getInvalidPackageVersionTemplate();
            expect(() => DefinitionHelper.mapAssetsByPackageVersionItem(invalidPackageVersionItem)).to.throw('packageVersionItem.contentPackageVersion is required');
        });

        it('mapConnections: It should throw an error if connectionList is missing', async function () {
            expect(() => DefinitionHelper.mapConnections()).to.throw('connectionList is required');
        });

        it('mapConnections: It should throw an error if contentPackageVersion is missing', async function () {
            expect(() => DefinitionHelper.mapConnections({connectionId: 'a'})).to.throw('Invalid connection list');
            expect(() => DefinitionHelper.mapConnections('Hi')).to.throw('Invalid connection list');
            expect(() => DefinitionHelper.mapConnections(1234)).to.throw('Invalid connection list');
        });
        
        it('mapConnections: It should throw an error if connectionList is missing', async function () {
            let basicPackageVersionTemplate = getBasicPackageVersionTemplate();
            let connections = DefinitionHelper.mapConnections(basicPackageVersionTemplate.contentPackageVersion.connections);

            expect(connections).to.be.a('array').to.have.lengthOf(4);

            for (let connectionItem of connections) {
                expect(connectionItem).to.be.a('object');
                expect(connectionItem).to.have.property('_type').to.be.a('string').to.be.eq('connection');
                expect(connectionItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(basicPackageVersionTemplate.contentPackageVersion.connections.map(item => item.connectionId));
                expect(connectionItem).to.have.property('connectionClassType').to.be.a('string').to.be.eq('database');
                expect(connectionItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(connectionItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
            }
            
        });

        it('mapDatasets: It should throw an error if datasetList is missing', async function () {
            expect(() => DefinitionHelper.mapDatasets()).to.throw('datasetList is required');
        });

        it('mapDatasets: It should throw an error if contentPackageVersion is missing', async function () {
            expect(() => DefinitionHelper.mapDatasets({datasetId: 'a'})).to.throw('Invalid dataset list');
            expect(() => DefinitionHelper.mapDatasets('Hi')).to.throw('Invalid dataset list');
            expect(() => DefinitionHelper.mapDatasets(1234)).to.throw('Invalid dataset list');
        });
        
        it('mapDatasets: It should throw an error if datasetList is missing', async function () {
            let basicPackageVersionTemplate = getBasicPackageVersionTemplate();
            let datasets = DefinitionHelper.mapDatasets(basicPackageVersionTemplate.contentPackageVersion.datasets);

            expect(datasets).to.be.a('array').to.have.lengthOf(4);

            for (let datasetItem of datasets) {
                expect(datasetItem).to.be.a('object');
                expect(datasetItem).to.have.property('_type').to.be.a('string').to.be.eq('dataset');
                expect(datasetItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(basicPackageVersionTemplate.contentPackageVersion.datasets.map(item => item.datasetId));
                expect(datasetItem).to.have.property('datasetType').to.be.a('string').to.be.eq('DATASET');
                expect(datasetItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(datasetItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
                expect(datasetItem).to.have.property('loadData').to.be.a('boolean').to.be.eq(false);
                
                expect(datasetItem).to.have.property('charts').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('metrics').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('formulas').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('buckets').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('transformations').to.be.a('array').to.has.lengthOf(0);
            }
            
        });

        it('mapAssetsByPackageVersionItem: It should map the package assets', async function () {
            let basicPackageVersionTemplate = getBasicPackageVersionTemplate();
            let contentPackageVersionSelected = DefinitionHelper.mapAssetsByPackageVersionItem(basicPackageVersionTemplate);

            expect(contentPackageVersionSelected).to.be.a('object');
            expect(contentPackageVersionSelected).to.be.have.property('connections').to.be.a('array').to.have.lengthOf(4);

            for (let connectionItem of contentPackageVersionSelected.connections) {
                expect(connectionItem).to.be.a('object');
                expect(connectionItem).to.have.property('_type').to.be.a('string').to.be.eq('connection');
                expect(connectionItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(basicPackageVersionTemplate.contentPackageVersion.connections.map(item => item.connectionId));
                expect(connectionItem).to.have.property('connectionClassType').to.be.a('string').to.be.eq('database');
                expect(connectionItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(connectionItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
            }

            for (let datasetItem of contentPackageVersionSelected.datasets) {
                expect(datasetItem).to.be.a('object');
                expect(datasetItem).to.have.property('_type').to.be.a('string').to.be.eq('dataset');
                expect(datasetItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(basicPackageVersionTemplate.contentPackageVersion.datasets.map(item => item.datasetId));
                expect(datasetItem).to.have.property('datasetType').to.be.a('string').to.be.eq('DATASET');
                expect(datasetItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(datasetItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
                expect(datasetItem).to.have.property('loadData').to.be.a('boolean').to.be.eq(false);
                
                expect(datasetItem).to.have.property('charts').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('metrics').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('formulas').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('buckets').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('transformations').to.be.a('array').to.has.lengthOf(0);
            }
            
        });
    });
});