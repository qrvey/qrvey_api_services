'use strict';
const { expect } = require('chai');
const { DateTime } = require('luxon');

const AdminHelper = require('../../helpers/adminHelper');

const {
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    SERVERS,
    GENERAL_USER_ID,
} = require('../../config/data');

const {
    authentication: Authentication,
} = require('../../../lib/adminCenter');

let token = undefined; // Authorization token

describe('Admin helper', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    before(async function () {
        let authenticationResponse = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);
        token = `jwt ${authenticationResponse.token}`;
    });

    describe('Server cases', function () {

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
            serverId: undefined,
        };

        it('It should create or find an existing server', async function () {

            let adminItem = await AdminHelper.findOrCreateServer(serverDetails, {
                authorization: token,
            });

            expect(adminItem).to.be.a('object');
            expect(adminItem).to.has.property('serverId').to.be.a('string');
            serverDetails.serverId = adminItem.serverId;

            return Promise.resolve();
        });
    });

    describe('Package cases', function () {

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
            serverId: undefined,
        };

        /** @type {AdminHelper} */
        let adminItem;

        it('It should create a package using the basic application helper', async function () {

            adminItem = await AdminHelper.createBasicPackage(serverDetails,
                {
                    appName: 'CD2.0 basic app',
                    packageName: `PK - CD2.0 basic app ${now.toMillis()}`,
                    userId: GENERAL_USER_ID,
                },
                {
                    authorization: token,
                });

            expect(adminItem).to.be.a('object');
            expect(adminItem).to.has.property('serverId').to.be.a('string');
            expect(adminItem).to.has.property('packageVersionId').to.be.a('string');
            expect(adminItem).to.has.property('packageId').to.be.a('string');

            serverDetails.serverId = adminItem.serverId;

            return Promise.resolve();
        });

        it('It should delete a package using the basic application helper', async function () {

            await adminItem.deleteAll({ authorization: token });

            return Promise.resolve();
        });
    });

    describe('Basic deployment', function () {

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
            serverId: undefined,
        };


        /** @type {AdminHelper} */
        let adminItem;

        it('It should create a definition using the basic application helper', async function () {

            adminItem = await AdminHelper.createBasicDefinition(serverDetails,
                {
                    name: `CD2.0 basic app ${now.toMillis()}`,
                    description: 'CD2.0 basic app',
                    userId: GENERAL_USER_ID,
                },
                {
                    authorization: token,
                });

            expect(adminItem).to.be.a('object');
            expect(adminItem).to.has.property('serverId').to.be.a('string');
            expect(adminItem).to.has.property('packageVersionId').to.be.a('string');
            expect(adminItem).to.has.property('packageId').to.be.a('string');
            expect(adminItem).to.has.property('definitionId').to.be.a('string');

            serverDetails.serverId = adminItem.serverId;

            return Promise.resolve();
        });

        it('It should delete all', async function () {

            await adminItem.deleteAll({ authorization: token });

            return Promise.resolve();
        });
    });

    describe('Full deployment', function () {

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
            serverId: undefined,
        };


        /** @type {AdminHelper} */
        let adminItem;

        it('It should create a definition using the basic application helper', async function () {

            adminItem = await AdminHelper.createFullDefinition(serverDetails,
                {
                    name: `CD2.0 basic app ${now.toMillis()}`,
                    description: 'CD2.0 basic app',
                    userId: GENERAL_USER_ID,
                },
                {
                    authorization: token,
                });

            expect(adminItem).to.be.a('object');
            expect(adminItem).to.has.property('serverId').to.be.a('string');
            expect(adminItem).to.has.property('packageVersionId').to.be.a('string');
            expect(adminItem).to.has.property('packageId').to.be.a('string');
            expect(adminItem).to.has.property('definitionId').to.be.a('string');

            serverDetails.serverId = adminItem.serverId;

            return Promise.resolve();
        });

        it('It should delete all', async function () {

            await adminItem.deleteAll({ authorization: token });

            return Promise.resolve();
        });
    });
});