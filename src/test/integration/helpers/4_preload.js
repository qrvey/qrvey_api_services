'use strict';
const { expect } = require('chai');
const preloadData = require('./../../helpers/preloadHelper')
const data = require('./../../config/preload').COMPOSER
describe('Preload setter and getters test', function() {
    describe('LC connections test', function () {
        it('It should validate SNOWFLAKE, REDSHIFT and PostgreSQL Object connection', async function () {
           const test = new preloadData(data)
           const myArray = []
           myArray.push({
                connectionId: 'w1wdee3re',
                name: 'SNOWFLAKE'
           })

           myArray.push({
                connectionId: 'w1wdee3re',
                name: 'REDSHIFT'
           })

           myArray.push({
                connectionId: 'w1wdee3re',
                name: 'PostgreSQL'
           })
           const result = test.validateLCConnections(myArray)
           expect(result).to.be.ok
           return Promise.resolve();
        }); 

        it('It should validate SNOWFLAKE, REDSHIFT and PostgreSQL Object dataset', async function () {
            const test = new preloadData(data)
            const myArray = []
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Snowflake'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Redshift'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Postgresql'
            })
            const result = test.validateLCDatasets(myArray)
            expect(result).to.be.ok
            return Promise.resolve();
         });


         it('It should set SNOWFLAKE, REDSHIFT and PostgreSQL Object connection', async function () {
            const test = new preloadData(data)
            const myArray = []
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Snowflake'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Redshift'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Postgresql'
            })
            test.setConnections(myArray)
            const result = test.getConnections()
            expect(result).to.have.lengthOf(3)
            return Promise.resolve();
         });

         it('It should set SNOWFLAKE, REDSHIFT and PostgreSQL Object dataset', async function () {
            const test = new preloadData(data)
            const myArray = []
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Snowflake'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Redshift'
            })
 
            myArray.push({
                datasetId: 'w1wdee3re',
                name: 'Postgresql'
            })
            test.setDatasets(myArray)
            const result = test.getDatasets()
            expect(result).to.have.lengthOf(3)
            return Promise.resolve();
         });

    })
    return Promise.resolve();
})