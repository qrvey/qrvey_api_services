'use strict';
const { expect } = require('chai');

const {
    commonHelper: CommonHelper,
} = require('../../../lib/helpers');

const UserService = require('../../../lib/userService');
const {
    GENERAL_COMPOSER_USER_PASSWORD,
    GENERAL_COMPOSER_USER_EMAIL,
    GENERAL_USER_ID,
} = require('../../config/data');

describe('Qrvey authentication', function () {
    this.timeout(1000 * 6 * 10 * 15);
    describe('Basic authentication', function () {

        it('It should login an user using email and password', async function () {
            let userItem = await UserService.userLogin({
                email: GENERAL_COMPOSER_USER_EMAIL,
                password: GENERAL_COMPOSER_USER_PASSWORD,
            });

            expect(userItem).to.be.a('object');
            expect(userItem).to.has.property('message').be.a('string').to.be.eq('OK');
            expect(userItem).to.has.property('status').be.a('number').to.be.eq(200);
            expect(userItem).to.has.property('userid').be.a('string').to.be.eq(GENERAL_USER_ID);

            return Promise.resolve();
        });

    });

    describe('Backend 2 Backend authentication', function () {

        it('Generate login token', async function () {
            let data = await UserService.generateLoginToken({
                userid: GENERAL_USER_ID,
                expiresIn: '5s',
            });

            expect(data).to.be.a('object');
            expect(data).to.has.property('token').be.a('string');

            return Promise.resolve();
        });

        it('Qrvey authentication without user permissions', async function () {

            let { token } = await UserService.generateLoginToken({
                userid: GENERAL_USER_ID,
                expiresIn: '5s',
            });

            let params = {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            };

            let data = await UserService.apiLogin({}, params);

            expect(data).to.be.a('object');
            expect(data).to.has.property('status').be.a('number').to.be.eq(200);
            expect(data).to.has.property('message').be.a('string').to.be.eq('OK');
            // expect(data).to.has.property('token').be.a('string');

            expect(data).to.has.property('user').be.a('object');
            expect(data.user).to.has.property('userid').be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(data.user).to.has.property('email').be.a('string').to.be.eq(GENERAL_COMPOSER_USER_EMAIL);


            return Promise.resolve();
        });

        it('Should throw error if tokens expires', async function () {
            let { token } = await UserService.generateLoginToken({
                userid: GENERAL_USER_ID,
                expiresIn: '1s',
            });

            let params = {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            };

            await CommonHelper.sleep(2000);

            let hasFailed = false;
            try {
                await UserService.apiLogin({}, params);
            } catch (error) {
                hasFailed = true;
                expect(error).to.be.a('Object');
                expect(error).to.has.property('status').be.a('string').to.be.eq('error');
                expect(error).to.has.property('errors').be.a('array').to.has.lengthOf(1);
                expect(error.errors[0]).to.has.property('message').be.a('string').to.be.eq('"exp" claim timestamp check failed');
                expect(error.errors[0]).to.has.property('code').be.a('string').to.be.eq('ERR_JWT_EXPIRED');
            }

            if (hasFailed === false) {
                return Promise.reject('Expected to fail');
            }

            return Promise.resolve();
        });

    });
});
