'use strict';
const { expect } = require('chai');
const axios = require('axios');

const ApplicationService = require('../../../../lib/applicationService');
const {
    webFormsService: WebFormsService,
    webFormLookupService: WebFormLookupService,
} = require('../../../../lib/preData');

const {
    workflowService: WorkflowService, workflowService,
} = require('../../../../lib/postData');

const {
    GENERAL_USER_ID,
    WRONG_APP_ID,
    WRONG_USER_ID,
} = require('../../../config/data');

describe('Web form', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let applications = {
        app1: {
            name: 'My app',
            appId: undefined,
        },
    };

    before(async function () {
        let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);
        applications.app1.appId = applicationItem.appid;
    });

    describe('Client I: Quiz new data trigger', function () {
        let qrveys = {
            quiz: {
                name: 'My quiz',
                description: 'Quiz description',
                qrveyId: null,
                questions: {
                    question1: {
                        answers: [
                            {
                                answer: 'OPTION 1',
                                answerid: 'OPTION_1'
                            },
                            {
                                answer: 'OPTION 2',
                                answerid: 'OPTION_2'
                            }
                        ],
                        text: 'Question 1',
                        type: 'YES_NO',
                        question: 'optional',
                        right_answer: 'OPTION 1',
                        incomplete: false,
                        visibility: 'visible',
                        score: 1,
                        questionIndex: 1
                    }
                },
                lookup: null,
            }
        };

        let workflow = {
            name: 'My workflow: Client I',
            description: 'My workflow description',
            workflowId: null,
        };

        it('It should create a quiz', async function () {
            let qrveyItem = await WebFormsService.createWebForm(GENERAL_USER_ID, applications.app1.appId, {
                name: qrveys.quiz.name,
                description: qrveys.quiz.description,
                appType: 'QUIZ',
            });

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string');
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');
            qrveys.quiz.qrveyId = qrveyItem.qrveyid;
        });

        it('It should get an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');
        });

        it('It should create a workflow', async function () {
            let workflowItem = await WorkflowService.createWorkflow(GENERAL_USER_ID, applications.app1.appId, {
                name: workflow.name,
                description: workflow.description,
            });

            expect(workflowItem).to.be.a('object');
            expect(workflowItem).to.has.property('workflowid').to.be.a('string');
            workflow.workflowId = workflowItem.workflowid;
        });

        it('It should update an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            qrveyItem.questions = {
                data: [
                    {...qrveys.quiz.questions.question1}
                ]
            };

            qrveyItem = await WebFormsService.updateWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId, {...qrveyItem});

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');

            expect(qrveyItem).to.has.property('questions').to.be.a('object').to.has.property('data').to.be.a('array').to.has.lengthOf(1);
        });

        it('It should update an existing worfklow by workflowId', async function () {
            let workflowItem = await WorkflowService.getWorkflow(GENERAL_USER_ID, applications.app1.appId, workflow.workflowId);

            expect(workflowItem).to.be.a('object');
            expect(workflowItem).to.has.property('workflowid').to.be.a('string').to.be.eq(workflow.workflowId);

            workflowItem.trigger = {
                type: 'NEW_DATA_TRIGGER',
                group: 'data_trigger',
                children: [
                    {
                        type: 'SEND_EMAIL_ACTION',
                        group: 'action',
                        children: [],
                        dropables: [
                            'condition'
                        ],
                        id: 'VL2QJ6HX',
                        data: {
                            recipients: [
                                'alberto.valle@qrvey.com'
                            ],
                            subject: 'Workflow: API services',
                            html: '<p>Workflow: API services</p>',
                            attachment: {
                                reports: [],
                                pages: [],
                                charts: []
                            },
                            includeSigned: true
                        }
                    }
                ],
                dropables: [
                    'action',
                    'condition',
                    'search',
                    'action_condition'
                ],
                id: 'U8AP3UT2',
                source: {
                    id: qrveys.quiz.qrveyId,
                    type: 'QUIZ'
                }
            };

            workflowItem = await workflowService.updateWorkflow(GENERAL_USER_ID, applications.app1.appId, workflow.workflowId, {...workflowItem});

            expect(workflowItem).to.be.a('object');
            expect(workflowItem).to.has.property('workflowid').to.be.a('string').to.be.eq(workflow.workflowId);
        });

        it('It should activate a quiz', async function () {
            let qrveyItem = await WebFormsService.activateWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string');
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('RUNNING');
        });

        it('It should activate a workflow', async function () {
            let workflowItem = await WorkflowService.activateWorkflow(GENERAL_USER_ID, applications.app1.appId, workflow.workflowId);
            expect(workflowItem).to.be.a('object');
            expect(workflowItem).to.has.property('workflowid').to.be.a('string').to.be.eq(workflow.workflowId);
            expect(workflowItem).to.has.property('status').to.be.eq('RUNNING');
        });

        it('It should get an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('RUNNING');
            expect(qrveyItem.workflowTriggers).to.has.property('newDataTrigger').to.be.a('array').to.has.lengthOf(1);
            expect(qrveyItem.workflowTriggers.newDataTrigger[0]).to.be.a('string').to.be.eq(workflow.workflowId);
        });

        it('It should get uniq url', async function () {
            let body = {
                recipients: ['alberto.valle@qrvey.com'],
            };
            let response = await WebFormLookupService.getTakerUrlByRecipient(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId, body);
            // console.log('LOOKUP: ', qrveyItem);
            expect(response[0]).to.be.a('object');
            expect(response[0]).to.has.property('url').to.be.a('string');
            qrveys.quiz.lookup = response[0].lookupid;
        });

        it('It should get an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('RUNNING');
            expect(qrveyItem.workflowTriggers).to.has.property('newDataTrigger').to.be.a('array').to.has.lengthOf(1);
            expect(qrveyItem.workflowTriggers.newDataTrigger[0]).to.be.a('string').to.be.eq(workflow.workflowId);
        });

        // it('Open web form taker URL', async function () {
        //     let response = await axios.post(`https://ltsstaging.qrveyapp.com/pubapi/v4/user/${GENERAL_USER_ID}/app/${applications.app1.appId}/qrvey/${qrveys.quiz.qrveyId}/analytiq/answers/results`, {
        //         "filters": [
        //             {
        //                 "questionid": "EMAIL",
        //                 "values": [
        //                     "alberto.valle@qrvey.com"
        //                 ]
        //             }
        //         ]
        //     });

        //     expect(response).to.be.a('object');
        // });

        it('It should get an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('RUNNING');
            expect(qrveyItem.workflowTriggers).to.has.property('newDataTrigger').to.be.a('array').to.has.lengthOf(1);
            expect(qrveyItem.workflowTriggers.newDataTrigger[0]).to.be.a('string').to.be.eq(workflow.workflowId);
        });

        it('It should delete a workflow', async function () {
            let response = await WorkflowService.deleteWorkflow(GENERAL_USER_ID, applications.app1.appId, workflow.workflowId);
            expect(response).to.be.a('object');
        });

        it('It should delete a quiz', async function () {
            let response = await WebFormsService.deleteWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            expect(response).to.be.a('object');
            expect(response.qrvey).to.be.a('object');
            expect(response.qrvey).to.has.property('qrveyid').to.be.a('string');
            expect(response.qrvey).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(response.qrvey).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(response.qrvey).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(response.qrvey).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(response.qrvey).to.has.property('status').to.be.eq('RUNNING');
        });
    });

    after(async function () {
        await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);
    });
});
