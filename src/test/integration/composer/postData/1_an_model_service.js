'use strict';
const { DateTime } = require('luxon');
const { expect } = require('chai');
const { find: _find } = require('lodash');

const {
    modelService: ModelService,
} = require('../../../../lib/postData');

const UserService = require('../../../../lib/userService');
const ComposerHelper = require('../../../helpers/composerHelper');
const DatasetHelper = require('../../../helpers/datasetHelper');

const {
    GENERAL_USER_ID,
} = require('../../../config/data');

describe('Data view: An model', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    let appName = 'Basic load process';

    let composerHelperItem, datasetItem;
    before(async function () {
        composerHelperItem = await ComposerHelper.createBasicApplication(GENERAL_USER_ID, { name: appName });
        datasetItem = _find(composerHelperItem.datasets, { name: 'Mysql Dataset - Clients' });
        await DatasetHelper.loadDataset(GENERAL_USER_ID, composerHelperItem.appId, datasetItem.datasetId, { wait: true });
        return Promise.resolve();
    });

    describe('x-api-key: Basic AN model', function () {
        it('Get records', async function () {
            let body = {
                logic: [],
            };
            let result = await ModelService.getModel(GENERAL_USER_ID, composerHelperItem.appId, datasetItem.datasetId, body);

            expect(result).to.be.a('object');
            expect(result).to.has.property('numOfRows').to.be.a('number').to.be.eq(1000);
            expect(result).to.has.property('numResponses').to.be.a('number').to.be.eq(1000);
            return Promise.resolve();
        });
        
        it('Get records using an invalid questionId', async function () {
            const WRONG_QUESTION_ID = 'WRONG_QUESTION_ID';

            let body = {
                logic: [
                    {
                        enabled: true,
                        filters: [
                            {
                                operator: 'AND',
                                expressions: [
                                    {
                                        questionid: WRONG_QUESTION_ID,
                                        questionType: 'TEXT_LABEL',
                                        validationType: 'EQUAL',
                                        value: [
                                            'Germany'
                                        ],
                                        enabled: true
                                    }
                                ]
                            }
                        ],
                        scope: 'GLOBAL'
                    }
                ]
            };

            let failed = true;
            try {
                await ModelService.getModel(GENERAL_USER_ID, composerHelperItem.appId, datasetItem.datasetId, body);
                failed = false;
            } catch (error) {
                if (failed === false) {
                    return Promise.reject(new Error('It did not fail'));
                }

                expect(error).to.be.an('object');
                expect(error).to.has.property('errors').to.be.an('array').to.has.lengthOf(1);
                expect(error.errors[0]).to.has.property('message').to.be.an('string').to.be.eq('The requested resource does not exist.');
                expect(error.errors[0]).to.has.property('type').to.be.an('object')
                    .to.has.property('msg').to.be.a('string').to.be.eq(`Question id [${WRONG_QUESTION_ID}] does not exist.`);
            }
            return Promise.resolve();
        });
    });

    describe('RLS: Basic AN model', function () {
        let token;

        it('Generate permissions', async function () {
            let body = {
                userid: GENERAL_USER_ID,
                appid: composerHelperItem.appId,
                permissions: [{
                    dataset_id: datasetItem.datasetId,
                    record_permissions: [{
                        security_name: 'company',
                        values: [
                            'Meevee', 'Jayo', 'Voolia'
                        ]
                    }],
                }],
            };
            let data = await UserService.generateLoginToken(body);
            token = data.token;
            return Promise.resolve();
        });

        it('Get records', async function () {
            let body = {
                logic: [],
            };

            let params = {
                authorization: `Bearer ${token}`,
            };

            let result = await ModelService.getModel(GENERAL_USER_ID, composerHelperItem.appId, datasetItem.datasetId, body, params);

            expect(result).to.be.a('object');
            expect(result).to.has.property('numOfRows').to.be.a('number').to.be.eq(1000);
            expect(result).to.has.property('numResponses').to.be.a('number').to.be.eq(27);
            return Promise.resolve();
        });
    });

    after(async function () {
        await composerHelperItem.deleteAll();
    });

});
