'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');
const formulaService = require('../../../../../lib/postData/formulaService');
const bucketService = require('../../../../../lib/postData/bucketService');

function defaultFormula(questionId) {
    return {
        name: 'Formula test Module',
        formula: `[${questionId}] / 2`,
        formulaType: 'number'
    }
}

async function createFormula(config, questionid, formulaFn) {
    const params = formulaFn ?? defaultFormula(questionid)
    const formula = await formulaService.createFormula(config, params)
    return formula.formulaId;
}

async function createBucket(config, questionid) {
    const bucketBody = {
        columnId: questionid,
        columnType: 'NUMERIC',
        bucketType: 'NUMERIC',
        numericType: 'BASIC',
        basicNumericValueType: 'DYNAMIC',
        min: 8,
        max: 26,
        bucketQuantity: 9,
        name: 'Bucket test'
    }
    const bucket = await bucketService.createBucket(config, bucketBody)
    return bucket.bucketId
}

async function deleteFormula(config, formulaId) {
    await formulaService.deleteFormula({ ...config, formulaId })
}

async function deleteBucket(config, bucketId) {
    await bucketService.deleteBucket({ ...config, bucketId })
}

/** Function to check if an array is sorted in ascending order  */
function isSortedAsc(array) {
    for (let i = 0; i < array.length - 1; i++) {
        if (array[i] > array[i + 1]) {
            return false;
        }
    }
    return true;
}

context('Virtuals: Formulas and Buckets', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit Formulas and Buckets for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            describe('Virtual columns', () => {
                let formulaId, bucketId, chartGenerator
                beforeEach(async () => {
                    await sleep(time)
                    chartGenerator = new BodyGenerator('chart', columns)
                    const questionid = chartGenerator.defaults['numerics'][0].id

                    const formulaBody = {
                        name: 'Formula test',
                        formula: `[${questionid}] + 1`,
                        formulaType: 'number'
                    }
                    const formula = await formulaService.createFormula(config, formulaBody)
                    formulaId = formula.formulaId

                    const bucketBody = {
                        columnId: questionid,
                        columnType: 'NUMERIC',
                        bucketType: 'NUMERIC',
                        numericType: 'BASIC',
                        basicNumericValueType: 'DYNAMIC',
                        min: 8,
                        max: 26,
                        bucketQuantity: 9,
                        name: 'Bucket test'
                    }
                    const bucket = await bucketService.createBucket(config, bucketBody)
                    bucketId = bucket.bucketId
                })

                it('Formula Group + Numeric Column', async () => {
                    const body = chartGenerator.dimension({ customId: formulaId }).sort().summary().toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(Number(data[0].key)).to.be.eq(9)
                    expect(data[0].summary[0]).to.be.eq(3392)
                })

                it('String group + Formula Column', async () => {
                    const body = chartGenerator.dimension().sort().summary({ customId: formulaId }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data[0].key).to.be.eq('A')
                    expect(data[0].summary[0]).to.be.closeTo(353692.5, 0.1)
                })

                it('String group + Formula Column', async () => {
                    const body = chartGenerator.dimension().sort().summary({ customId: formulaId }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data[0].key).to.be.eq('A')
                    expect(data[0].summary[0]).to.be.closeTo(353692.5, 0.1)
                })

                it('Formula Group + Sorting ASC', async () => {
                    const body = chartGenerator.dimension({ customId: formulaId }).sort().toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data || []
                    const listOfKeys = data.map(d => Number(d.key))
                    expect(isSortedAsc(listOfKeys)).to.be.true
                })

                it('Formula Column + Sorting ASC', async () => {
                    const body = chartGenerator.dimension().summary({ customId: formulaId }).sort().toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data[0].summary[0]).to.be.closeTo(347403.2, 0.1)
                    expect(data[1].summary[0]).to.be.closeTo(349618.7, 0.1)
                    expect(data[2].summary[0]).to.be.closeTo(350433.2, 0.1)
                    expect(data[3].summary[0]).to.be.closeTo(353692.5, 0.1)
                })

                it('Filter by Formula', async () => {
                    const values = [{ 'lt': '10' }]
                    const validation = 'RANGE'
                    const column = { customId: formulaId }
                    const body = chartGenerator.dimension(column).sort().filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(2)
                    expect(Number(data[0].key)).to.be.eq(9)
                    expect(Number(data[1].key)).to.be.eq(9.5)
                })

                it('Formula with Totals and Subtotals', async () => {
                    const body = chartGenerator
                        .dimension()
                        .dimension({ customId: formulaId })
                        .sort()
                        .summary({ customId: formulaId })
                        .addTotals()
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(chartItem[0].summary[0]).to.be.closeTo(1401147.6, 0.1)
                    expect(data[0].summary[0]).to.be.closeTo(353692.5, 0.1)
                })

                it('Trend by Formula column', async () => {
                    const body = chartGenerator.dimension().sort().summary().inner({ customId: formulaId }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const inner = chartItem[0].data[0].innerCharts[0].charts[0]
                    expect(inner.data[0].summary[0]).to.be.closeTo(872, 0.1)
                    expect(Number(inner.data[0].key)).to.be.eq(9)
                })

                afterEach(async () => {
                    await formulaService.deleteFormula({ ...config, formulaId })
                    await bucketService.deleteBucket({ ...config, bucketId })
                })
            })

            describe('Virtual columns: formulas', () => {
                let formulaId, chartGenerator

                beforeEach(async () => {
                    await sleep(time)
                    chartGenerator = new BodyGenerator('chart', columns)
                    const questionid = chartGenerator.defaults['numerics'][0].id
                    formulaId = await createFormula(config, questionid);
                })

                it('Filter by Formula', async () => {
                    const values = [{ gt: 4 }]
                    const validation = 'RANGE'
                    const column = { customId: formulaId }
                    const body = chartGenerator
                        .dimension(column)
                        .sort()
                        .filter()
                        .expression({ values, validation, ...column })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(95)
                    expect(parseFloat(data[0].key)).to.be.closeTo(4.25, 0.01)
                    expect(parseFloat(data[1].key)).to.be.closeTo(4.5, 0.1)
                    expect(parseFloat(data[2].key)).to.be.closeTo(4.75, 0.01)
                    expect(parseFloat(data[3].key)).to.be.closeTo(5, 0.1)
                })

                it('Formula with Totals and Subtotals', async () => {
                    const body = chartGenerator
                        .dimension()
                        .dimension({ customId: formulaId })
                        .sort()
                        .summary({ customId: formulaId })
                        .addTotals()
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(chartItem[0].summary[0]).to.be.closeTo(658170.3, 0.1)
                    expect(data[0].key).to.be.eq('A')
                    expect(data[0].summary[0]).to.be.closeTo(166138.75, 0.01)
                })

                it('Trend by Formula column', async () => {
                    const values = [{ gte: 4 }]
                    const validation = 'RANGE'
                    const column = { customId: formulaId }
                    const body = chartGenerator
                        .dimension()
                        .sort()
                        .summary()
                        .filter()
                        .expression({ values, validation, ...column })
                        .inner({ customId: formulaId })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const inner = chartItem[0].data[0].innerCharts[0].charts[0]
                    expect(parseFloat(inner.data[0].key)).to.be.closeTo(4, 0.1)
                    expect(inner.data[0].summary[0]).to.be.closeTo(872, 0.1)
                    expect(parseFloat(inner.data[1].key)).to.be.closeTo(4.25, 0.1)
                    expect(inner.data[1].summary[0]).to.be.closeTo(858.5, 0.1)
                    expect(parseFloat(inner.data[2].key)).to.be.closeTo(4.5, 0.01)
                    expect(inner.data[2].summary[0]).to.be.closeTo(432, 0.1)
                })

                it('Aggregated Formula', async () => {
                    const body = chartGenerator
                        .dimension()
                        .sort()
                        .summary({pointer: true})
                        .summary({agg: 'COUNT', formula: true})
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(4)
                    expect(data[0].key).to.be.eq('A')
                    expect(data[1].key).to.be.eq('AB')
                    expect(data[2].key).to.be.eq('B')
                    expect(data[3].key).to.be.eq('O')
                    expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                    expect(data[0].summary[1]).to.be.closeTo(332278.5, 0.1)
                    expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                    expect(data[1].summary[1]).to.be.closeTo(329240.2, 0.1)
                    expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                    expect(data[2].summary[1]).to.be.closeTo(326356.2, 0.1)
                    expect(data[3].summary[0]).to.be.closeTo(328468.7, 0.1)
                    expect(data[3].summary[1]).to.be.closeTo(328469.7, 0.1)
                })

                afterEach(async () => {
                    await formulaService.deleteFormula({ ...config, formulaId })
                })
            })

            describe('Virtual columns: formulas - Raw', () => {
                let formulaId, rowsGenerator

                beforeEach(async () => {
                    await sleep(time)
                    rowsGenerator = new BodyGenerator('rows', columns)
                    const questionid = rowsGenerator.defaults['numerics'][0].id
                    formulaId = await createFormula(config, questionid);
                })

                it('Raw Formula', async () => {
                    const body = rowsGenerator.dimension({ customId: formulaId }).sort().toBody()
                    const chartItem = await resultsService.rowsResult(config, body)

                    expect(chartItem).to.has.property('totalRecords').to.be.a('number').to.be.eq(100000)
                    expect(chartItem).to.has.property('items').to.be.an('object')
                    expect(chartItem.items).to.has.property('data').to.be.an('array').to.have.lengthOf(50)
                    const data = chartItem.items.data
                    expect(data[0]).to.be.an('array').to.have.lengthOf(1)
                    for (const d of data) {
                        expect(d[0]).to.be.a('number')
                    }
                    expect(data[0][0]).to.be.closeTo(4, 0.1)
                })

                afterEach(async () => {
                    await deleteFormula(config, formulaId)
                })
            })

            describe('Virtual columns: buckets - Raw', () => {
                let bucketId, rowsGenerator

                beforeEach(async () => {
                    await sleep(time)
                    rowsGenerator = new BodyGenerator('rows', columns)
                    const questionid = rowsGenerator.defaults['numerics'][0].id
                    bucketId = await createBucket(config, questionid);
                })

                it('Raw bucket', async () => {
                    const body = rowsGenerator.dimension({ customId: bucketId }).toBody()
                    const chartItem = await resultsService.rowsResult(config, body)

                    expect(chartItem).to.has.property('totalRecords').to.be.a('number').to.be.eq(100000)
                    expect(chartItem).to.has.property('items').to.be.an('object')
                    expect(chartItem.items).to.has.property('data').to.be.an('array').to.have.lengthOf(50)
                    const data = chartItem.items.data
                    expect(data).to.be.an('array').to.have.lengthOf(50)
                    for (const d of data) {
                        expect(typeof d[0]).to.be.oneOf(['string', 'object']) /** There can be nulls */
                    }
                })

                afterEach(async () => {
                    await deleteBucket(config, bucketId)
                })
            })

            describe('Virtual columns: formulas with filters', () => {
                let formulaId, chartGenerator, questionid;

                beforeEach(async () => {
                    await sleep(time)
                    chartGenerator = new BodyGenerator('chart', columns)
                })

                it('Fomula Numeric + Equals', async () => {
                    questionid = chartGenerator.defaults['numerics'][0].id
                    formulaId = await createFormula(config, questionid);

                    const values = [4]
                    const validation = 'EQUAL'
                    const column = { customId: formulaId }
                    const body = chartGenerator.dimension(column).sort()
                        .summary()
                        .filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(1)
                    expect(parseFloat(data[0].key)).to.be.closeTo(4, 0.1)
                    expect(data[0].summary[0]).to.be.closeTo(3392, 0.1)
                })
                
                it('Fomula String + Equals', async () => {
                    questionid = chartGenerator.defaults['strings'][0].id
                    const params = {
                        formula: `LOWER([${questionid}])`,
                        formulaMode: "STANDARD",
                        formulaType: "string",
                        name: 'Formula test Module',
                    }
                    formulaId = await createFormula(config, questionid, params);

                    const values = ['ab']
                    const validation = 'EQUAL'
                    const column = { customId: formulaId }
                    const body = chartGenerator.dimension(column).sort()
                        .summary()
                        .filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(1)
                    expect(data[0].key).to.be.eq('ab')
                    expect(data[0].summary[0]).to.be.closeTo(329239.2, 0.1)
                })
                
                it('Fomula Date + Equals', async () => {
                    questionid = chartGenerator.defaults['dates'][0].id
                    const params = {
                        formula: `YEAR([${questionid}])`,
                        formulaMode: "STANDARD",
                        formulaType: "number",
                        name: 'Formula test Module'
                    }
                    formulaId = await createFormula(config, questionid, params);

                    const values = [1954]
                    const validation = 'EQUAL'
                    const column = { customId: formulaId }
                    const body = chartGenerator.dimension(column).sort()
                        .summary()
                        .filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(1)
                    expect(data[0].key).to.be.eq(1954)
                    expect(data[0].summary[0]).to.be.closeTo(12162.2, 0.1)
                })

                afterEach(async () => {
                    await deleteFormula(config, formulaId)
                })
            })

            describe('Virtual columns: Buckets with filters', () => {
                let bucketId, chartGenerator, questionid;

                beforeEach(async () => {
                    await sleep(time)
                    chartGenerator = new BodyGenerator('chart', columns)
                    const questionid = chartGenerator.defaults['numerics'][0].id
                    bucketId = await createBucket(config, questionid);
                })

                it('Bucket + Equals', async () => {
                    const values = ['14-16']
                    const validation = 'EQUAL'
                    const column = { customId: bucketId }
                    const body = chartGenerator.dimension(column).sort()
                        .summary({ agg: 'COUNT' })
                        .filter()
                        .expression({ values, validation, ...column })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(1)
                    expect(data[0].key).to.be.eq('14-16')
                    expect(data[0].summary[0]).to.be.closeTo(26643, 0.1)
                })

                it('Bucket + Not Equals', async () => {
                    const values = ['14-16']
                    const validation = 'NOT_EQUAL'
                    const column = { customId: bucketId }
                    const body = chartGenerator.dimension(column).sort()
                        .summary({ agg: 'COUNT' })
                        .filter()
                        .expression({ values, validation, ...column })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data.some(d => d.key === '14-16')).to.be.false
                    expect(data).to.have.lengthOf(8)
                })

                afterEach(async () => {
                    await deleteBucket(config, bucketId)
                })
            })

            describe('Virtual columns: Buckets', () => {
                let bucketId, chartGenerator;

                beforeEach(async () => {
                    await sleep(time)
                    chartGenerator = new BodyGenerator('chart', columns)
                    const questionid = chartGenerator.defaults['numerics'][0].id
                    bucketId = await createBucket(config, questionid);
                })

                it('Bucket Group + Numeric Column', async () => {
                    const expectedData = [
                        { key: '8-10', summary: [12242] },
                        { key: '10-12', summary: [65881.5] },
                        { key: '12-14', summary: [193092.3] },
                        { key: '14-16', summary: [397026.5] },
                        { key: '16-18', summary: [328260.6] },
                        { key: '18-20', summary: [213370.3] },
                        { key: '20-22', summary: [72548.9] },
                        { key: '22-24', summary: [22654.5] },
                        { key: '24-26', summary: [11264] }
                    ]
                    /** Special validation for ES due to AN-18270 */
                    if (name === 'Managed') {
                        const first = expectedData.shift()
                        if (first?.key) expectedData.push(first)
                    }
                    const body = chartGenerator
                        .dimension({ customId: bucketId })
                        .sort()
                        .summary({ agg: 'SUM' })
                        .addTotals()
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(chartItem[0].summary[0]).to.be.closeTo(1316340.6, 0.1)
                    expect(data).to.have.lengthOf(9)
                    for (let i = 0; i < data.length; i++) {
                        expect(data[i].key).to.be.eq(expectedData[i].key)
                        expect(data[i].summary[0]).to.be.closeTo(expectedData[i].summary[0], 0.1)
                    } 
                })

                it('String group + Bucket Column', async () => {
                    const body = chartGenerator
                        .dimension()
                        .sort()
                        .summary({ customId: bucketId, agg: 'COUNT' })
                        .addTotals()
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(chartItem[0].summary[0]).to.be.closeTo(84807, 0.1)
                    expect(data).to.have.lengthOf(4)
                    expect(data[0].key).to.be.eq('A')
                    expect(data[0].summary[0]).to.be.closeTo(21415, 0.1)
                    expect(data[1].key).to.be.eq('AB')
                    expect(data[1].summary[0]).to.be.closeTo(21194, 0.1)
                    expect(data[2].key).to.be.eq('B')
                    expect(data[2].summary[0]).to.be.closeTo(21048, 0.1)
                    expect(data[3].key).to.be.eq('O')
                    expect(data[3].summary[0]).to.be.closeTo(21150, 0.1)
                })

                it('Bucket Column + Sorting ASC', async () => {
                    const expectedData = [
                        { key: '8-10' },
                        { key: '10-12' },
                        { key: '12-14' },
                        { key: '14-16' },
                        { key: '16-18' },
                        { key: '18-20' },
                        { key: '20-22' },
                        { key: '22-24' },
                        { key: '24-26' }
                    ]
                    /** Special validation for ES due to AN-18270 */
                    if (name === 'Managed') {
                        const first = expectedData.shift()
                        if (first?.key) expectedData.push(first)
                    }
                    const body = chartGenerator.dimension({ customId: bucketId }).sort().toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(9)
                    for (let i = 0; i < data.length; i++) {
                        expect(data[i].key).to.be.eq(expectedData[i].key)
                        expect(data[i].summary[0]).to.be.undefined
                    } 
                })

                it('Bucket Group + Sorting DESC', async () => {
                    const expectedData = [
                        { key: '24-26' },
                        { key: '22-24' },
                        { key: '20-22' },
                        { key: '18-20' },
                        { key: '16-18' },
                        { key: '14-16' },
                        { key: '12-14' },
                        { key: '10-12' },
                        { key: '8-10' }
                    ]
                    /** Special validation for ES due to AN-18270 */
                    if (name === 'Managed') {
                        const last = expectedData.pop()
                        if (last?.key) expectedData.unshift(last)
                    }
                    const body = chartGenerator.dimension({ customId: bucketId }).sort({ direction: 'DESC' }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(9)
                    for (let i = 0; i < data.length; i++) {
                        expect(data[i].key).to.be.eq(expectedData[i].key)
                        expect(data[i].summary[0]).to.be.undefined
                    } 
                })

                it('Filter by Bucket', async () => {
                    const values = ['10-12', '12-14']
                    const validation = 'EQUAL'
                    const column = { customId: bucketId }
                    const body = chartGenerator.dimension(column)
                        .filter()
                        .expression({ values, validation, ...column })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(2)
                    for (const d of data) {
                        expect(d.key).to.be.oneOf(values)
                    }
                })

                it('Bucket with Totals and Subtotals', async () => {
                    const body = chartGenerator
                        .dimension()
                        .dimension({ customId: bucketId })
                        .sort()
                        .summary({ customId: bucketId, agg: 'COUNT' })
                        .addTotals()
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(chartItem[0].summary[0]).to.be.equal(84807)
                    expect(data[0].key).to.be.eq('A')
                    expect(data[0].summary[0]).to.be.equal(21415)
                })

                it('Trend by Bucket column', async () => {
                    const expectedData = [
                        { key: '8-10' },
                        { key: '10-12' },
                        { key: '12-14' },
                        { key: '14-16' },
                        { key: '16-18' },
                        { key: '18-20' },
                        { key: '20-22' },
                        { key: '22-24' },
                        { key: '24-26' }
                    ]
                    /** Special validation for ES due to AN-18270 */
                    if (name === 'Managed') {
                        const first = expectedData.shift()
                        if (first?.key) expectedData.push(first)
                    }
                    const body = chartGenerator
                        .dimension()
                        .sort()
                        .summary()
                        .inner({ customId: bucketId })
                        .toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const inner = chartItem[0].data[0].innerCharts[0].charts[0]
                    for (let i = 0; i < inner.length; i++) {
                        expect(inner[i].key).to.be.eq(expectedData[i].key)
                        expect(inner[i].summary[0]).to.be.undefined
                    }
                })

                afterEach(async () => {
                    await deleteBucket(config, bucketId);
                })
            })
        })
    })
})
