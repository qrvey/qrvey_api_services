'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');
const formulaService = require('../../../../../lib/postData/formulaService');

context('Results services', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            it('It should validate Columns lengh', () => {
                expect(config).to.be.an('object');
                expect(config.qrveyId).to.be.an('string');
                expect(columns).to.be.an('array').to.have.lengthOf(47); // Amount of columns in the default dataset
                expect(columns[0]).to.has.property('id').to.be.a('string');
            })

            describe('Chart endpoint', () => {
                let generator
                beforeEach(async () => {
                    await sleep(time)
                    generator = new BodyGenerator('chart', columns)
                })

                it('Basic X/Y Columns: String + number (Sum agg)', async () => {
                    const body = generator.dimension().sort().summary().toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    expect(chartItem).to.be.an('array')
                    expect(chartItem).to.have.lengthOf(1)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(4)
                    expect(data[0]).to.have.property('key')
                    expect(data[0].key).to.be.eq('A')
                    expect(data[1].key).to.be.eq('AB')
                    expect(data[2].key).to.be.eq('B')
                    expect(data[3].key).to.be.eq('O')
                    expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                    expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                    expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                    expect(data[3].summary[0]).to.be.closeTo(328468.7, 0.1)
                })

                describe('Simple: X or Y', () => {

                    const commonXAgg = (chartItem) => {
                        expect(chartItem).to.be.an('array')
                        const data = chartItem[0]
                        expect(data).to.have.property('data')
                        expect(data.data).to.be.an('array').to.have.lengthOf(0)
                        expect(data).to.have.property('summary')
                        expect(data.summary).to.be.an('array').to.have.lengthOf(1)
                        return data
                    }

                    it('Only String Column in Category', async () => {
                        const body = generator.dimension().sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0]).to.have.property('summary')
                        expect(data[0].summary).to.be.an('array').to.have.lengthOf(0)
                    })

                    it('Only Numeric Column in Value (SUM Agg)', async () => {
                        const body = generator.summary().addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.closeTo(1316340.6, 0.1)
                    })

                    it('Only Numeric Column in Value (AVG Agg)', async () => {
                        const body = generator.summary({ agg: 'AVG' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.closeTo(15.52, 0.01)
                    })

                    it('Only Numeric Column in Value (MEDIAN Agg)', async () => {
                        const body = generator.summary({ agg: 'MEDIAN' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.eq(15.5)
                    })

                    it('Only Numeric Column in Value (COUNT Agg)', async () => {
                        const body = generator.summary({ agg: 'COUNT' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.eq(84807)
                    })

                    it('Only Numeric Column in Value (DISTINCTCOUNT Agg)', async () => {
                        const body = generator.summary({ agg: 'DISTINCTCOUNT' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.eq(96)
                    })

                    it('Only Numeric Column in Value (STDEV Agg)', async () => {
                        const body = generator.summary({ agg: 'STDEV' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.closeTo(2.79, 0.01)
                    })

                    it('Only Numeric Column in Value (MIN Agg)', async () => {
                        const body = generator.summary({ agg: 'MIN' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.eq(8)
                    })

                    it('Only Numeric Column in Value (MAX Agg)', async () => {
                        const body = generator.summary({ agg: 'MAX' }).addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = commonXAgg(chartItem)
                        expect(data.summary[0]).to.be.eq(24.8)
                    })
                })

                describe('Dates groups', () => {

                    it('Column: Full date', async () => {
                        const date = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(date).size(2).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('07/24/1954')
                        expect(data[1].key).to.be.eq('07/25/1954')
                    })

                    it('Column: Year', async () => {
                        const date = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)
                        expect(data[0].key).to.be.eq('1954')
                        expect(data[1].key).to.be.eq('1955')
                        expect(data[48].key).to.be.eq('2002')
                    })

                    it('Column: Quarter, year', async () => {
                        const date = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)
                        expect(data[0].key).to.be.eq('Q3 1954')
                        expect(data[1].key).to.be.eq('Q4 1954')
                        expect(data[190].key).to.be.eq('Q1 2002')
                        expect(data[191].key).to.be.eq('Q2 2002')
                    })

                    it('Column: Month, year', async () => {
                        const date = { type: 'dates', dateGroup: 'MONTH' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[6].key).to.be.eq('Jan 1955')
                        expect(data[7].key).to.be.eq('Feb 1955')
                        expect(data[8].key).to.be.eq('Mar 1955')
                        expect(data[9].key).to.be.eq('Apr 1955')
                        expect(data[10].key).to.be.eq('May 1955')
                        expect(data[11].key).to.be.eq('Jun 1955')
                        expect(data[12].key).to.be.eq('Jul 1955')
                        expect(data[13].key).to.be.eq('Aug 1955')
                        expect(data[14].key).to.be.eq('Sep 1955')
                        expect(data[15].key).to.be.eq('Oct 1955')
                        expect(data[16].key).to.be.eq('Nov 1955')
                        expect(data[17].key).to.be.eq('Dec 1955')
                    })

                    it('Column: Week, year', async () => {
                        const date = { type: 'dates', dateGroup: 'WEEK' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('W29 1954')
                        expect(data[1].key).to.be.eq('W30 1954')
                        expect(data[20].key).to.be.eq('W49 1954')
                    })

                    it('Column: Quarter', async () => {
                        const date = { type: 'dates', dateGroup: 'QUARTER_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0].key).to.be.eq('Q1')
                        expect(data[1].key).to.be.eq('Q2')
                        expect(data[2].key).to.be.eq('Q3')
                        expect(data[3].key).to.be.eq('Q4')
                    })

                    it('Column: Month', async () => {
                        const date = { type: 'dates', dateGroup: 'MONTH_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)
                        expect(data[0].key).to.be.eq('Jan')
                        expect(data[1].key).to.be.eq('Feb')
                        expect(data[2].key).to.be.eq('Mar')
                        expect(data[3].key).to.be.eq('Apr')
                        expect(data[4].key).to.be.eq('May')
                        expect(data[5].key).to.be.eq('Jun')
                        expect(data[6].key).to.be.eq('Jul')
                        expect(data[7].key).to.be.eq('Aug')
                        expect(data[8].key).to.be.eq('Sep')
                        expect(data[9].key).to.be.eq('Oct')
                        expect(data[10].key).to.be.eq('Nov')
                        expect(data[11].key).to.be.eq('Dec')
                    })

                    it('Column: Day (Month)', async () => {
                        const date = { type: 'dates', dateGroup: 'DAY_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)
                        expect(data[0].key).to.be.eq(1)
                        expect(data[30].key).to.be.eq(31)
                    })

                    it('Column: Date, Hour', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'HOUR' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:00:00')
                        expect(data[6].key).to.be.eq('07/25/1954 18:00:00')
                    })

                    it('Column: Date, Minute', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'MINUTE' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:06:00')
                        expect(data[6].key).to.be.eq('07/25/1954 18:39:00')
                    })

                    it('Column: Date, Second', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'SECOND' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:06:50')
                        expect(data[6].key).to.be.eq('07/25/1954 18:39:04')
                    })
                    
                    it('Column: Hour', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'HOUR_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(0)
                        expect(data[6].key).to.be.eq(20)
                    })
                    
                    it('Column: Minute', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'MINUTE_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(6)
                        expect(data[6].key).to.be.eq(51)
                    })
                    
                    it('Column: Second', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'SECOND_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(0)
                        expect(data[6].key).to.be.eq(53)
                    })
                })

                describe('Sorting in X/Y Charts', () => {

                    it('Category ASC', async () => {
                        const body = generator.dimension().sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                    })

                    it('Category DESC', async () => {
                        const body = generator.dimension().sort({ direction: 'DESC' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('O')
                        expect(data[1].key).to.be.eq('B')
                        expect(data[2].key).to.be.eq('AB')
                        expect(data[3].key).to.be.eq('A')
                    })

                    it('Value ASC', async () => {
                        const body = generator.dimension().summary().sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('summary')
                        expect(data[0].summary[0]).to.be.closeTo(326355.2, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(328468.7, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(332277.5, 0.1)
                    })

                    it('Value DESC', async () => {
                        const body = generator.dimension().summary().sort({ direction: 'DESC' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('summary')
                        expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(328468.7, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(326355.2, 0.1)
                    })

                    it('Other columns: numeric (Sum) ASC', async () => {
                        const body = generator.dimension().summary().summary().sort({ hidden: true }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('B')
                        expect(data[1].key).to.be.eq('O')
                        expect(data[2].key).to.be.eq('AB')
                        expect(data[3].key).to.be.eq('A')
                        expect(data[0]).to.have.property('summary')
                        expect(data[0].summary).to.have.lengthOf(1)
                        expect(data[1].summary).to.have.lengthOf(1)
                    })

                    it('Sorting DESC by 2nd Group', async () => {
                        const body = generator.dimension().dimension({ id: 1 }).sort({ direction: 'DESC' }).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].items[0].key).to.be.eq('Trans')
                        expect(data[1].items[0].key).to.be.eq('Trans')
                    })

                    it('Two (2) groups Sorting in Both', async () => {
                        const body = generator.dimension({ id: 1 }).sort().dimension().sort({ direction: 'DESC' })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[1].items[0].key).to.be.eq('O')
                        expect(data[2].items[1].key).to.be.eq('B')
                        expect(data[5].key).to.be.eq('Trans')
                    })
                })

                describe('Table calculations', () => {

                    it('Basic X/Y + Running Average', async () => {
                        const body = generator.dimension().sort().summary().calculation().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[1].summary[0]).to.be.closeTo(330758.35, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(329290.63, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(329085.14, 0.1)
                    })

                    it('Basic X/Y + Running SUM', async () => {
                        const body = generator.dimension().sort().summary().calculation({ aggregate: 'SUM' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[1].summary[0]).to.be.closeTo(661516.7, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(987871.89, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(1316340.59, 0.1)
                    })

                    it('Basic X/Y + Running MIN', async () => {
                        const body = generator.dimension().sort().summary().calculation({ aggregate: 'MIN' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(326355.2, 0.1)
                    })

                    it('Basic X/Y + Running MAX', async () => {
                        const body = generator.dimension().sort().summary().calculation({ aggregate: 'MAX' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[1].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(332277.5, 0.1)
                    })

                    it('Basic X/Y + Running DIFF', async () => {
                        const body = generator.dimension().sort().summary().calculation({ aggregate: 'DIFF' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[2].summary[0]).to.be.closeTo(-2884, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(2113.5, 0.1)
                    })

                    it('Basic X/Y + Running PDIFF', async () => {
                        const body = generator.dimension().sort().summary().calculation({ aggregate: 'PDIFF' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[2].summary[0]).to.be.closeTo(-0.87, 0.01)
                        expect(data[3].summary[0]).to.be.closeTo(0.64, 0.01)
                    })

                    it('2G+1C: Agg AVG Running MIN Reset: 0', async () => {
                        const body = generator.dimension().sort().dimension({ id: 1 })
                            .summary({ agg: 'AVG' }).calculation({ aggregate: 'MIN', reset: 0 })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].items[1].summary[0]).to.be.closeTo(15.50, 0.01)
                        expect(data[0].items[2].summary[0]).to.be.closeTo(15.45, 0.01)
                        expect(data[0].items[2].summary[0]).to.be.eq(data[0].items[3].summary[0])
                    })

                    it('2G+1C: Agg AVG Running MIN direction: vertical', async () => {
                        const body = generator.dimension({ id: 1 }).sort().dimension({ id: 2 }).sort()
                            .summary({ agg: 'AVG' }).calculation({ aggregate: 'MIN', direction: 'vertical' })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[1].items[0].summary[0]).to.be.closeTo(16.83, 0.01)
                        expect(data[2].items[0].summary[0]).to.be.closeTo(16.76, 0.01)
                        expect(data[2].items[0].summary[0]).to.be.eq(data[3].items[0].summary[0])
                    })
                })

                it('Basic X/Y with Max data point of 2 ', async () => {
                    const body = generator.dimension({ id: 2 }).size(2).sort({direction: 'DESC'}).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(2)
                })

                describe('Filters', () => {

                    it('Column String + EQUAL', async () => {
                        const values = ['AB']
                        const body = generator.dimension().filter().expression({ values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('AB')
                    })

                    it('Column String + NOT_EQUAL', async () => {
                        const values = ['AB']
                        const validation = 'NOT_EQUAL'
                        const body = generator.dimension().filter().expression({ values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('B')
                        expect(data[2].key).to.be.eq('O')
                    })

                    it('Column String + START_WITH', async () => {
                        const values = ['Mal', 'Femal']
                        const validation = 'START_WITH'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('Female')
                        expect(data[1].key).to.be.eq('Male')
                    })

                    it('Column String + NOT_START_WITH', async () => {
                        const values = ['Mal']
                        const validation = 'NOT_START_WITH'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(5)
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[1].key).to.be.eq('Female')
                        expect(data[2].key).to.be.eq('Genderqueer')
                        expect(data[3].key).to.be.eq('Pangender')
                        expect(data[4].key).to.be.eq('Trans')
                    })

                    it('Column String + END_WITH', async () => {
                        const values = ['le']
                        const validation = 'END_WITH'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('Female')
                        expect(data[1].key).to.be.eq('Male')
                    })
                    
                    it('Column String + IS_EMPTY', async () => {
                        const values = ['Is Null']
                        const validation = 'IS_EMPTY'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column String + IS_NOT_EMPTY', async () => {
                        const values = ['Is Not Null']
                        const validation = 'IS_NOT_EMPTY'
                        const body = generator.dimension({ id: 1 }).sort().summary({ id: 1, agg: 'COUNT', type: 'strings' }).filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(6)
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[0].summary[0]).to.be.eq(16552)
                        expect(data[5].key).to.be.eq('Trans')
                        expect(data[5].summary[0]).to.be.eq(16687)
                    })

                    it('Column String + NOT_END_WITH', async () => {
                        const values = ['le']
                        const validation = 'NOT_END_WITH'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[1].key).to.be.eq('Genderqueer')
                        expect(data[2].key).to.be.eq('Pangender')
                        expect(data[3].key).to.be.eq('Trans')
                    })

                    it('Column String + CONTAIN', async () => {
                        const values = ['gen']
                        const validation = 'CONTAIN'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[1].key).to.be.eq('Genderqueer')
                        expect(data[2].key).to.be.eq('Pangender')
                    })

                    it('Column String + NOT_CONTAIN', async () => {
                        const values = ['gen']
                        const validation = 'NOT_CONTAIN'
                        const body = generator.dimension({ id: 1 }).sort().filter().expression({ id: 1, values, validation }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq('Female')
                        expect(data[1].key).to.be.eq('Male')
                        expect(data[2].key).to.be.eq('Trans')
                    })

                    it('Column Numeric + Less than', async () => {
                        const values = [{ "lt": "9" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq(8)
                        expect(data[1].key).to.be.eq(8.5)
                    })

                    it('Column Numeric + Less than or Equal to', async () => {
                        const values = [{ "lte": "9" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq(8)
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[2].key).to.be.eq(9)
                    })

                    it('Column Numeric + Greather than', async () => {
                        const values = [{ "gt": "24.6" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(24.8)
                    })

                    it('Column Numeric + Greather than or Equal to', async () => {
                        const values = [{ "gte": "24.6" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq(24.6)
                        expect(data[1].key).to.be.eq(24.8)
                    })

                    it('Column Numeric + Between, inclusive', async () => {
                        const values = [{ "gte": "11.3", "lte": "11.5" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq(11.3)
                        expect(data[1].key).to.be.eq(11.4)
                        expect(data[2].key).to.be.eq(11.5)
                    })

                    it('Column Numeric + Between, exclusive', async () => {
                        const values = [{ "gt": "11.3", "lt": "11.5" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(11.4)
                    })

                it('Column Numeric + IS_EMPTY', async () => {
                    const values = ['Is Null']
                    const validation = 'IS_EMPTY'
                    const column = { type: 'numerics' }
                    const body = generator.dimension(column).sort().filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(0)
                })

                it('Column Numeric + IS_NOT_EMPTY', async () => {
                    const values = ['Is Not Null']
                    const validation = 'IS_NOT_EMPTY'
                    const column = { type: 'numerics' }
                    const body = generator.dimension(column).sort().filter().expression({ values, validation, ...column }).toBody()
                    const chartItem = await resultsService.chartResult(config, body)
                    const data = chartItem[0].data
                    expect(data).to.have.lengthOf(96)
                    expect(data[0].key).to.be.eq(8)
                    expect(data[95].key).to.be.eq(24.8)
                })

                    it('Column full Date + Before', async () => {
                        const values = [{ "lt": "07/25/1954", "dateGroup": "day" }]
                        const validation = 'RANGE'
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('07/24/1954')
                    })

                    it('Column full Date + Before or on', async () => {
                        const values = [{ "lte": "07/25/1954", "dateGroup": "day" }]
                        const validation = 'RANGE'
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('07/24/1954')
                        expect(data[1].key).to.be.eq('07/25/1954')
                    })

                    it('Column full Date + After', async () => {
                        const values = [{ "gt": "07/23/2002", "dateGroup": "day" }]
                        const validation = 'RANGE'
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('07/24/2002')
                    })

                    it('Column full Date + After or on', async () => {
                        const values = [{ "gte": "07/23/2002", "dateGroup": "day" }]
                        const validation = 'RANGE'
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('07/23/2002')
                        expect(data[1].key).to.be.eq('07/24/2002')
                    })

                    it('Column full Date + In + The last X day', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "day" }
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The last X day Starting On', async () => {
                        const values = {
                            cursor: 'the_last',
                            enabled: true,
                            number: 30,
                            unit: 'day',
                            anchor: '07/24/1954',
                            includeCurrent: true,
                            isCalendarDate: true,
                            displayed: true
                        }
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('07/24/1954')
                    })

                    it('Column full Date + In + The last X Week', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "week" }
                        const column = { type: 'dates', dateGroup: 'WEEK' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The last X Week', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "week" }
                        const column = { type: 'dates', dateGroup: 'WEEK' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The last X Month', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "month" }
                        const column = { type: 'dates', dateGroup: 'MONTH' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The last X Quarter', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "quarter" }
                        const column = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The last X Year', async () => {
                        const values = { cursor: "the_last", number: 1, unit: "year" }
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + The next X Quarter', async () => {
                        const values = { cursor: "the_next", number: 1, unit: "quarter" }
                        const column = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + This Quarter', async () => {
                        const values = { cursor: "this", number: 1, unit: "quarter" }
                        const column = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(column).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + IS_EMPTY', async () => {
                        const values = ['Is Null']
                        const validation = 'IS_EMPTY'
                        const column = { type: 'dates', dateGroup: 'day' }
                        const body = generator.dimension(column).sort().filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column full Date + IS_NOT_EMPTY', async () => {
                        const column = { type: 'dates', dateGroup: 'day' }
                        const body = generator.dimension(column).sort()
                            .filter()
                            .expression({ values: [{ gte: '07/24/1954', lte: '07/25/1954' }], validation: 'RANGE', ...column })
                            .expression({ values: ['Is Not Null'], validation: 'IS_NOT_EMPTY', ...column })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('07/24/1954')
                        expect(data[1].key).to.be.eq('07/25/1954')
                    })

                    it('Column year Date + Equals', async () => {
                        const values = ['2002']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('2002')
                    })

                    it('Column Quarter, year Date + Equals', async () => {
                        const values = ['Q2 2002']
                        const column = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('Q2 2002')
                    })

                    it('Column Month, year Date + Equals ', async () => {
                        const values = ['Dec 1992']
                        const column = { type: 'dates', dateGroup: 'MONTH' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('Dec 1992')
                    })

                    it('Column Week, year Date + Equals ', async () => {
                        const values = ['W24 2002']
                        const column = { type: 'dates', dateGroup: 'WEEK' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('W24 2002')
                    })

                    it('Column Quarter Date + Equals', async () => {
                        const values = ['Q3']
                        const column = { type: 'dates', dateGroup: 'QUARTER_ONLY' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('Q3')
                    })

                    it('Column Month Date + Equals', async () => {
                        const values = ['Dec']
                        const column = { type: 'dates', dateGroup: 'MONTH_ONLY' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq('Dec')
                    })

                    it('Column Day (month) Date + Equals', async () => {
                        const values = ['3']
                        const column = { type: 'dates', dateGroup: 'DAY_ONLY' }
                        const body = generator.dimension(column).filter().expression({ values, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(3)
                    })

                    it('Column String AND Column String', async () => {
                        const body = generator.dimension({ id: 1 }).sort()
                            .filter().expression({ id: 1, values: ['Male'] }).expression({ id: 1, values: ['Female'] })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column String OR Column String', async () => {
                        const body = generator.dimension({ id: 1 }).sort()
                            .filter({ operator: 'OR' }).expression({ id: 1, values: ['Male'] }).expression({ id: 1, values: ['Female'] })
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq('Female')
                        expect(data[1].key).to.be.eq('Male')
                    })
                })

                describe('More filters comparisons', () => {
                    /** @type {BodyGenerator} */
                    let generator, valuesToCheck
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                        valuesToCheck = (values) => values.reduce((acc, v) => {
                            const newValues = []
                            if (v.gt || v.gte) newValues.push(v.gt || v.gte)
                            if (v.lt || v.lte) newValues.push(v.lt || v.lte)
                            if (v.anchor) newValues.push(v.anchor)
                            if (!newValues.length) newValues.push(v)
                            return [...acc, ...newValues]
                        }, [])
                    })

                    it('Column Numeric + Equals', async () => {
                        const values = ['23.5']
                        const validation = 'EQUAL'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(23.5)
                    })

                    it('Column Numeric + Does not Equal', async () => {
                        const values = ['8.5']
                        const validation = 'NOT_EQUAL'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.not.include.members([8.5])
                    })

                    it('Column full Date + Equals', async () => {
                        const values = ['07/24/1954']
                        const validation = 'EQUAL'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(1)
                        expect(results[0]).to.be.eq(values[0])
                    })

                    it('Column full Date + Does not Equal', async () => {
                        const values = ['07/24/1954']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.not.include.members(values)
                    })

                    it('Column full Date + Between, inclusive', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/27/1954' }]
                        const validation = 'RANGE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(4)
                        expect(results).to.include.members([values[0].gte, values[0].lte])
                    })

                    it('Column full Date + Between, exclusive', async () => {
                        const values = [{ gt: '07/24/1954', lt: '07/27/1954' }]
                        const validation = 'RANGE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(2)
                        expect(results).to.not.include.members([values[0].gte, values[0].lte])
                    })

                    it('Column full Date + In + The next X Day', async () => {
                        const values = [{ anchor: '07/24/1954', cursor: "the_next", number: 6, unit: "day" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(6)
                    })

                    it('Column full Date + In + The next X Day Starting On', async () => {
                        const values = {
                            cursor: 'the_next',
                            enabled: true,
                            number: 30,
                            unit: 'day',
                            anchor: '07/24/1954',
                            includeCurrent: true,
                            isCalendarDate: true,
                            displayed: true
                        }
                        const column = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(column).summary({ agg: 'COUNT', type: 'dates' }).filter().relative({ ...values }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(30)
                        expect(data[0].key).to.be.eq('07/24/1954')
                        expect(data[0].summary[0]).to.be.eq(2)
                        expect(data[29].key).to.be.eq('08/22/1954')
                        expect(data[29].summary[0]).to.be.eq(3)
                    })

                    it('Column full Date + In + The next X Week', async () => {
                        const values = [{ anchor: '07/24/1954', cursor: "the_next", number: 2, unit: "week" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(8)
                    })

                    it('Column full Date + In + The next X Month', async () => {
                        const values = [{ anchor: '07/24/1954', cursor: "the_next", number: 1, unit: "month" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(8)
                    })

                    it('Column full Date + In + The next X Year', async () => {
                        const values = [{ anchor: '07/24/1954', cursor: "the_next", number: 1, unit: "year" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(158)
                    })

                    it('Column full Date + In + This X Day', async () => {
                        const values = [{ cursor: "this", unit: "day" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + This X Week', async () => {
                        const values = [{ cursor: "this", unit: "week" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(0)
                    })

                    it('Column full Date + In + This X Month', async () => {
                        const values = [{ cursor: "this", unit: "month" }];
                        const validation = 'RELATIVE'
                        const dateGroup = 'day'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.have.lengthOf(0)
                    })

                    it('Column year Date + Does not Equal', async () => {
                        const values = ['1954']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Quarter, year Date + Does not Equal', async () => {
                        const values = ['Q4 1954']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Month, year Date + Does not Equal', async () => {
                        const values = ['Jul 1954']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Week, year Date + Does not Equal', async () => {
                        const values = ['W30 1954']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Quarter Date + Does not Equal', async () => {
                        const values = ['Q1']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'quarter_only'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Month Date + Does not Equal', async () => {
                        const values = ['Jan']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'month_only'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.not.include.members(values)
                    })

                    it('Column Day (month) Date + Does not Equal ', async () => {
                        const values = ['1']
                        const validation = 'NOT_EQUAL'
                        const dateGroup = 'day_only'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key)
                        expect(results).to.not.include.members(values)
                    })

                    it('Column year Date + Before', async () => {
                        const values = [{ lt: '1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(2)
                        expect(results).to.not.include.members([values[0].lt])
                    })

                    it('Column Quarter, year Date + Before', async () => {
                        const values = [{ lt: 'Q1 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(2)
                        expect(results).to.not.include.members([values[0].lt])
                    })

                    it('Column Month, year Date + Before', async () => {
                        const values = [{ lt: 'Feb 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(7)
                        expect(results).to.not.include.members([values[0].lt])
                    })

                    it('Column Week, year Date + Before', async () => {
                        const values = [{ lt: 'W3 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(26)
                        expect(results).to.not.include.members([values[0].lt])
                    })

                    it('Column year Date + Before or on', async () => {
                        const values = [{ lte: '1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(3)
                        expect(results).to.include.members([values[0].lte])
                    })

                    it('Column Quarter, year Date + Before or on', async () => {
                        const values = [{ lte: 'Q1 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(3)
                        expect(results).to.include.members([values[0].lte])
                    })

                    it('Column Month, year Date + Before or on', async () => {
                        const values = [{ lte: 'Feb 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(8)
                        expect(results).to.include.members([values[0].lte])
                    })

                    it('Column Week, year Date + Before or on', async () => {
                        const values = [{ lte: 'W3 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key);
                        expect(results).to.have.lengthOf(27)
                        expect(results).to.include.members([values[0].lte])
                    })

                    it('Column year Date + After', async () => {
                        const values = [{ gt: '1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(3)
                        expect(results).to.not.include.members([values[0].gt])
                    })

                    it('Column Quarter, year Date + After', async () => {
                        const values = [{ gt: 'Q2 1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(13)
                        expect(results).to.not.include.members([values[0].gt])
                    })

                    it('Column Month, year Date + After', async () => {
                        const values = [{ gt: 'Nov 1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(32)
                        expect(results).to.not.include.members([values[0].gt])
                    })

                    it('Column Week, year Date + After', async () => {
                        const values = [{ gt: 'W50 2001' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(32)
                        expect(results).to.not.include.members([values[0].gt])
                    })

                    it('Column year Date + After or on', async () => {
                        const values = [{ gte: '1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(4)
                        expect(results).to.include.members([values[0].gte])
                    })

                    it('Column Quarter, year Date + After or on', async () => {
                        const values = [{ gte: 'Q2 1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(14)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column Month, year Date + After or on', async () => {
                        const values = [{ gte: 'Nov 1999' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(33)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column Week, year Date + After or on', async () => {
                        const values = [{ gte: 'W50 2001' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(33)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column year Date + Between, inclusive', async () => {
                        const values = [{ gte: '1954', lte: '1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(3)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column Quarter, year Date + Between, inclusive', async () => {
                        const values = [{ gte: 'Q3 1954', lte: 'Q2 1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(8)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column Month, year Date + Between, inclusive', async () => {
                        const values = [{ gte: 'Oct 1954', lte: 'Feb 1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(17)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column Week, year Date + Between, inclusive', async () => {
                        const values = [{ gte: 'W50 1954', lte: 'W3 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(6)
                        expect(results).to.include.members(valuesToCheck(values))
                    })

                    it('Column year Date + Between, exclusive', async () => {
                        const values = [{ gt: '1954', lt: '1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'year'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(1)
                        expect(results).to.not.include.members(valuesToCheck(values))
                    })

                    it('Column Quarter, year Date + Between, exclusive', async () => {
                        const values = [{ gt: 'Q3 1954', lt: 'Q2 1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'quarter'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(6)
                        expect(results).to.not.include.members(valuesToCheck(values))
                    })

                    it('Column Month, year Date + Between, exclusive', async () => {
                        const values = [{ gt: 'Oct 1954', lt: 'Feb 1956' }]
                        const validation = 'RANGE'
                        const dateGroup = 'month'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(15)
                        expect(results).to.not.include.members(valuesToCheck(values))
                    })

                    it('Column Week, year Date + Between, exclusive', async () => {
                        const values = [{ gt: 'W50 1954', lt: 'W3 1955' }]
                        const validation = 'RANGE'
                        const dateGroup = 'week'
                        const column = { type: 'dates', dateGroup }
                        const body = generator.dimension(column).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        const results = data.map(r => r.key + '');
                        expect(results).to.have.lengthOf(4)
                        expect(results).to.not.include.members(valuesToCheck(values))
                    })
                    
                    it('Column Numeric (Agg) + Less than', async () => {
                        const values = [{ "lt": "9" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq(8)
                        expect(data[0].summary[0]).to.be.eq(8)
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[1].summary[0]).to.be.eq(8.5)
                    })

                    it('Column Numeric (Agg) + Less than or Equal to', async () => {
                        const values = [{ "lte": "9" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq(8)
                        expect(data[0].summary[0]).to.be.eq(8)
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[2].key).to.be.eq(9)
                        expect(data[2].key).to.be.eq(9)
                    })

                    it('Column Numeric (Agg) + Greather than', async () => {
                        const values = [{ "gt": "24.6" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(24.8)
                        expect(data[0].summary[0]).to.be.eq(24.8)
                    })

                    it('Column Numeric (Agg) + Greather than or Equal to', async () => {
                        const values = [{ "gte": "24.6" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0].key).to.be.eq(24.6)
                        expect(data[0].summary[0]).to.be.closeTo(24.6, 0.1)
                        expect(data[1].key).to.be.eq(24.8)
                        expect(data[1].summary[0]).to.be.eq(24.8)
                    })

                    it('Column Numeric (Agg) + Between, inclusive', async () => {
                        const values = [{ "gte": "11.3", "lte": "11.5" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(3)
                        expect(data[0].key).to.be.eq(11.3)
                        expect(data[0].summary[0]).to.be.eq(11.3)
                        expect(data[1].key).to.be.eq(11.4)
                        expect(data[1].summary[0]).to.be.eq(11.4)
                        expect(data[2].key).to.be.eq(11.5)
                        expect(data[2].summary[0]).to.be.eq(11.5)
                    })

                    it('Column Numeric (Agg) + Between, exclusive', async () => {
                        const values = [{ "gt": "11.3", "lt": "11.5" }]
                        const validation = 'RANGE'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(11.4)
                        expect(data[0].summary[0]).to.be.eq(11.4)
                    })

                    it('Column Numeric (Agg) + IS_EMPTY', async () => {
                        const values = ['Is Null']
                        const validation = 'IS_EMPTY'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).sort().summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(0)
                    })

                    it('Column Numeric (Agg) + IS_NOT_EMPTY', async () => {
                        const values = ['Is Not Null']
                        const validation = 'IS_NOT_EMPTY'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).sort().summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(96)
                        expect(data[0].key).to.be.eq(8)
                        expect(data[0].summary[0]).to.be.eq(8)
                        expect(data[95].key).to.be.eq(24.8)
                        expect(data[95].summary[0]).to.be.eq(24.8)
                    })

                    it('Column Numeric (Agg) + Equals', async () => {
                        const values = ['23.5']
                        const validation = 'EQUAL'
                        const column = { type: 'numerics' }
                        const body = generator.dimension(column).summary({agg: 'AVG', ...column}).filter().expression({ values, validation, ...column }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(1)
                        expect(data[0].key).to.be.eq(23.5)
                        expect(data[0].summary[0]).to.be.eq(23.5)
                    })
                })

                describe('Multi X/Y charts', () => {

                    it('String Cat and Serie + Numeric Value', async () => {
                        const body = generator.dimension().sort().dimension({ id: 1 }).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0]).to.have.property('items')
                        expect(data[0].items).to.be.an('array').to.have.lengthOf(6)
                        expect(data[0].items[0].key).to.be.eq('Agender')
                        expect(data[0].items[1].key).to.be.eq('Female')
                        expect(data[0].items[1].summary[0]).to.be.closeTo(54962.3, 0.1)
                        expect(data[1].items).to.have.lengthOf(6)
                        expect(data[1].items[2].key).to.be.eq('Genderqueer')
                        expect(data[1].items[3].key).to.be.eq('Male')
                        expect(data[1].items[3].summary[0]).to.be.closeTo(56875, 0.1)
                        expect(data[2].items).to.have.lengthOf(6)
                        expect(data[2].items[4].key).to.be.eq('Pangender')
                        expect(data[2].items[5].key).to.be.eq('Trans')
                        expect(data[2].items[5].summary[0]).to.be.closeTo(54708.4, 0.1)
                    })

                    it('String Cat + 2 Numeric Values', async () => {
                        const body = generator.dimension().summary().summary({ id: 1 }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].summary).to.be.an('array').to.have.length(2)
                        expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(492039.7, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(489360.9, 0.1)
                    })

                    it('String 2 categories', async () => {
                        const body = generator.dimension().dimension({ id: 2, type: 'numerics' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].items[0].key).to.be.eq(3)
                        expect(data[0].items[1].key).to.be.eq(4)
                        expect(data[1].items[3].key).to.be.eq(6)
                        expect(data[1].items[4].key).to.be.eq(8)
                    })

                    it('Small Multiples + Numeric Column', async () => {
                        const small = { type: 'numerics', numberAsRange: true }
                        const column = { type: 'strings', agg: 'COUNT' }
                        const body = generator.dimension(small).sort({ direction: 'ASC' }).dimension(column).summary(column).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('8-10')
                        expect(data[0].items[0].summary[0]).to.be.eq(344)
                        expect(data[0].items[1].summary[0]).to.be.eq(358)
                        expect(data[0].items[2].summary[0]).to.be.eq(345)
                        expect(data[0].items[3].summary[0]).to.be.eq(361)
                        expect(data[1].key).to.be.eq('10-12')
                        expect(data[1].items[0].summary[0]).to.be.eq(1559)
                        expect(data[1].items[1].summary[0]).to.be.eq(1478)
                        expect(data[1].items[2].summary[0]).to.be.eq(1489)
                        expect(data[1].items[3].summary[0]).to.be.eq(1447)
                    })

                    it('Small Multiples + Date Column', async () => {
                        const small = { type: 'dates', dateGroup: 'month' }
                        const column = { type: 'strings', agg: 'COUNT' }
                        const body = generator.dimension(small).dimension(column).summary(column).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0].summary[0]).to.be.eq(10)
                        expect(data[0].items[1].summary[0]).to.be.eq(15)
                        expect(data[0].items[2].summary[0]).to.be.eq(8)
                        expect(data[0].items[3].summary[0]).to.be.eq(15)
                        expect(data[1].key).to.be.eq('Aug 1954')
                        expect(data[1].items[0].summary[0]).to.be.eq(58)
                        expect(data[1].items[1].summary[0]).to.be.eq(40)
                        expect(data[1].items[2].summary[0]).to.be.eq(55)
                        expect(data[1].items[3].summary[0]).to.be.eq(38)
                    })

                    it('Small Multiples + Text Column', async () => {
                        const small = { type: 'strings', id: 1 }
                        const column = { type: 'strings', agg: 'COUNT' }
                        const body = generator.dimension(small).dimension(column).summary(column).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[0].items[0].summary[0]).to.be.eq(4208)
                        expect(data[0].items[1].summary[0]).to.be.eq(4158)
                        expect(data[0].items[2].summary[0]).to.be.eq(3989)
                        expect(data[0].items[3].summary[0]).to.be.eq(4197)
                        expect(data[1].key).to.be.eq('Female')
                        expect(data[1].items[0].summary[0]).to.be.eq(4172)
                        expect(data[1].items[1].summary[0]).to.be.eq(4057)
                        expect(data[1].items[2].summary[0]).to.be.eq(4108)
                        expect(data[1].items[3].summary[0]).to.be.eq(4193)
                    })
                })

                describe('Groups table', () => {

                    it('3 Groups + 2 Columns', async () => {
                        const body = generator.dimension().dimension({ id: 1 }).dimension({ id: 2 }).summary().summary({ id: 1 }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.has.property('items').to.be.an('array')
                        expect(data[0].items[0]).to.has.property('key')
                        expect(data[0].items[0]).to.has.property('items')
                        expect(data[0].items[0].items[0]).to.has.property('items').to.be.null
                        expect(data[0].items[0].items[0]).to.has.property('summary').to.have.length(2)

                        expect(data[0].items[1]).to.has.property('key')
                        expect(data[0].items[1]).to.has.property('items')
                        expect(data[0].items[1].items[0]).to.has.property('items').to.be.null
                        expect(data[0].items[1].items[0]).to.has.property('summary').to.have.length(2)

                        expect(data[1].items[0]).to.has.property('key')
                        expect(data[1].items[0]).to.has.property('items')
                        expect(data[1].items[0].items[0]).to.has.property('items').to.be.null
                        expect(data[1].items[0].items[0]).to.has.property('summary').to.have.length(2)

                        expect(data[1].items[0]).to.has.property('key')
                        expect(data[1].items[0]).to.has.property('items')
                        expect(data[1].items[0].items[1]).to.has.property('items').to.be.null
                        expect(data[1].items[0].items[1]).to.has.property('summary').to.have.length(2)
                    })

                    it('2G+1C: Totals and Sub Totals', async () => {
                        const body = generator.dimension().dimension({ id: 1 }).summary().addTotals().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem[0].summary).to.has.length(1)
                        expect(chartItem[0].summary[0]).to.be.closeTo(1316340.6, 0.1)
                        const data = chartItem[0].data
                        expect(data[0].summary).to.has.length(1)
                        expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(328468.7, 0.1)
                    })

                    it('1G+1C: Trend by Numeric column', async () => {
                        const body = generator.dimension().summary({ agg: 'AVG' }).inner({ id: 1 }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0]).to.has.property('innerCharts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('charts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('summaryIndex').to.be.an('number').to.have.eq(0)
                        const inner = data[0].innerCharts[0].charts[0]
                        expect(inner).to.has.property('data').to.be.an('array')
                        expect(inner).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(inner.data[0]).to.has.property('key').to.be.eq(9)
                        expect(inner.data[0]).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(inner.data[0].summary[0]).to.be.eq(18.5)
                        expect(inner.data[1].key).to.be.eq(10)
                        expect(inner.data[1].summary[0]).to.be.closeTo(14.53, 0.01)
                    })

                    it('1G+1C: Trend by Date column (Year) Period: All time', async () => {
                        const body = generator.dimension().summary({ agg: 'AVG' }).inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0]).to.has.property('innerCharts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('charts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('summaryIndex').to.be.an('number').to.have.eq(0)
                        const inner = data[0].innerCharts[0].charts[0]
                        expect(inner).to.has.property('data').to.be.an('array')
                        expect(inner).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(inner.data[0]).to.has.property('key').to.be.eq('1954')
                        expect(inner.data[0]).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(inner.data[0].summary[0]).to.be.closeTo(15.63, 0.01)
                        expect(inner.data[1].key).to.be.eq('1955')
                        expect(inner.data[1].summary[0]).to.be.closeTo(15.37, 0.01)
                    })
                    
                    it('1G+1C: Trend by Formula column', async () => {
                        const params = {
                            formula: `[${generator.defaults['numerics'][0].id}] / 2`,
                            formulaMode: "STANDARD",
                            formulaType: "number",
                            name: 'Formula test Module'
                        }
                        const formula = await formulaService.createFormula(config, params)
                        const formulaId = formula.formulaId
                        const body = generator.dimension().summary({ agg: 'AVG' }).inner({ customId: formulaId }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        await formulaService.deleteFormula({ ...config, formulaId })
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.has.property('innerCharts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('charts').to.be.an('array').to.have.lengthOf(1)
                        expect(data[0].innerCharts[0]).to.has.property('summaryIndex').to.be.an('number').to.have.eq(0)
                        const inner = data[0].innerCharts[0].charts[0]
                        expect(inner).to.has.property('data').to.be.an('array').to.have.lengthOf(96)
                        expect(inner).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(Number(inner.data[0].key)).to.be.eq(4)
                        expect(inner.data[0]).to.has.property('summary').to.be.an('array').to.have.lengthOf(1)
                        expect(inner.data[0].summary[0]).to.be.eq(8)
                        expect(Number(inner.data[1].key)).to.be.eq(4.25)
                        expect(inner.data[1].summary[0]).to.be.closeTo(8.5, 0.1)
                    })
                })

                describe('Group table chart requests', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })

                    it('1G+1C (SUM) + Sort', async () => {
                        const body = generator.dimension().sort().summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(328468.7, 0.1)
                    })

                    it('1G+1C (AVG) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'AVG' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.closeTo(15.51, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(15.53, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(15.50, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(15.53, 0.1)
                    })

                    it('1G+1C (MED) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'MEDIAN' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.closeTo(15.51, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(15.53, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(15.50, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(15.53, 0.1)
                    })

                    it('1G+1C (COUNT) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'COUNT' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.eq(21415)
                        expect(data[1].summary[0]).to.be.eq(21194)
                        expect(data[2].summary[0]).to.be.eq(21048)
                        expect(data[3].summary[0]).to.be.eq(21150)
                    })

                    it('1G+1C (DCOUNT) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'DISTINCTCOUNT' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.eq(96)
                        expect(data[1].summary[0]).to.be.eq(96)
                        expect(data[2].summary[0]).to.be.eq(96)
                        expect(data[3].summary[0]).to.be.eq(96)
                    })

                    it('1G+1C (MIN) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'MIN' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.eq(8)
                        expect(data[1].summary[0]).to.be.eq(8)
                        expect(data[2].summary[0]).to.be.eq(8)
                        expect(data[3].summary[0]).to.be.eq(8)
                    })

                    it('1G+1C (MAX) + Sort', async () => {
                        const body = generator.dimension().sort().summary({ agg: 'MAX' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[2].key).to.be.eq('B')
                        expect(data[3].key).to.be.eq('O')
                        expect(data[0].summary[0]).to.be.closeTo(24.8, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(24.8, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(24.8, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(24.8, 0.1)
                    })

                    it('Max Groups', async () => {
                        const body = generator.dimension().size(2).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[1].key).to.be.eq('AB')
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month, Year) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(577)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jul 1954')
                        expect(firstData.summary[0]).to.be.closeTo(48, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(586.9, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(586.9, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Aug 1954')
                        expect(secondData.summary[0]).to.be.closeTo(191, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(2488.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2488.8, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month, Year) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(577)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jul 1954')
                        expect(firstData.summary[0]).to.be.closeTo(48, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(586.9, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(586.9, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Aug 1954')
                        expect(secondData.summary[0]).to.be.closeTo(191, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(2488.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2488.8, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month, Year) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(577)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jul 1954')
                        expect(firstData.summary[0]).to.be.closeTo(48, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(586.9, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(586.9, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Aug 1954')
                        expect(secondData.summary[0]).to.be.closeTo(191, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(2488.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Aug 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2488.8, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month, Year) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(577)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jul 1954')
                        expect(firstData.summary[0]).to.be.closeTo(48, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(586.9, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W29 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Aug 1954')
                        expect(secondData.summary[0]).to.be.closeTo(191, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(2488.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W31 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(623, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month, Year) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(577)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jul 1954')
                        expect(firstData.summary[0]).to.be.closeTo(48, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(586.9, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('07/24/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Aug 1954')
                        expect(secondData.summary[0]).to.be.closeTo(191, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(2488.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('08/01/1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(148.6, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Year) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'YEAR' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('1954')
                        expect(firstData.summary[0]).to.be.closeTo(923, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(12162.2, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(12162.2, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('1955')
                        expect(secondData.summary[0]).to.be.closeTo(2063, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(26916.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(26916.8, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Year) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'YEAR' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('1954')
                        expect(firstData.summary[0]).to.be.closeTo(923, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(12162.2, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(5582.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('1955')
                        expect(secondData.summary[0]).to.be.closeTo(2063, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(26916.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q1 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(6307.1, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Year) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'YEAR' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('1954')
                        expect(firstData.summary[0]).to.be.closeTo(923, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(12162.2, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(586.9, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('1955')
                        expect(secondData.summary[0]).to.be.closeTo(2063, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(26916.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Jan 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(1992.2, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Year) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'YEAR' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('1954')
                        expect(firstData.summary[0]).to.be.closeTo(923, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(12162.2, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W29 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('1955')
                        expect(secondData.summary[0]).to.be.closeTo(2063, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(26916.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W52 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(51.7, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Year) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'YEAR' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('1954')
                        expect(firstData.summary[0]).to.be.closeTo(923, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(12162.2, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('07/24/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('1955')
                        expect(secondData.summary[0]).to.be.closeTo(2063, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(26916.8, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('01/01/1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(51.7, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter, Year) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q3 1954')
                        expect(firstData.summary[0]).to.be.closeTo(432, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(5582.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(5582.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q4 1954')
                        expect(secondData.summary[0]).to.be.closeTo(491, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(6580.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(6580.1, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter, Year) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q3 1954')
                        expect(firstData.summary[0]).to.be.closeTo(432, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(5582.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(5582.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q4 1954')
                        expect(secondData.summary[0]).to.be.closeTo(491, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(6580.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q4 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(6580.1, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter, Year) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q3 1954')
                        expect(firstData.summary[0]).to.be.closeTo(432, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(5582.1, 0.1)


                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(586.9, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q4 1954')
                        expect(secondData.summary[0]).to.be.closeTo(491, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(6580.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Oct 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2366.9, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter, Year) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q3 1954')
                        expect(firstData.summary[0]).to.be.closeTo(432, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(5582.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W29 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q4 1954')
                        expect(secondData.summary[0]).to.be.closeTo(491, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(6580.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W39 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(218.5, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter, Year) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q3 1954')
                        expect(firstData.summary[0]).to.be.closeTo(432, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(5582.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('07/24/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q4 1954')
                        expect(secondData.summary[0]).to.be.closeTo(491, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(6580.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('10/01/1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(91.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Week, Year) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'WEEK' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2506)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('W29 1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('W30 1954')
                        expect(secondData.summary[0]).to.be.closeTo(46, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(553.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(553.4, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Week, Year) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'WEEK' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2506)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('W29 1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('W30 1954')
                        expect(secondData.summary[0]).to.be.closeTo(46, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(553.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(553.4, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Week, Year) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'WEEK' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2506)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('W29 1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)


                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('W30 1954')
                        expect(secondData.summary[0]).to.be.closeTo(46, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(553.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(553.4, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Week, Year) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'WEEK' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2506)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('W29 1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W29 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('W30 1954')
                        expect(secondData.summary[0]).to.be.closeTo(46, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(553.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W30 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(553.4, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Week, Year) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'WEEK' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(2506)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('W29 1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('07/24/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('W30 1954')
                        expect(secondData.summary[0]).to.be.closeTo(46, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(553.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('07/25/1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Full Date) + Trend by Date (Year)', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY' }).filter().expression({ values, ...column }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(158)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('07/24/1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('07/25/1954')
                        expect(secondData.summary[0]).to.be.closeTo(5, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(67.3, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Full Date) + Trend by Date (Quarter)', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY' }).filter().expression({ values, ...column }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(158)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('07/24/1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('07/25/1954')
                        expect(secondData.summary[0]).to.be.closeTo(5, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(67.3, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Full Date) + Trend by Date (Month)', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY' }).filter().expression({ values, ...column }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(158)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('07/24/1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)

                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('07/25/1954')
                        expect(secondData.summary[0]).to.be.closeTo(5, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(67.3, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Jul 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Full Date) + Trend by Date (Week, Year)', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY' }).filter().expression({ values, ...column }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(158)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('07/24/1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W29 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('07/25/1954')
                        expect(secondData.summary[0]).to.be.closeTo(5, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(67.3, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W30 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Full Date) + Trend by Date (Day (Month))', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY' }).filter().expression({ values, ...column }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(158)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('07/24/1954')
                        expect(firstData.summary[0]).to.be.closeTo(2, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(33.5, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('07/24/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(33.5, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('07/25/1954')
                        expect(secondData.summary[0]).to.be.closeTo(5, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(67.3, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('07/25/1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(67.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q1')
                        expect(firstData.summary[0]).to.be.closeTo(24721, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(326094.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(6307.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q2')
                        expect(secondData.summary[0]).to.be.closeTo(24629, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(324155, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(6346.7, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q1')
                        expect(firstData.summary[0]).to.be.closeTo(24721, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(326094.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q1 1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(6307.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q2')
                        expect(secondData.summary[0]).to.be.closeTo(24629, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(324155, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q2 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(6346.7, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q1')
                        expect(firstData.summary[0]).to.be.closeTo(24721, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(326094.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jan 1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(1992.2, 0.1)

                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q2')
                        expect(secondData.summary[0]).to.be.closeTo(24629, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(324155, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Apr 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2122.3, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q1')
                        expect(firstData.summary[0]).to.be.closeTo(24721, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(326094.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W52 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(51.7, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q2')
                        expect(secondData.summary[0]).to.be.closeTo(24629, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(324155, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W13 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(172.1, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Quarter) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'QUARTER_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Q1')
                        expect(firstData.summary[0]).to.be.closeTo(24721, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(326094.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('01/01/1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(51.7, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Q2')
                        expect(secondData.summary[0]).to.be.closeTo(24629, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(324155, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('04/01/1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(28.1, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jan')
                        expect(firstData.summary[0]).to.be.closeTo(8548, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(113213.8, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(1992.2, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Feb')
                        expect(secondData.summary[0]).to.be.closeTo(7720, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(102264.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2288.9, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jan')
                        expect(firstData.summary[0]).to.be.closeTo(8548, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(113213.8, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q1 1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(1992.2, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Feb')
                        expect(secondData.summary[0]).to.be.closeTo(7720, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(102264.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q1 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2288.9, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jan')
                        expect(firstData.summary[0]).to.be.closeTo(8548, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(113213.8, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Jan 1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(1992.2, 0.1)

                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Feb')
                        expect(secondData.summary[0]).to.be.closeTo(7720, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(102264.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Feb 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(2288.9, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jan')
                        expect(firstData.summary[0]).to.be.closeTo(8548, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(113213.8, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W52 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(51.7, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Feb')
                        expect(secondData.summary[0]).to.be.closeTo(7720, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(102264.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W5 1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(361, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Month) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'MONTH_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('string')
                        expect(firstData.key).to.be.eq('Jan')
                        expect(firstData.summary[0]).to.be.closeTo(8548, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(113213.8, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('01/01/1955')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(51.7, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('string')
                        expect(secondData.key).to.be.eq('Feb')
                        expect(secondData.summary[0]).to.be.closeTo(7720, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(102264.1, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('02/01/1955')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(28, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Day (Month)) + Trend by Date (Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'YEAR' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('number')
                        expect(firstData.key).to.be.oneOf(['1', 1])
                        expect(firstData.summary[0]).to.be.closeTo(3266, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(43167.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(475.6, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('number')
                        expect(secondData.key).to.be.oneOf(['2', 2])
                        expect(secondData.summary[0]).to.be.closeTo(3207, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(42429.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(385.6, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Day (Month)) + Trend by Date (Quarter)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'QUARTER' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('number')
                        expect(firstData.key).to.be.oneOf(['1', 1])
                        expect(firstData.summary[0]).to.be.closeTo(3266, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(43167.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(244.1, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('number')
                        expect(secondData.key).to.be.oneOf(['2', 2])
                        expect(secondData.summary[0]).to.be.closeTo(3207, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(42429.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Q3 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(167.9, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Day (Month)) + Trend by Date (Month)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'MONTH' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('number')
                        expect(firstData.key).to.be.oneOf(['1', 1])
                        expect(firstData.summary[0]).to.be.closeTo(3266, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(43167.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('Aug 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(148.6, 0.1)

                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('number')
                        expect(secondData.key).to.be.oneOf(['2', 2])
                        expect(secondData.summary[0]).to.be.closeTo(3207, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(42429.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('Aug 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(118, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Day (Month)) + Trend by Date (Week, Year)', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'WEEK' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('number')
                        expect(firstData.key).to.be.oneOf(['1', 1])
                        expect(firstData.summary[0]).to.be.closeTo(3266, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(43167.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('W31 1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(148.6, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('number')
                        expect(secondData.key).to.be.oneOf(['2', 2])
                        expect(secondData.summary[0]).to.be.closeTo(3207, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(42429.4, 0.1)


                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('W31 1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(118, 0.1)
                    })

                    it('1 Group Date + 1 C Numeric + 1 C String (Date Group: Day (Month)) + Trend by Date (Day (Month))', async () => {
                        const body = generator.dimension({ type: 'dates', dateGroup: 'DAY_ONLY' }).sort().summary({ type: 'strings', agg: 'COUNT' }).summary().inner({ type: 'dates', dateGroup: 'DAY' }).toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)

                        const firstData = data[0];
                        expect(firstData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(firstData).to.have.property('key')
                        expect(firstData).to.have.property('innerCharts')
                        //expect(firstData.key).to.be.an('number')
                        expect(firstData.key).to.be.oneOf(['1', 1])
                        expect(firstData.summary[0]).to.be.closeTo(3266, 0.1)
                        expect(firstData.summary[1]).to.be.closeTo(43167.1, 0.1)

                        const dataInnerChartsOfFirstData = firstData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfFirstData).to.have.property('key')
                        expect(dataInnerChartsOfFirstData).to.have.property('summary')
                        //expect(dataInnerChartsOfFirstData.key).to.be.an('string')
                        expect(dataInnerChartsOfFirstData.key).to.be.eq('08/01/1954')
                        expect(dataInnerChartsOfFirstData.summary[0]).to.be.closeTo(148.6, 0.1)


                        const secondData = data[1];
                        expect(secondData.innerCharts).to.be.an('array').to.have.lengthOf(1)
                        expect(secondData).to.have.property('key')
                        expect(secondData).to.have.property('innerCharts')
                        //expect(secondData.key).to.be.an('number')
                        expect(secondData.key).to.be.oneOf(['2', 2])
                        expect(secondData.summary[0]).to.be.closeTo(3207, 0.1)
                        expect(secondData.summary[1]).to.be.closeTo(42429.4, 0.1)

                        const dataInnerChartsOfSecondData = secondData.innerCharts[0].charts[0].data[0]
                        expect(dataInnerChartsOfSecondData).to.have.property('key')
                        expect(dataInnerChartsOfSecondData).to.have.property('summary')
                        //expect(dataInnerChartsOfSecondData.key).to.be.an('string')
                        expect(dataInnerChartsOfSecondData.key).to.be.eq('08/02/1954')
                        expect(dataInnerChartsOfSecondData.summary[0]).to.be.closeTo(118, 0.1)
                    })
                })

                describe('Crosstab table', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })

                    it("2 Text Rows + 2 Text Columns + 2 Values (SUM, AVG)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator.dimension().size(4).dimension({ id: 1 }).size(4).summary({ agg: "SUM" }).summary({ agg: "AVG" }).toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('A')

                        expect(data[0]).to.has.property("items").to.be.an("array");
                        expect(data[0].items[0]).to.has.property("key");
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0].key).to.be.eq('Agender')
                        expect(data[0].items[1].key).to.be.eq('Female')
                        expect(data[0].items[2].key).to.be.eq('Genderqueer')
                        expect(data[0].items[3].key).to.be.eq('Male')

                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0].summary[0]).to.be.closeTo(55401.7, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(15.509994401, 0.1)
                        expect(data[0].items[1].summary[0]).to.be.closeTo(54962.3, 0.1)
                        expect(data[0].items[1].summary[1]).to.be.closeTo(15.556835551, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('AB')

                        expect(data[1]).to.has.property("items").to.be.an("array");
                        expect(data[1].items[0]).to.has.property("key");
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);

                        expect(data[1].items[0].key).to.be.eq('Agender')
                        expect(data[1].items[1].key).to.be.eq('Female')
                        expect(data[1].items[2].key).to.be.eq('Genderqueer')
                        expect(data[1].items[3].key).to.be.eq('Male')

                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0].summary[0]).to.be.closeTo(55195.2, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(15.499915754, 0.1)
                        expect(data[1].items[1].summary[0]).to.be.closeTo(53533.5, 0.1)
                        expect(data[1].items[1].summary[1]).to.be.closeTo(15.625656743, 0.1)
                    });

                    it("2 Numeric Rows + 2 Numeric Columns + 3 Values (MEDIAN, MIN, MAX))", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator.dimension({ type: "numerics" }).size(4).dimension({ id: 1, type: "numerics" }).size(4).summary({ type: "numerics", agg: "MEDIAN" }).summary({ type: "numerics", agg: "MIN" }).summary({ type: "numerics", agg: "MAX" }).toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(8)

                        expect(data[0]).to.has.property("items").to.be.an("array");
                        expect(data[0].items[0]).to.has.property("key");
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(3);
                        expect(data[0].items[0]).to.has.property('items').to.be.null
                        expect(data[0].items[0].key).to.be.eq(14)

                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(3);
                        expect(data[0].items[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(8, 0.1)
                        expect(data[0].items[0].summary[2]).to.be.closeTo(8, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(8.5)

                        expect(data[1]).to.has.property("items").to.be.an("array");
                        expect(data[1].items[0]).to.has.property("key");
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(3);
                        expect(data[1].items[0]).to.has.property('items').to.be.null
                        expect(data[1].items[0].key).to.be.eq(14)

                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(3);
                        expect(data[1].items[0].summary[0]).to.be.closeTo(8.5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(8.5, 0.1)
                        expect(data[1].items[0].summary[2]).to.be.closeTo(8.5, 0.1)
                    });

                    it("Max Rows", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator.dimension({ type: "numerics" }).size(10).summary({ type: "numerics", agg: "SUM" }).addTotals().toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(10);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(8)

                        expect(data[0]).to.has.property('items').to.be.null
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[0].summary[0]).to.be.closeTo(3392, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(8.5)

                        expect(data[1]).to.has.property('items').to.be.null
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[1].summary[0]).to.be.closeTo(3425.5, 0.1)
                    });

                    it("Sort Columns", async () => {
                        const body = generator
                            .dimension({ type: 'strings', id: 1 })
                            .size(50)
                            .dimension({ type: 'strings', id: 0 })
                            .sort({direction: 'DESC'})
                            .size(50)
                            .summary({ type: 'numerics', agg: 'SUM' })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(6);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Agender')
                        expect(data[0]).to.has.property('items').to.be.an('array').to.has.length(4);
                        expect(data[0].items[0].key).to.be.eq('O')
                        expect(data[0].items[0].summary[0]).to.be.closeTo(55039.9, 0.1)
                        expect(data[0]).to.has.property('summary').to.be.an('array').to.has.length(1);
                        expect(data[0].summary[0]).to.be.closeTo(218621.3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Female')
                        expect(data[1]).to.has.property('items').to.be.an('array').to.has.length(4);
                        expect(data[1].items[0].key).to.be.eq('O')
                        expect(data[1].items[0].summary[0]).to.be.closeTo(55007.9, 0.1)
                        expect(data[1]).to.has.property('summary').to.be.an('array').to.has.length(1);
                        expect(data[1].summary[0]).to.be.closeTo(217021.3, 0.1)
                    });

                    it("Sort Rows", async () => {
                        const body = generator
                            .dimension({ type: 'strings', id: 1 })
                            .sort({direction: 'DESC'})
                            .size(50)
                            .dimension({ type: 'strings', id: 0 })
                            .size(50)
                            .summary({ type: 'numerics', agg: 'SUM' })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(6);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Trans')
                        expect(data[0]).to.has.property('items').to.be.an('array').to.has.length(4);
                        expect(data[0].items[0].key).to.be.eq('A')
                        expect(data[0].items[0].summary[0]).to.be.closeTo(55460, 0.1)
                        expect(data[0]).to.has.property('summary').to.be.an('array').to.has.length(1);
                        expect(data[0].summary[0]).to.be.closeTo(219730.3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Pangender')
                        expect(data[1]).to.has.property('items').to.be.an('array').to.has.length(4);
                        expect(data[1].items[0].key).to.be.eq('A')
                        expect(data[1].items[0].summary[0]).to.be.closeTo(54454.5, 0.1)
                        expect(data[1]).to.has.property('summary').to.be.an('array').to.has.length(1);
                        expect(data[1].summary[0]).to.be.closeTo(216650.6, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(2);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(4);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q1 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(404, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(82, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(6);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(12);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jan 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(54, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(24);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W29 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(53);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W52 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(3, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(158);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(364);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('01/01/1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(3, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(2);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(4);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q1')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(404, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(82, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(6);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(12);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jan')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(54, 0.1)
                    });

                    it("1 Date Row (Year) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(49);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(788, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(93, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(1)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(31, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(21, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('1955')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(1737, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(95, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(1)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(41, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(32, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(69);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(89);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('10/01/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(6, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);

                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(88, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q4 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(88, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Oct 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(154, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(69, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(11);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W29 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(14);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W39 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(15, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(12, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(69);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(89);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('10/01/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(6, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q4')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(88, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Oct')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(154, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(69, 0.1)
                    });

                    it("1 Date Row (Qarter, Year) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(193);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q3 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(362, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(80, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(1)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(16, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(15, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q4 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(426, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(1)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(15, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(12, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(61, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(61, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Aug 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(61, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(2);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W29 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(5);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W31 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(42, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(32, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(8);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('08/01/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(10, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(10, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(61, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Aug')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(61, 0.1)
                    });

                    it("1 Date Row (Month, Year) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jul 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(8);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(39, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(28, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(24)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Aug 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(162, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(61, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(1)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(10, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(10, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jul 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W29 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W30 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(7);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('07/25/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jul')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(27, 0.1)
                    });

                    it("1 Date Row (Week, Year) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('W29 1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(24)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('W30 1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(7);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(37, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(27, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(25)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jul 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W29 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W30 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('07/24/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('07/25/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jul')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jul')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Full date) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(500);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('07/24/1954')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(24)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(2, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('07/25/1954')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(5, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(25)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(5, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(404, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(82, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(408, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(83, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q1 1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(404, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(82, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q2 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(408, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(83, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jan 1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(54, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Apr 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(136, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(54, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W52 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W13 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(11, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(10, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('01/01/1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('04/01/1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(2, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q1')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q2')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(96, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jan')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(3);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Apr')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(6827, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(96, 0.1)
                    });

                    it("1 Date Row (Quarter) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(10)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(10)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(4);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Q1')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(21010, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(1)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(681, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Q2')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(10);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(20864, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(1)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(670, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(92, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(54, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(145, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(60, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q1 1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(54, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q1 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(145, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(60, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jan 1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(127, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(54, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(48);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Feb 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(145, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(60, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(254);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W52 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(235);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W5 1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(24, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(20, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('01/01/1955')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(3, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(3, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('02/01/1955')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(2, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(2, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q1')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q1')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(96, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jan')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Feb')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(96, 0.1)
                    });

                    it("1 Date Row (Month) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(12);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq('Jan')

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(31);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(7275, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(1)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(219, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(69, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq('Feb')

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(29);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(6591, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(1)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(224, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(73, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "YEAR" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(49);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(31, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(21, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(49);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(25, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(21, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Qarter, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(193);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q3 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(16, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(15, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(193);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q3 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(11, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(11, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Month, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Aug 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(10, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(10, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Aug 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(8, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Week, Year) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "WEEK" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('W31 1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(10, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(10, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('W31 1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(8, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Full date) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('08/01/1954')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(10, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(10, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(500);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('08/02/1954')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(8, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Quarter) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "QUARTER_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(4);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Q1')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(681, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(88, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(4);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Q1')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(670, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(90, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Month) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "MONTH_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(12);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq('Jan')
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(219, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(69, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(12);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq('Jan')
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(218, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(68, 0.1)
                    });

                    it("1 Date Row (Day (Month)) + 1 Date Column (Day (Month)) + 2 Values (COUNT, DCOUNT)", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .dimension({ type: "dates", dateGroup: "DAY_ONLY" })
                            .size(500)
                            .summary({ type: "numerics", agg: "COUNT" })
                            .addTotals()
                            .summary({ type: "numerics", agg: "DISTINCTCOUNT" })
                            .addTotals()
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(31);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(1)

                        expect(data[0]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of first element of data array
                        expect(data[0].items[0].key).to.be.eq(1)
                        expect(data[0].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[0].items[0]).to.has.property("items").to.be.null;
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2780, 0.1)
                        expect(data[0].items[0].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(2)

                        expect(data[1]).to.has.property("items").to.be.an("array").to.has.length(1);
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(96, 0.1)

                        // Testing first element of items array of second element of data array
                        expect(data[1].items[0].key).to.be.eq(2)
                        expect(data[1].items[0]).to.has.property("summary").to.be.an("array").to.has.length(2);
                        expect(data[1].items[0]).to.has.property("items").to.be.null;
                        expect(data[1].items[0].summary[0]).to.be.closeTo(2738, 0.1)
                        expect(data[1].items[0].summary[1]).to.be.closeTo(96, 0.1)
                    });

                    it("Totals and Subtotals", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "numerics" })
                            .size(10)
                            .summary({ type: "numerics", agg: "SUM" })
                            .addTotals().toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(10);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(8)
                        expect(data[0]).to.has.property('items').to.be.null
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[0].summary[0]).to.be.closeTo(3392, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[1]).to.has.property('items').to.be.null
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[1].summary[0]).to.be.closeTo(3425.5, 0.1)
                    });

                    it("Max Rows + Max Columns", async () => {
                        // Prepare configuration for request to chart service
                        const body = generator
                            .dimension({ type: "numerics" })
                            .size(10)
                            .summary({ type: "numerics", agg: "SUM" })
                            .size(10)
                            .toBody();
                        const chartItem = await resultsService.chartResult(config, body);
                        const data = chartItem[0].data;

                        expect(data).to.have.lengthOf(10);
                        // Testing first element of data array
                        expect(data[0].key).to.be.eq(8)
                        expect(data[0]).to.has.property('items').to.be.null
                        expect(data[0]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[0].summary[0]).to.be.closeTo(3392, 0.1)

                        // Testing second element of data array
                        expect(data[1].key).to.be.eq(8.5)
                        expect(data[1]).to.has.property('items').to.be.null
                        expect(data[1]).to.has.property("summary").to.be.an("array").to.has.length(1);
                        expect(data[1].summary[0]).to.be.closeTo(3425.5, 0.1)
                    });
                })

                describe('Funnel', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })
                    
                    it('Date + Value', async () => {
                        const body = generator
                            .dimension({ type: 'dates', dateGroup: 'year' })
                            .sort({direction: 'ASC'})
                            .summary({agg: 'SUM'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('1954')
                        expect(data[0].summary[0]).to.be.closeTo(12162.2, 0.1)
                        expect(data[1].key).to.be.eq('1955')
                        expect(data[1].summary[0]).to.be.closeTo(26916.8, 0.1)
                        expect(data[2].key).to.be.eq('1956')
                        expect(data[2].summary[0]).to.be.closeTo(27540.9, 0.1)
                    })

                    it('Num + Value', async () => {
                        const body = generator
                            .dimension({ type: 'numerics' })
                            .sort({direction: 'ASC'})
                            .summary({agg: 'SUM'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(96)
                        expect(data[0]).to.have.property('key')
                        expect(Number(data[0].key)).to.be.eq(8)
                        expect(data[0].summary[0]).to.be.closeTo(3392, 0.1)
                        expect(Number(data[1].key)).to.be.eq(8.5)
                        expect(data[1].summary[0]).to.be.closeTo(3425.5, 0.1)
                        expect(Number(data[2].key)).to.be.eq(9)
                        expect(data[2].summary[0]).to.be.closeTo(1710, 0.1)
                    })

                    it('Text + Value', async () => {
                        const body = generator
                            .dimension()
                            .sort({direction: 'ASC'})
                            .summary({agg: 'SUM'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[0].summary[0]).to.be.closeTo(332277.5, 0.1)
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[1].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[2].key).to.be.eq('B')
                        expect(data[2].summary[0]).to.be.closeTo(326355.2, 0.1)
                    })
                })

                describe('Min/Max', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })
                    
                    it('Date + Value', async () => {
                        const body = generator
                            .dimension({ type: 'dates', dateGroup: 'year' })
                            .sort({direction: 'ASC'})
                            .summary({agg: 'MIN'})
                            .summary({agg: 'MAX'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('1954')
                        expect(data[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(24.8, 0.1)
                        expect(data[1].key).to.be.eq('1955')
                        expect(data[1].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(24.8, 0.1)
                        expect(data[2].key).to.be.eq('1956')
                        expect(data[2].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[2].summary[1]).to.be.closeTo(24.8, 0.1)
                    })

                    it('Num + Value', async () => {
                        const body = generator
                            .dimension({ type: 'numerics' })
                            .sort({direction: 'ASC'})
                            .summary({agg: 'MIN'})
                            .summary({agg: 'MAX'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(96)
                        expect(data[0]).to.have.property('key')
                        expect(Number(data[0].key)).to.be.eq(8)
                        expect(data[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(8, 0.1)
                        expect(Number(data[1].key)).to.be.eq(8.5)
                        expect(data[1].summary[0]).to.be.closeTo(8.5, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(8.5, 0.1)
                        expect(Number(data[2].key)).to.be.eq(9)
                        expect(data[2].summary[0]).to.be.closeTo(9, 0.1)
                        expect(data[2].summary[1]).to.be.closeTo(9, 0.1)
                    })

                    it('Text + Value', async () => {
                        const body = generator
                            .dimension()
                            .sort({direction: 'ASC'})
                            .summary({agg: 'MIN'})
                            .summary({agg: 'MAX'})
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('A')
                        expect(data[0].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[0].summary[1]).to.be.closeTo(24.8, 0.1)
                        expect(data[1].key).to.be.eq('AB')
                        expect(data[1].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[1].summary[1]).to.be.closeTo(24.8, 0.1)
                        expect(data[2].key).to.be.eq('B')
                        expect(data[2].summary[0]).to.be.closeTo(8, 0.1)
                        expect(data[2].summary[1]).to.be.closeTo(24.8, 0.1)
                    })
                })

                describe('Heatmap', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })

                    it('Date + Value + Date', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator
                            .dimension({ type: 'dates', dateGroup: 'month' }).filter().expression({values, ...column})
                            .sort({direction: 'DESC'})
                            .dimension({ type: 'dates', dateGroup: 'month' }).filter().expression({values, ...column})
                            .summary()
                            .toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(6)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('Dec 1954')
                        expect(data[0].items).to.have.lengthOf(1)
                        expect(data[0].items[0].key).to.be.eq('Dec 1954')
                        expect(data[0].items[0].summary[0]).to.be.closeTo(2248.5, 0.1)
                        expect(data[1].key).to.be.eq('Nov 1954')
                        expect(data[1].items).to.have.lengthOf(1)
                        expect(data[1].items[0].key).to.be.eq('Nov 1954')
                        expect(data[1].items[0].summary[0]).to.be.closeTo(1964.7, 0.1)
                        expect(data[2].key).to.be.eq('Oct 1954')
                        expect(data[2].items).to.have.lengthOf(1)
                        expect(data[2].items[0].key).to.be.eq('Oct 1954')
                        expect(data[2].items[0].summary[0]).to.be.closeTo(2366.9, 0.1)
                        expect(data[3].key).to.be.eq('Sep 1954')
                        expect(data[3].items).to.have.lengthOf(1)
                        expect(data[3].items[0].key).to.be.eq('Sep 1954')
                        expect(data[3].items[0].summary[0]).to.be.closeTo(2506.4, 0.1)
                    })

                    it('Text + Value + Text', async () => {
                        const body = generator.dimension().sort({direction: 'DESC'}).dimension({ type: 'numerics' }).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('O')
                        expect(data[0].items).to.have.lengthOf(96)
                        expect(Number(data[0].items[0].key)).to.be.eq(8)
                        expect(data[0].items[0].summary[0]).to.be.eq(840)
                        expect(data[1].key).to.be.eq('B')
                        expect(data[1].items).to.have.lengthOf(96)
                        expect(Number(data[1].items[0].key)).to.be.eq(8)
                        expect(data[1].items[0].summary[0]).to.be.eq(864)
                        expect(data[2].key).to.be.eq('AB')
                        expect(data[2].items).to.have.lengthOf(96)
                        expect(Number(data[2].items[0].key)).to.be.eq(8)
                        expect(data[2].items[0].summary[0]).to.be.eq(816)
                        expect(data[3].key).to.be.eq('A')
                        expect(data[3].items).to.have.lengthOf(96)
                        expect(Number(data[3].items[0].key)).to.be.eq(8)
                        expect(data[3].items[0].summary[0]).to.be.eq(872)
                    })

                    it('Num + Value + Num', async () => {
                        const body = generator.dimension({ type: 'numerics' }).sort({direction: 'DESC'}).dimension({ type: 'numerics' }).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(96)
                        expect(data[0]).to.have.property('key')
                        expect(Number(data[0].key)).to.be.eq(24.8)
                        expect(data[0].items).to.have.lengthOf(1)
                        expect(Number(data[0].items[0].key)).to.be.eq(24.8)
                        expect(data[0].items[0].summary[0]).to.be.closeTo(5753.6, 0.1)
                        expect(Number(data[1].key)).to.be.eq(24.6)
                        expect(data[1].items).to.have.lengthOf(1)
                        expect(Number(data[1].items[0].key)).to.be.eq(24.6)
                        expect(data[1].items[0].summary[0]).to.be.closeTo(5510.400000000001, 0.1)
                        expect(Number(data[2].key)).to.be.eq(23.7)
                        expect(data[2].items).to.have.lengthOf(1)
                        expect(Number(data[2].items[0].key)).to.be.eq(23.7)
                        expect(data[2].items[0].summary[0]).to.be.closeTo(4266, 0.1)
                        expect(Number(data[3].key)).to.be.eq(23.5)
                        expect(data[3].items).to.have.lengthOf(1)
                        expect(Number(data[3].items[0].key)).to.be.eq(23.5)
                        expect(data[3].items[0].summary[0]).to.be.closeTo(4911.5, 0.1)
                    })
                })

                describe('Word Cloud', () => {
                    let generator
                    beforeEach(async () => {
                        await sleep(time)
                        generator = new BodyGenerator('chart', columns)
                    })

                    it('Text + Value', async () => {
                        const body = generator.dimension().sort({direction: 'DESC'}).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('O')
                        expect(data[1].key).to.be.eq('B')
                        expect(data[2].key).to.be.eq('AB')
                        expect(data[3].key).to.be.eq('A')
                        expect(data[0].summary[0]).to.be.closeTo(328468.7, 0.1)
                        expect(data[1].summary[0]).to.be.closeTo(326355.2, 0.1)
                        expect(data[2].summary[0]).to.be.closeTo(329239.2, 0.1)
                        expect(data[3].summary[0]).to.be.closeTo(332277.5, 0.1)
                    })
                    
                    it('Num + Value', async () => {
                        const body = generator.dimension({ type: 'numerics' }).sort({direction: 'DESC'}).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(96)
                        expect(data[0]).to.have.property('key')
                        expect(Number(data[0].key)).to.be.closeTo(24.8, 0.1)
                        expect(data[0].summary[0]).to.be.closeTo(5753.6, 0.1)
                    })
                    
                    it('Date + Value', async () => {
                        const values = ['1954']
                        const column = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension({ type: 'dates', dateGroup: 'month' }).filter().expression({values, ...column}).sort({direction: 'DESC'}).summary().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        expect(chartItem).to.be.an('array')
                        expect(chartItem).to.have.lengthOf(1)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(6)
                        expect(data[0]).to.have.property('key')
                        expect(data[0].key).to.be.eq('Dec 1954')
                        expect(data[0].summary[0]).to.be.closeTo(2248.5, 0.1)
                        expect(data[1].key).to.be.eq('Nov 1954')
                        expect(data[1].summary[0]).to.be.closeTo(1964.7, 0.1)
                    })
                })

                describe('Continuous X/Y charts', () => {
                    it('Date Group: Year', async () => {
                        const date = { type: 'dates', dateGroup: 'YEAR' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(49)
                        expect(data[0].key).to.be.eq('1954')
                        expect(data[1].key).to.be.eq('1955')
                        expect(data[48].key).to.be.eq('2002')
                    })

                    it('Date Group: Quarter, year', async () => {
                        const date = { type: 'dates', dateGroup: 'QUARTER' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(193)
                        expect(data[0].key).to.be.eq('Q3 1954')
                        expect(data[1].key).to.be.eq('Q4 1954')
                        expect(data[190].key).to.be.eq('Q1 2002')
                        expect(data[191].key).to.be.eq('Q2 2002')
                    })

                    it('Date Group: Month, year', async () => {
                        const date = { type: 'dates', dateGroup: 'MONTH' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[6].key).to.be.eq('Jan 1955')
                        expect(data[7].key).to.be.eq('Feb 1955')
                        expect(data[8].key).to.be.eq('Mar 1955')
                        expect(data[9].key).to.be.eq('Apr 1955')
                        expect(data[10].key).to.be.eq('May 1955')
                        expect(data[11].key).to.be.eq('Jun 1955')
                        expect(data[12].key).to.be.eq('Jul 1955')
                        expect(data[13].key).to.be.eq('Aug 1955')
                        expect(data[14].key).to.be.eq('Sep 1955')
                        expect(data[15].key).to.be.eq('Oct 1955')
                        expect(data[16].key).to.be.eq('Nov 1955')
                        expect(data[17].key).to.be.eq('Dec 1955')
                    })

                    it('Date Group: Week, year', async () => {
                        const date = { type: 'dates', dateGroup: 'WEEK' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('W29 1954')
                        expect(data[1].key).to.be.eq('W30 1954')
                        expect(data[20].key).to.be.eq('W49 1954')
                    })

                    it('Date Group: Full date', async () => {
                        const date = { type: 'dates', dateGroup: 'DAY' }
                        const body = generator.dimension(date).size(2).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data[0].key).to.be.eq('07/24/1954')
                        expect(data[1].key).to.be.eq('07/25/1954')
                    })

                    it('Date Group: Date, Hour', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'HOUR' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:00:00')
                        expect(data[6].key).to.be.eq('07/25/1954 18:00:00')
                    })

                    it('Date Group: Date, Minute', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'MINUTE' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:06:00')
                        expect(data[6].key).to.be.eq('07/25/1954 18:39:00')
                    })

                    it('Date Group: Date, Second', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'SECOND' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq('07/24/1954 16:06:50')
                        expect(data[6].key).to.be.eq('07/25/1954 18:39:04')
                    })

                    it('Date Group: Quarter', async () => {
                        const date = { type: 'dates', dateGroup: 'QUARTER_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(4)
                        expect(data[0].key).to.be.eq('Q1')
                        expect(data[1].key).to.be.eq('Q2')
                        expect(data[2].key).to.be.eq('Q3')
                        expect(data[3].key).to.be.eq('Q4')
                    })

                    it('Date Group: Month', async () => {
                        const date = { type: 'dates', dateGroup: 'MONTH_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(12)
                        expect(data[0].key).to.be.eq('Jan')
                        expect(data[1].key).to.be.eq('Feb')
                        expect(data[2].key).to.be.eq('Mar')
                        expect(data[3].key).to.be.eq('Apr')
                        expect(data[4].key).to.be.eq('May')
                        expect(data[5].key).to.be.eq('Jun')
                        expect(data[6].key).to.be.eq('Jul')
                        expect(data[7].key).to.be.eq('Aug')
                        expect(data[8].key).to.be.eq('Sep')
                        expect(data[9].key).to.be.eq('Oct')
                        expect(data[10].key).to.be.eq('Nov')
                        expect(data[11].key).to.be.eq('Dec')
                    })

                    it('Date Group: Day (Month)', async () => {
                        const date = { type: 'dates', dateGroup: 'DAY_ONLY' }
                        const body = generator.dimension(date).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(31)
                        expect(data[0].key).to.be.eq(1)
                        expect(data[30].key).to.be.eq(31)
                    })

                    it('Date Group: Hour', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'HOUR_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(0)
                        expect(data[6].key).to.be.eq(20)
                    })
                    
                    it('Date Group: Minute', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'MINUTE_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(6)
                        expect(data[6].key).to.be.eq(51)
                    })
                    
                    it('Date Group: Second', async () => {
                        const values = [{ gte: '07/24/1954', lte: '07/25/1954' }]
                        const validation = 'RANGE'
                        const columnFilter = { type: 'dates', dateGroup: 'day' }

                        const dimension = { type: 'dates', dateGroup: 'SECOND_ONLY' }
                        const body = generator.dimension(dimension).filter().expression({ values, validation, ...columnFilter }).sort().toBody()
                        const chartItem = await resultsService.chartResult(config, body)
                        const data = chartItem[0].data
                        expect(data).to.have.lengthOf(7)
                        expect(data[0].key).to.be.eq(0)
                        expect(data[6].key).to.be.eq(53)
                    })
                })
            })
        })
    })
})
