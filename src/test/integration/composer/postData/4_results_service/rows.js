'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');

context('Rows endpoint (Simple table)', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit Rows for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            describe('Simple table', () => {
                let generator
                beforeEach(async () => {
                    await sleep(time)
                    generator = new BodyGenerator('rows', columns)
                })

                it('2 Columns', async () => {
                    const body = generator
                        .dimension()
                        .sort({ direction: 'ASC' })
                        .dimension({ type: 'numerics' })
                        .sort({ direction: 'ASC' })
                        .toBody()
                    const chartItem = await resultsService.rowsResult(config, body)
                    expect(chartItem).to.has.property('totalRecords').to.be.a('number').to.be.eq(1e5)
                    expect(chartItem).to.has.property('items').to.be.an('object')
                    expect(chartItem.items).to.has.property('data').to.be.an('array').to.have.lengthOf(50)
                    const data = chartItem.items.data
                    expect(data[0]).to.be.an('array').to.have.lengthOf(2)
                    expect(data[0][0]).to.be.a('string')
                    expect(data[0][1]).to.be.a('number')
                })

                it('1 Columns + Totals (AVG and COUNT)', async () => {
                    const body = generator.dimension().summary({ agg: 'AVG' }).summary({ agg: 'COUNT' }).toBody()
                    const chartItem = await resultsService.rowsResult(config, body)
                    expect(chartItem).to.has.property('aggregations').to.be.an('array').to.have.lengthOf(1)
                    expect(chartItem.aggregations[0]).to.has.property('aggregates').to.be.an('object')
                    expect(chartItem.aggregations[0]).to.has.property('questionid').to.be.an('string')
                    const aggregates = chartItem.aggregations[0].aggregates
                    expect(aggregates).to.has.property('AVG').to.be.an('number').to.be.closeTo(15.52, 0.01)
                    expect(aggregates).to.has.property('COUNT').to.be.an('number').to.be.eq(84807)
                })

                it('1 Columns + Sorting ASC', async () => {
                    const body = generator.dimension({ id: 1 }).sort().toBody()
                    const chartItem = await resultsService.rowsResult(config, body)
                    const data = chartItem.items.data
                    expect(data[0][0]).to.be.eq('Agender')
                    expect(data[1][0]).to.be.eq('Agender')
                    expect(data[10][0]).to.be.eq('Agender')
                    expect(data[20][0]).to.be.eq('Agender')
                })

                it('Max rows of 2', async () => {
                    const body = generator.dimension().addToRoot({ take: 2 }).toBody()
                    const chartItem = await resultsService.rowsResult(config, body)
                    expect(chartItem.items).to.has.property('data').to.be.an('array').to.have.lengthOf(2)
                })
            })
        })
    })
})
