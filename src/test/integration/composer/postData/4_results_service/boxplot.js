'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');

context('Boxplot endpoint', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit Boxplot for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            describe('Boxplot', () => {
                let generator
                beforeEach(async () => {
                    await sleep(time)
                    generator = new BodyGenerator('boxplot', columns)
                })

                it('Only Value', async () => {
                    const body = generator.summary().toBody()
                    const chartItem = await resultsService.boxplotResult(config, body)
                    expect(chartItem).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(chartItem).to.has.property('max').to.be.a('number').to.be.eq(24.8)
                    expect(chartItem).to.has.property('data').to.be.a('array').to.have.lengthOf(1)
                    const data = chartItem.data[0]
                    expect(data).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(data).to.has.property('max').to.be.a('number').to.be.eq(24.8)
                    expect(data).to.has.property('q1').to.be.a('number').to.be.closeTo(13.65, 0.1)
                    expect(data).to.has.property('q2').to.be.a('number').to.be.closeTo(15.5, 0.1)
                    expect(data).to.has.property('q3').to.be.a('number').to.be.closeTo(17.15, 0.1)
                    expect(data).to.has.property('iqr').to.be.a('number').to.be.closeTo(3.50, 0.1)
                    expect(data).to.has.property('maxWhisker').to.be.a('number').to.be.closeTo(22.2, 0.1)
                    expect(data).to.has.property('minWhisker').to.be.a('number').to.be.closeTo(8.5, 0.1)
                    expect(data).to.has.property('outliers').to.be.a('array').to.have.lengthOf(5)
                    expect(data.outliers[0]).to.be.closeTo(24.8, 0.1)
                    expect(data.outliers[1]).to.be.closeTo(24.6, 0.1)
                    expect(data.outliers[2]).to.be.closeTo(23.7, 0.1)
                    expect(data.outliers[3]).to.be.closeTo(23.5, 0.1)
                    expect(data.outliers[4]).to.be.closeTo(8, 0.1)
                    expect(data).to.has.property('maxOutliers').to.be.a('array').to.have.lengthOf(4)
                    expect(data).to.has.property('minOutliers').to.be.a('array').to.have.lengthOf(1)
                })

                it('Category + Value', async () => {
                    const body = generator.dimension().summary().toBody()
                    const chartItem = await resultsService.boxplotResult(config, body)
                    expect(chartItem).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(chartItem).to.has.property('max').to.be.a('number').to.be.eq(24.8)
                    expect(chartItem).to.has.property('data').to.be.a('array').to.have.lengthOf(4)
                    const data0 = chartItem.data[0]
                    expect(data0).to.has.property('key').to.be.a('string').to.be.eq('A')
                    expect(data0).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(data0).to.has.property('max').to.be.a('number').to.be.eq(24.8)
                    expect(data0).to.has.property('q1').to.be.a('number').to.be.closeTo(13.60, 0.1)
                    expect(data0).to.has.property('q2').to.be.a('number').to.be.closeTo(15.5, 0.1)
                    expect(data0).to.has.property('q3').to.be.a('number').to.be.closeTo(17.14, 0.1)
                    expect(data0).to.has.property('iqr').to.be.a('number').to.be.closeTo(3.53, 0.1)
                    expect(data0).to.has.property('maxWhisker').to.be.a('number').to.be.closeTo(22.2, 0.1)
                    expect(data0).to.has.property('minWhisker').to.be.a('number').to.be.closeTo(8.5, 0.1)
                    const data1 = chartItem.data[1]
                    expect(data1).to.has.property('key').to.be.a('string').to.be.eq('AB')
                    expect(data1).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(data1).to.has.property('max').to.be.a('number').to.be.eq(24.8)
                    expect(data1).to.has.property('q1').to.be.a('number').to.be.closeTo(13.71, 0.1)
                    expect(data1).to.has.property('q2').to.be.a('number').to.be.closeTo(15.5, 0.1)
                    expect(data1).to.has.property('q3').to.be.a('number').to.be.closeTo(17.26, 0.1)
                    expect(data1).to.has.property('iqr').to.be.a('number').to.be.closeTo(3.55, 0.1)
                    expect(data1).to.has.property('maxWhisker').to.be.a('number').to.be.closeTo(22.2, 0.1)
                    expect(data1).to.has.property('minWhisker').to.be.a('number').to.be.closeTo(8.5, 0.1)
                })
            })
        })
    })
})
