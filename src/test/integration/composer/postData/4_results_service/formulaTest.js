'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');

context('Formula testing', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }
    const body = {
        formula: '',
        formulaMode: 'STANDARD',
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit Formula testing for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            describe('Testing Standard Formulas (v2)', () => {
                let stringColumn, numericColumn, dateColumn
                beforeEach(async () => {
                    await sleep(time)
                    const chartGenerator = new BodyGenerator('chart', columns)
                    stringColumn = chartGenerator.defaults['strings'][0].id
                    numericColumn = chartGenerator.defaults['numerics'][0].id
                    dateColumn = chartGenerator.defaults['dates'][0].id
                })

                it('Simply 1+1 test', async () => {
                    body.formula = '1 + 1'
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string');
                    expect(result).to.has.property('message').to.be.a('string');
                    expect(result).to.has.property('data').to.be.a('array');
                    const data = result.data[0].item
                    expect(data).to.be.eq(2)
                })

                it('ABS Function (literal -1)', async () => {
                    body.formula = 'ABS(-1)'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(1)
                })

                it('DateDif Function with Y param (literal 2 dates)', async () => {
                    body.formula = 'DATEDIF("12/31/2021", "12/31/2023", "Y")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([2, '2'])
                })

                it('DateDif Function with Y param (literal 2 dates) missing 1 day to full year', async () => {
                    body.formula = 'DATEDIF("12/31/2021", "12/30/2023", "Y")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([1, '1'])
                })

                it('DateDif Function with Y param (Date column)', async () => {
                    body.formula = `DATEDIF([${dateColumn}], [${dateColumn}], "Y")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([0, '0'])
                })

                it('DateDif Function with M param (literal 2 dates)', async () => {
                    body.formula = 'DATEDIF("11/30/2023", "12/30/2023", "M")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([1, '1'])
                })

                it('DateDif Function with D param (literal 2 dates)', async () => {
                    body.formula = 'DATEDIF("12/29/2023", "12/30/2023", "D")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([1, '1'])
                })

                it('YEAR Function (literal date)', async () => {
                    body.formula = 'YEAR("12/29/2023")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([2023, '2023'])
                })

                it('MONTH Function (literal date)', async () => {
                    body.formula = 'MONTH("12/29/2023")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([12, '12'])
                })

                it('DAY Function (literal date)', async () => {
                    body.formula = 'DAY("12/29/2023")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([29, '29'])
                })

                it('HOUR Function (literal date)', async () => {
                    body.formula = 'HOUR("12/29/2023 14:30:26")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([14, '14'])
                })

                it('MINUTE Function (literal date)', async () => {
                    body.formula = 'MINUTE("12/29/2023 14:30:26")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([30, '30'])
                })

                it('SECOND Function (literal date)', async () => {
                    body.formula = 'SECOND("12/29/2023 14:30:26")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([26, '26'])
                })

                it('IF Function true (literals)', async () => {
                    body.formula = 'IF(true, "YES", "NO")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("YES")
                })

                it('IF Function false (literals)', async () => {
                    body.formula = 'IF(false, "YES", "NO")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("NO")
                })

                it('IFS Function null result (literals)', async () => {
                    body.formula = 'IFS(false, "YES")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(null)
                })

                it('IFS Function true (literals)', async () => {
                    body.formula = 'IFS(true, "YES")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("YES")
                })

                it('IFS Function more params(literals)', async () => {
                    body.formula = 'IFS(false, "NO", true, "YES")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("YES")
                })

                it('ISNULL Function (literals)', async () => {
                    body.formula = `ISNULL([${numericColumn}], 0)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT');
                })

                it('DATEADD Function with 1 Y (year)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "Y", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 Y (year)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "Y", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2024-12-29T14:30:26.000Z", "2024-12-29 14:30:26.000" ])
                })

                it('DATEADD Function with 1 M (Month)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "M", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 M (Month)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "M", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2024-01-29T14:30:26.000Z", "2024-01-29 14:30:26.000" ])
                })

                it('DATEADD Function with 1 D (Days)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "D", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 D (Days)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "D", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-30T14:30:26.000Z", "2023-12-30 14:30:26.000" ])
                })

                it('DATEADD Function with 1 H (Hour)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "H", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 H (Hour)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "H", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T15:30:26.000Z", "2023-12-29 15:30:26.000" ])
                })

                it('DATEADD Function with 1 MI (Minutes)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "MI", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 MI (Minutes)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "MI", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T14:31:26.000Z", "2023-12-29 14:31:26.000" ])
                })

                it('DATEADD Function with 1 S (Seconds)', async () => {
                    body.formula = `DATEADD([${dateColumn}], "S", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATEADD Function (literals) with 1 S (Seconds)', async () => {
                    body.formula = `DATEADD("12/29/2023 14:30:26", "S", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T14:30:27.000Z", "2023-12-29 14:30:27.000" ])
                })

                it('DAYOFWEEK Function with DAY', async () => {
                    body.formula = `DAYOFWEEK([${dateColumn}], "Day")`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DAYOFWEEK Function with D', async () => {
                    body.formula = `DAYOFWEEK([${dateColumn}], "D")`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DAYOFWEEK Function (literals) with Day', async () => {
                    body.formula = `DAYOFWEEK("12/29/2023 14:30:26", "Day")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item?.trim() //For some reason, redshift return 'Friday   '
                    expect(data).to.be.eq("Friday")
                })

                it('DAYOFWEEK Function (literals) with D', async () => {
                    body.formula = `DAYOFWEEK("12/29/2023 14:30:26", "D")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf([6, "6"])
                })

                it('TRIM Function (literals)', async () => {
                    body.formula = `TRIM(" Hello Word  ")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Hello Word")
                })

                it('CONCATENATE Function (literals)', async () => {
                    body.formula = `CONCATENATE("H", "E", "L", "L", "O")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("HELLO")
                })

                it('LEFT Function (literals)', async () => {
                    body.formula = `LEFT("Hello Word", 5)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Hello")
                })

                it('RIGHT Function (literals)', async () => {
                    body.formula = `RIGHT("Hello Word", 4)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Word")
                })

                it('UPPER Function (literals)', async () => {
                    body.formula = `UPPER("HeLlo WoRd")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("HELLO WORD")
                })

                it('LOWER Function (literals)', async () => {
                    body.formula = `LOWER("HeLlo WoRd")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("hello word")
                })

                it('PROPER Function (literals)', async () => {
                    body.formula = `PROPER("heLlo woRd-s")`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Hello Word-S")
                })

                it('AND Function (literals)', async () => {
                    body.formula = `AND(true, false)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(false)
                })

                it('OR Function (literals)', async () => {
                    body.formula = `OR(true, false)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(true)
                })

                it('DATESUBTRACT Function with 1 Y (year)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "Y", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 Y (year)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "Y", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2022-12-29T14:30:26.000Z", "2022-12-29 14:30:26.000" ])
                })

                it('DATESUBTRACT Function with 1 M (Month)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "M", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 M (Month)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "M", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-11-29T14:30:26.000Z", "2023-11-29 14:30:26.000" ])
                })

                it('DATESUBTRACT Function with 1 D (Days)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "D", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 D (Days)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "D", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-28T14:30:26.000Z", "2023-12-28 14:30:26.000" ])
                })

                it('DATESUBTRACT Function with 1 H (Hour)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "H", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 H (Hour)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "H", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T13:30:26.000Z", "2023-12-29 13:30:26.000" ])
                })

                it('DATESUBTRACT Function with 1 MI (Minutes)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "MI", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 MI (Minutes)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "MI", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T14:29:26.000Z", "2023-12-29 14:29:26.000" ])
                })

                it('DATESUBTRACT Function with 1 S (Seconds)', async () => {
                    body.formula = `DATESUBTRACT([${dateColumn}], "S", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    expect(result).to.has.property('syntax').to.be.a('string').to.be.eq('CORRECT')
                })

                it('DATESUBTRACT Function (literals) with 1 S (Seconds)', async () => {
                    body.formula = `DATESUBTRACT("12/29/2023 14:30:26", "S", 1)`
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    //Same date different format (without T and Z)
                    expect(data).to.be.oneOf([ "2023-12-29T14:30:25.000Z", "2023-12-29 14:30:25.000" ])
                })

                it('MAX Function (literal 1, 2)', async () => {
                    body.formula = 'MAX(1, 2)'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(2)
                })

                it('MAX Function (literal dates)', async () => {
                    body.formula = 'MAX("12/29/2023 14:30:26", "10/15/2020 12:30:26")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf(["2023-12-29T14:30:26.000Z", "2023-12-29 14:30:26.000"])
                })

                it('MIN Function (literal 1, 2)', async () => {
                    body.formula = 'MIN(1, 2)'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(1)
                })

                it('MIN Function (literal dates)', async () => {
                    body.formula = 'MIN("12/29/2023 14:30:26", "10/15/2020 12:30:26")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.oneOf(["2020-10-15T12:30:26.000Z", "2020-10-15 12:30:26.000"])
                })

                it('INCLUDE Function (literal starting)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "Billboard")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(true)
                })

                it('INCLUDE Function (literal ending)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "Tech")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(true)
                })

                it('INCLUDE Function (literal starting Mayus)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "BILL")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(false)
                })

                it('INCLUDE Function (literal ending Mayus)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "tECH")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(false)
                })

                it('INCLUDE Function (literal single letter)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "E")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(false)
                })

                it('INCLUDE Function (literal lower)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "billboard-tech")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(false)
                })

                it('INCLUDE Function (literal dash)', async () => {
                    body.formula = 'INCLUDE("Billboard-Tech", "-")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(true)
                })

                it('REPLACE Function with mayus (literals)', async () => {
                    body.formula = 'REPLACE("Billboard-Tech", "tech", "technologies")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Billboard-Tech")
                })

                it('REPLACE Function right replacement (literals)', async () => {
                    body.formula = 'REPLACE("Billboard-Tech", "Tech", "Technologies")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("Billboard-Technologies")
                })

                it('REPLACE Function (literal a)', async () => {
                    body.formula = 'REPLACE("aaa", "aa", "b")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("ba")
                })

                it('REPLACE Function (literal aa-aa)', async () => {
                    body.formula = 'REPLACE("aa-aa", "aa", "b")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq("b-b")
                })

                it('LENGTH Function (literal)', async () => {
                    body.formula = 'LENGTH("string1")'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(7)
                })

                it('SQRT Function (literal)', async () => {
                    body.formula = 'SQRT(4)'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(2)
                })

                it('EXP Function (literal)', async () => {
                    body.formula = 'EXP(0)'
                    const result = await resultsService.formulaTestResult(config, body)
                    const data = result.data[0].item
                    expect(data).to.be.eq(1)
                })
            })
        })
    })
})
