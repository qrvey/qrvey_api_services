'use strict';
const withThese = require('mocha-each');
const { expect } = require('chai');
const { sleep } = require('../../../../../lib/helpers/commonHelper');
const time = 500;
const { GENERAL_USER_ID } = require('../../../../config/data')
const { BodyGenerator } = require('../../../../helpers/analyticsGenerator');
const ComposerHelper = require('../../../../helpers/composerHelper')
const { getColumns } = require('../../../../../lib/postData/modelService');
const resultsService = require('../../../../../lib/postData/resultsService');

context('Metric endpoint', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let composer, datasets
    const config = {
        userId: GENERAL_USER_ID,
        appId: null,
        qrveyId: null
    }

    before(async () => {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'Automated Testing App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        // Mocha-each package need this structure...
        datasets = composer.datasets.map((item, index) => {
            return [index, item.name, item.datasetId]
        })
    })

    it('Dummy Runner', () => {
        withThese(datasets).describe('Suit Metric for Dataset # %d %s with ID %s', function (index, name, datasetId) {
            this.timeout(1000 * 6 * 10 * 15);

            let columns
            before(async () => {
                try {
                    config.qrveyId = datasetId
                    const body = {
                        optionsAttributes: [
                            "id", "text", "type", "property",
                            "bucketId", "formulaId", "formulaType",
                            "geogroup", "geoGroups"
                        ],
                        splitQuestions: false
                    }
                    const columnList = await getColumns(config, body);
                    columns = columnList[0].options
                } catch (error) {
                    console.log(error);
                }
            })

            describe('Metrics', () => {
                let generator
                beforeEach(async () => {
                    await sleep(time)
                    generator = new BodyGenerator('metric', columns)
                })

                it('Numeric column', async () => {
                    const body = generator.summary({ agg: 'AVG' }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.a('number').to.be.closeTo(15.52, 0.01)
                    expect(chartItem).to.has.property('max').to.be.a('number').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.a('number').to.be.eq(8)
                    expect(chartItem).to.has.property('comparison').to.be.null
                })

                it('Comparison (last and next 30 days)', async () => {
                    const relative = generator._relative().values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid }
                    const comparison = { ...relative, questionid, cursor: 'the_next' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.null
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.equal(0)
                })

                it('Comparison (Custom Range)', async () => {
                    const questionid = generator.defaults['dates'][0].id
                    const period = {
                        questionid,
                        customRange: {
                            lt: "07/25/1954"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                            lte: "07/23/2002"
                        }
                    }
                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.a('number').to.be.closeTo(16.75, 0.01)
                    expect(chartItem).to.has.property('max').to.be.a('number').to.be.closeTo(17, 0.1)
                    expect(chartItem).to.has.property('min').to.be.a('number').to.be.closeTo(16.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.a('number').to.be.closeTo(15.27, 0.01)
                })

                it('Time Period (All History) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const body = generator.summary(column).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (All History) + Comparison (Custom Range After)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparison = { questionid, customRange: { gte: "07/17/2000" } }
                    const body = generator.summary(column).addToRoot({ comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (All History) + Comparison (Custom Range Before)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparison = { questionid, customRange: { lte: "07/17/2000" } }
                    const body = generator.summary(column).addToRoot({ comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (All History) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const customRange = { gte: "01/17/2000", lte: "07/17/2000" }
                    const comparison = { questionid, customRange, type: 'DATE' }
                    const body = generator.summary(column).addToRoot({ comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (All History) + Comparison (This Year)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparison = { questionid, cursor: 'this', unit: 'year', type: 'DATE' }
                    const body = generator.summary(column).addToRoot({ comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (All History) + Comparison (This Quarter)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparison = { questionid, type: 'DATE', cursor: 'this', unit: 'quarter' }
                    const body = generator.summary(column).addToRoot({ comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (This Year) + Comparison (Last Year)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'year' }
                    const comparison = { ...comparisonColumn, cursor: 'the_last', unit: 'year', number: 1, includeCurrent: false, isCalendarDate: true }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([null, 0])
                })

                it('Time Period (This Year) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'year' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (This Year) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'year' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Year) + Comparison (This Year)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'year' }
                    const comparison = { ...comparisonColumn, cursor: 'this', unit: 'year', number: 1, includeCurrent: false, isCalendarDate: true }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([null, 0])
                })

                it('Time Period (Last Year) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'year' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (Last Year) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'year' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (This Quarter) + Comparison (Last Quarter)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'quarter' }
                    const comparison = { ...comparisonColumn, cursor: 'the_last', unit: 'quarter', number: 1, includeCurrent: false, isCalendarDate: true }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([null, 0])
                })

                it('Time Period (This Quarter) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'quarter' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (This Quarter) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'quarter' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Quarter) + Comparison (This Quarter)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'quarter' }
                    const comparison = { questionid, type: 'DATE', cursor: 'this', unit: 'quarter' }
                    const body = generator.summary(column).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Quarter) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'quarter' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })
                
                it('Time Period (Last Quarter) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'quarter' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (This Month) + Comparison (Last Month)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'month' }
                    const comparison = { questionid, type: 'DATE', cursor: 'the_last', unit: 'month' }
                    const body = generator.summary(column).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (This Month) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'month' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (This Month) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'month' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Month) + Comparison (This Month)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'month' }
                    const comparison = { questionid, type: 'DATE', cursor: 'this', unit: 'month' }
                    const body = generator.summary(column).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Month) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'month' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (Last Month) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'month' }
                    const comparison = { ...comparisonColumn, gte: "01/17/2000", lte: "07/17/2000" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (This Week) + Comparison (Last Week)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'week' }
                    const comparison = { questionid, type: 'DATE', cursor: 'the_last', unit: 'week', number: 1, includeCurrent: false }
                    const body = generator.summary(column).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (This Week) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'week' }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                    expect(chartItem.comparison).to.be.closeTo(1316340.6, 0.1)
                })

                it('Time Period (This Week) + Comparison (Custom Range Between)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'this', unit: 'week' }
                    const comparison = { ...comparisonColumn, gte: "07/21/2002", lte: "07/23/2002" }
                    const body = generator.summary(column).addToRoot({ period, comparison }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Week) + Comparison (This Week)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'week', number: 1, includeCurrent: false }
                    const comparison = { questionid, type: 'DATE', cursor: 'this', unit: 'week' }
                    const body = generator.summary(column).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Week) + Comparison (All History)', async () => {
                    const column = { type: 'numerics', agg: 'SUM' }
                    const questionid = generator.defaults['dates'][0].id
                    const comparisonColumn = { questionid, type: 'DATE' };
                    const period = { ...comparisonColumn, cursor: 'the_last', unit: 'week', number: 1, includeCurrent: false }
                    const body = generator.summary(column).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem.value).to.be.oneOf([0, null])
                })

                it('Time Period (Last Week) + Comparison (Custom Range Between)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid }

                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                            lte: "07/23/2002"
                        }
                    }
                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.27, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-15.27, 0.1)
                })

                it('Time Period (Today) + Comparison (Yesterday)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "day"
                    }
                    const comparison = { ...relative, questionid, cursor: 'the_last', unit: 'day' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Today) + Comparison (All History)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "day"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-15.52, 0.1)
                })

                it('Time Period (Today) + Comparison (Custom Range Between)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "day"
                    }

                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                            lte: "07/23/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.27, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-15.27, 0.1)
                })

                it('Time Period (Yesterday) + Comparison (Today)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'day' }
                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "day"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.oneOf([null, 0])
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Yesterday) + Comparison (All History)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'day' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-15.52, 0.1)
                })

                it('Time Period (Yesterday) + Comparison (Custom Range Between)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'day' }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                            lte: "07/23/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.27, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-15.27, 0.1)
                })

                it('Time Period (This Hour) + Comparison (Last Hour)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "hour"
                    }
                    const comparison = { ...relative, questionid, cursor: 'the_last', unit: 'hour' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Last Hour) + Comparison (This Hour)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'hour' }
                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "hour"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (This Minute) + Comparison (Last Minute)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "minute"
                    }
                    const comparison = { ...relative, questionid, cursor: 'the_last', unit: 'minute' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Last Minute) + Comparison (This Minute)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'minute' }
                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "minute"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (This Second) + Comparison (Last Second)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        cursor: "this",
                        unit: "second"
                    }
                    const comparison = { ...relative, questionid, cursor: 'the_last', unit: 'second' }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Last Second) + Comparison (This Second)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = { ...relative, questionid, cursor: 'the_last', unit: 'second' }
                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "second"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.oneOf([null, 0])
                    expect(chartItem).to.has.property('max').to.be.equal(10)
                    expect(chartItem).to.has.property('min').to.be.equal(0)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)
                })

                it('Time Period (Custom Range Before) + Comparison (Custom Range After)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            lte: "01/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.52, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.37, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0.14, 0.1)
                })

                it('Time Period (Custom Range Before) + Comparison (Custom Range Between)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            lte: "01/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "07/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.52, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.39, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0.12, 0.1)
                })

                it('Time Period (Custom Range Before) + Comparison (All History)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            lte: "01/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.52, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(0, 0.1)

                })

                it('Time Period (Custom Range Before) + Comparison (Today)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            lte: "01/01/2002"
                        }
                    }

                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "day"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.52, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.52, 0.1)
                })

                it('Time Period (Custom Range Before) + Comparison (Last Quarter)', async () => {
                    const questionid = generator.defaults['dates'][0].id
                    const relative = generator._relative({ cursor: 'the_last', number: 1, unit: 'quarter', includeCurrent: false }).values[0]
                    const period = {
                        questionid,
                        customRange: {
                            lte: "01/01/1982"
                        }
                    }

                    const comparison = { ...relative, questionid }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.53, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.53, 0.1)
                })

                it('Time Period (Custom Range After) + Comparison (Custom Range Before)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            lte: "01/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.37, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.14, 0.1)
                })

                it('Time Period (Custom Range After) + Comparison (Custom Range Between)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "07/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.37, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.39, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.015, 0.1)
                })

                it('Time Period (Custom Range After) + Comparison (All History)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.37, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.14, 0.1)

                })

                it('Time Period (Custom Range After) + Comparison (This Month)', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }

                    const comparison = {
                        questionid,
                        cursor: "this",
                        unit: "month"
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.37, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.37, 0.1)
                })

                it('Time Period (Custom Range After) + Comparison (Last Week)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'week', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002"
                        }
                    }
                    const comparison = { ...relative, questionid }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.37, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(24.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.37, 0.1)
                })

                it('Time Period (Custom Range Between) + Comparison (Custom Range Between) ', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                            lte: "07/23/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.051, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.27, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.21, 0.1)
                })

                it('Time Period (Custom Range Between) + Comparison (Custom Range After) ', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            gte: "07/21/2002",
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.051, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.24, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.19, 0.1)
                })

                it('Time Period (Custom Range Between) + Comparison (Custom Range Before) ', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }
                    const comparison = {
                        questionid,
                        customRange: {
                            lte: "07/21/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.051, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.521, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.47, 0.1)
                })

                it('Time Period (Custom Range Between) + Comparison (All History) ', async () => {
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.05, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(15.52, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(-0.47, 0.1)

                })

                it('Time Period (Custom Range Between) + Comparison (Yesterday)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'day', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }
                    const comparison = { ...relative, questionid }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.05, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.05, 0.1)
                })

                it('Time Period (Custom Range Between) + Comparison (Last Year)', async () => {
                    const relative = generator._relative({ number: 1, unit: 'year', includeCurrent: false }).values[0]
                    const questionid = generator.defaults['dates'][0].id

                    const period = {
                        questionid,
                        customRange: {
                            gte: "01/01/2002",
                            lte: "02/01/2002"
                        }
                    }
                    const comparison = { ...relative, questionid }

                    const body = generator.summary({ agg: 'AVG' }).addToRoot({ comparison, period }).toBody()
                    const chartItem = await resultsService.metricResult(config, body)
                    expect(chartItem).to.has.property('value').to.be.closeTo(15.05, 0.1)
                    expect(chartItem).to.has.property('max').to.be.closeTo(21.8, 0.1)
                    expect(chartItem).to.has.property('min').to.be.closeTo(8.5, 0.1)
                    expect(chartItem).to.has.property('comparison').to.be.closeTo(0, 0.1)
                    // expect(chartItem).to.has.property('difference').to.be.closeTo(15.05, 0.1)
                })
            })
        })
    })
})
