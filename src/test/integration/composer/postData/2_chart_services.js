'use strict';
const { expect } = require('chai');
const DatasetService = require('../../../../lib/preData/datasetService');
const ChartService = require('../../../../lib/postData/chartService');
const ComposerHelper = require('../../../helpers/composerHelper');
const { GENERAL_USER_ID } = require('../../../config/data');

let composer;
const config = {
    userId: GENERAL_USER_ID,
    appId: null,
    qrveyId: null,
    chartId: null
}

describe('Chart services', function () {
    this.timeout(1000 * 6 * 10 * 15);

    before(async function () {
        try {
            composer = await ComposerHelper.preLoadApplication()
        } catch (error) {
            composer = await ComposerHelper.postDataApp(GENERAL_USER_ID, {
                name: 'App for PostData Team',
                description: 'Delete this app if you can see it'
            })
        }
        config.appId = composer.appId
        config.qrveyId = composer.datasets[0].datasetId
    })

    describe('Chart over single Dataset', function () {

        it('Dataset status should be loaded', async function () {
            const datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, config.appId, config.qrveyId);
            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            return Promise.resolve();
        })

        it('It should create a chart', async function () {
            const body = {
                title: 'Chart',
                position: 0
            };
            const chartItem = await ChartService.createChart(config, body);
            expect(chartItem).to.be.a('object');
            expect(chartItem).to.has.property('chartid').to.be.a('string');
            config.chartId = chartItem.chartid
            expect(chartItem).to.has.property('title').to.be.a('string').to.be.eq(body.title);
            return Promise.resolve();
        });

        it('It should return the current chart', async function () {
            const chartItem = await ChartService.getChart(config)
            expect(chartItem).to.be.a('object');
            expect(chartItem).to.has.property('position').to.be.a('number');
            expect(chartItem).to.has.property('title').to.be.a('string');
            return Promise.resolve();
        })

        it('It should delete the current chart', async function () {
            const chartItem = await ChartService.deleteChart(config)
            expect(chartItem).to.be.a('object');
            return Promise.resolve();
        })

        it('It should return the list of charts', async function () {
            const chartItem = await ChartService.getChartList(config)
            expect(chartItem).to.be.a('object');
            expect(chartItem).to.has.property('Count').to.be.a('number');
            expect(chartItem).to.has.property('Items').to.be.a('array');
            return Promise.resolve();
        })
    })

    after(async function () {
        await composer.deleteAll()
    })

})
