'use strict';
const { expect } = require('chai');

const {
    commonHelper: CommonHelper,
} = require('../../../../lib/helpers');

const ApplicationService = require('../../../../lib/applicationService');
const ConnectionService = require('../../../../lib/preData/connectionService');
const DatasetService = require('../../../../lib/preData/datasetService');
const DataRouterService = require('../../../../lib/preData/dataRouterService');

const {
    GENERAL_USER_ID,
    CONNECTIONS,
} = require('../../../config/data');

let applications = {
    app1: {
        name: 'My app',
        appId: undefined,
    },
};

let connections = {
    mysqlConnection: {
        name: 'My MYSQL connection',
        host: CONNECTIONS.mysql.host,
        password: CONNECTIONS.mysql.password,
        user: CONNECTIONS.mysql.user,
        connectorType: CONNECTIONS.mysql.connectorType,
        port: CONNECTIONS.mysql.port,
        connectionId: undefined,
    },
    mssqlConnection: {
        name: 'My MSSQL connection',
        host: CONNECTIONS.mssql.host,
        password: CONNECTIONS.mssql.password,
        user: CONNECTIONS.mssql.user,
        connectorType: CONNECTIONS.mssql.connectorType,
        port: CONNECTIONS.mssql.port,
        connectionId: undefined,
    },
    oracleConnection: {
        name: 'My Oracle connection',
        host: CONNECTIONS.oracle.host,
        password: CONNECTIONS.oracle.password,
        user: CONNECTIONS.oracle.user,
        serviceName: CONNECTIONS.oracle.serviceName,
        connectorType: CONNECTIONS.oracle.connectorType,
        port: CONNECTIONS.oracle.port,
        connectionId: undefined,
    },
    athenaConnection: {
        name: 'My Athena connection',
        configType: 'typical',
        accessKey: CONNECTIONS.athena.user,
        awsRegion: CONNECTIONS.athena.awsRegion,
        // catalogName: CONNECTIONS.athena.catalogName,
        connectorType: CONNECTIONS.athena.connectorType,
        hasHeader: true,

        secretKey: CONNECTIONS.athena.password,
        workGroup: CONNECTIONS.athena.workGroup,

        outputLocation: CONNECTIONS.athena.outputLocation,

        connectionId: undefined,

    }
};

describe('Dataset service', function () {
    this.timeout(1000 * 6 * 10 * 15);

    before(async function () {

        // Application
        let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);
        applications.app1.appId = applicationItem.appid;

        // Connections
        let mysqlConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection);
        connections.mysqlConnection.connectionId = mysqlConnectionItem.connectorid;

        let athenaConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection);
        connections.athenaConnection.connectionId = athenaConnectionItem.connectorid;

        let mssqlConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection);
        connections.mssqlConnection.connectionId = mssqlConnectionItem.connectorid;

        let oracleConnectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection);
        connections.oracleConnection.connectionId = oracleConnectionItem.connectorid;
    });

    // MYSQL connection

    describe('Dataset: Dataset using MYSQL connection', function () {
        let datasets = {
            mysql: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.mysql.name,
                connectionId: connections.mysqlConnection.connectionId,
                connectorType: connections.mysqlConnection.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body);

            expect(datasetItem).to.be.a('object');
            datasets.mysql.datasetId = datasetItem.datasetId;
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mysql.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            return Promise.resolve();
        });
    });

    describe('Dataset-Invoke: Dataset using MYSQL connection', function () {
        let datasets = {
            mysql: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should not create a dataset', async function () {
            let body = {
                invalid: true,
            };

            return DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true })
                .then(data => {
                    return Promise.reject(data);
                })
                .catch(error => {
                    expect(error).to.be.a('object');
                });
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.mysql.name,
                connectionId: connections.mysqlConnection.connectionId,
                connectorType: connections.mysqlConnection.connectorType,
                database: CONNECTIONS.mysql.databases.general.name,
                tableName: CONNECTIONS.mysql.databases.general.tables.wineQuality.tableName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            datasets.mysql.datasetId = datasetItem.datasetId;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mysql.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mysql.datasetId, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mysql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mysql.name);

            return Promise.resolve();
        });
    });

    // MSSQL connection

    describe('Dataset: Dataset using MSSQL connection', function () {
        let datasets = {
            mssql: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.mssql.name,
                connectionId: connections.mssqlConnection.connectionId,
                connectorType: connections.mssqlConnection.connectorType,
                database: CONNECTIONS.mssql.databases.general.name,
                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mssql.name);

            datasets.mssql.datasetId = datasetItem.datasetId;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mssql.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mssql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mssql.name);

            return Promise.resolve();
        });
    });

    describe('Dataset-Invoke: Dataset using MSSQL connection', function () {
        let datasets = {
            mssql: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should not create a dataset', async function () {
            let body = {
                invalid: true,
            };

            return DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true })
                .then(data => {
                    return Promise.reject(data);
                })
                .catch(error => {
                    expect(error).to.be.a('object');
                });
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.mssql.name,
                connectionId: connections.mssqlConnection.connectionId,
                connectorType: connections.mssqlConnection.connectorType,
                database: CONNECTIONS.mssql.databases.general.name,
                tableName: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableName,
                tableSchema: CONNECTIONS.mssql.databases.general.tables.wineQuality.tableSchema,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mssql.name);

            datasets.mssql.datasetId = datasetItem.datasetId;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.mssql.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.mssql.datasetId, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.mssql.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.mssql.name);

            return Promise.resolve();
        });
    });

    // Oracle connection

    describe('Dataset: Dataset using Oracle connection', function () {
        let datasets = {
            oracle: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.oracle.name,
                connectionId: connections.oracleConnection.connectionId,
                connectorType: connections.oracleConnection.connectorType,
                database: CONNECTIONS.oracle.databases.general.name,
                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.oracle.name);

            datasets.oracle.datasetId = datasetItem.datasetId;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);
            let jobData = await DataRouterService.getJobStatus(datasetItem.jobId);

            expect(jobData).to.be.a('object');
            expect(jobData).has.property('jobCompletionStatistics').to.be.an('object');
            expect(jobData).has.property('statusJob').to.be.an('object');
            expect(jobData.statusJob).has.property('jobId').to.be.an('string').to.be.eq(datasetItem.jobId);
            expect(jobData.statusJob).has.property('status').to.be.an('string');
            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.oracle.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.oracle.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.oracle.name);

            return Promise.resolve();
        });
    });

    describe('Dataset-Invoke: Dataset using Oracle connection', function () {
        let datasets = {
            oracle: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should not create a dataset', async function () {
            let body = {
                invalid: true,
            };

            return DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true })
                .then(data => {
                    return Promise.reject(data);
                })
                .catch(error => {
                    expect(error).to.be.a('object');
                });
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.oracle.name,
                connectionId: connections.oracleConnection.connectionId,
                connectorType: connections.oracleConnection.connectorType,
                database: CONNECTIONS.oracle.databases.general.name,
                tableName: CONNECTIONS.oracle.databases.general.tables.wineQuality.tableName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.oracle.name);

            datasets.oracle.datasetId = datasetItem.datasetId;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.oracle.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.oracle.datasetId, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.oracle.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.oracle.name);

            return Promise.resolve();
        });
    });

    // Athena connection

    describe('Dataset: Dataset using an Athena connection', function () {
        let datasets = {
            athena: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.athena.name,

                connectionId: connections.athenaConnection.connectionId,
                connectorType: connections.athenaConnection.connectorType,
                database: CONNECTIONS.athena.databases.general.name,
                tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
                catalogName: CONNECTIONS.athena.databases.general.catalogName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.athena.name);

            datasets.athena.datasetId = datasetItem.datasetId;
            datasets.athena.confirmedLoadedStatusTime = datasetItem.confirmedLoadedStatusTime;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId);
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId);

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.athena.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId);

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.athena.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.athena.name);

            return Promise.resolve();
        });

    });

    describe('Dataset-Invoke: Dataset using an Athena connection', function () {
        let datasets = {
            athena: {
                confirmedLoadedStatusTime: undefined,
                datasetId: undefined,
                name: 'Untitled Dataset',
            },
        };

        it('It should return an empty list of dataset', async function () {
            let connectionListItem = await DatasetService.getDatasets(GENERAL_USER_ID, applications.app1.appId, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should create a dataset', async function () {
            let body = {
                name: datasets.athena.name,

                connectionId: connections.athenaConnection.connectionId,
                connectorType: connections.athenaConnection.connectorType,
                database: CONNECTIONS.athena.databases.general.name,
                tableName: CONNECTIONS.athena.databases.general.tables.client.tableName,
                catalogName: CONNECTIONS.athena.databases.general.catalogName,
            };

            let datasetItem = await DatasetService.createDatasetFromDataSource(GENERAL_USER_ID, applications.app1.appId, body, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string');
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.athena.name);

            datasets.athena.datasetId = datasetItem.datasetId;
            datasets.athena.confirmedLoadedStatusTime = datasetItem.confirmedLoadedStatusTime;

            return Promise.resolve();
        });

        it('It should load a dataset', async function () {
            let datasetItem = await DatasetService.loadDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('ok').to.be.a('boolean').to.be.eq(true);

            return Promise.resolve();
        });

        it('Dataset status should be loaded', async function () {

            let stopProcess = false;

            while (stopProcess !== true) {
                let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId, { invoke: true });
                if (datasetItem.status !== 'LOADING') {
                    stopProcess = true;
                }

                await CommonHelper.sleep(2000);
            }

            let datasetItem = await DatasetService.getDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId, { invoke: true });

            expect(datasetItem).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
            expect(datasetItem).to.has.property('confirmedLoadedStatusTime').to.be.a('string').to.not.be.equal(datasets.athena.confirmedLoadedStatusTime);

            return Promise.resolve();
        });

        it('It should delete a dataset', async function () {
            let datasetItem = await DatasetService.deleteDataset(GENERAL_USER_ID, applications.app1.appId, datasets.athena.datasetId, { invoke: true });

            expect(datasetItem).to.be.a('object');
            expect(datasetItem).to.has.property('datasetId').to.be.a('string').to.be.eq(datasets.athena.datasetId);
            // expect(datasetItem).to.has.property('name').to.be.a('string').to.be.eq(datasets.athena.name);

            return Promise.resolve();
        });

    });

    after(async function () {
        await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);
        await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId);
        await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId);
        await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId);
        await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId);
    });
});
