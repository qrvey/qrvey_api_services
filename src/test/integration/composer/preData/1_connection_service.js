'use strict';
const { expect } = require('chai');

const ApplicationService = require('../../../../lib/applicationService');
const ConnectionService = require('../../../../lib/preData/connectionService');

const {
    GENERAL_USER_ID,
    WRONG_APP_ID,
    WRONG_USER_ID,
    CONNECTIONS,
} = require('../../../config/data');

let applications = {
    app1: {
        name: 'My app',
        appId: undefined,
    },
};

let connections = {
    mysqlConnection: {
        name: 'MYSQL connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.mysql.connectorType,
    },
    advancedMysqlConnection: {
        name: 'Advance MYSQL connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.mysql.connectorType,
        configType: 'advanced',
    },
    mssqlConnection: {
        name: 'MSSQL connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.mssql.connectorType,
    },
    advancedMssqlConnection: {
        name: 'Advance MSSQL connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.mssql.connectorType,
        configType: 'advanced',
    },
    oracleConnection: {
        name: 'Oracle connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.oracle.connectorType,
    },
    advancedOracleConnection: {
        connectString: CONNECTIONS.oracle.connectString,
        name: 'Oracle connection',
        connectionId: undefined,
        connectorType: CONNECTIONS.oracle.connectorType,
        configType: 'advanced',
    },
    athenaConnection: {
        name: 'Athena connection',
        updatedName: 'Athena connection II',
        connectionId: undefined,
        connectorType: 'ATHENA',
    },
};

describe('Connection service', function () {
    this.timeout(1000 * 6 * 10 * 15);

    before(async function () {
        let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);
        applications.app1.appId = applicationItem.appid;
    });

    describe('Connection-Basic: Get connections', function () {

        it('It should return an empty list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should return an empty list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, WRONG_APP_ID);
            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(WRONG_USER_ID, applications.app1.appId);
            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);
        });

    });

    // MYSQL connection

    describe('Connection-Basic: Mysql connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.mysqlConnection.name,
                host: CONNECTIONS.mysql.host,
                password: CONNECTIONS.mysql.password,
                user: CONNECTIONS.mysql.user,
                connectorType: connections.mysqlConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            connections.mysqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mysqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mysqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            return Promise.resolve();
        });
    });

    describe('Connection-Basic: Mysql-Advanced connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.advancedMysqlConnection.name,
                connectorType: connections.advancedMysqlConnection.connectorType,
                configType: connections.advancedMysqlConnection.configType,
                connectionConfig: {
                    user: CONNECTIONS.mysql.user,
                    host: CONNECTIONS.mysql.host,
                    password: CONNECTIONS.mysql.password,
                },
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem.connectionConfig).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.connectorType);

            connections.advancedMysqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMysqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.connectionId);
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem.connectionConfig).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMysqlConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.advancedMysqlConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMysqlConnection.connectionId);

            // expect(connectionItem).to.be.a('object');
            // expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.connectionId);
            // expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            // expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            // expect(connectionItem.connectionConfig).to.has.propertyhostserver').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);

            // expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            // expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.configType);
            // expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            // expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMysqlConnection.connectorType);

            return Promise.resolve();
        });
    });

    describe('Connection-Invoke: Mysql connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.mysqlConnection.name,
                host: CONNECTIONS.mysql.host,
                password: CONNECTIONS.mysql.password,
                user: CONNECTIONS.mysql.user,
                connectorType: connections.mysqlConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            connections.mysqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mysqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId, { invoke: true });
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId, { connectionData: { ...connectionItem } }, { invoke: true });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mysqlConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mysqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mysqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mysql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mysql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mysql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mysqlConnection.connectorType);

            return Promise.resolve();
        });
    });

    // MSSQL connection

    describe('Connection-Basic: MSSQL connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.mssqlConnection.name,
                host: CONNECTIONS.mssql.host,
                password: CONNECTIONS.mssql.password,
                user: CONNECTIONS.mssql.user,
                connectorType: connections.mssqlConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            connections.mssqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mssqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mssqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            return Promise.resolve();
        });
    });

    describe('Connection-Basic: MSSQL-Advanced connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.advancedMssqlConnection.name,
                connectorType: connections.advancedMssqlConnection.connectorType,
                configType: connections.advancedMssqlConnection.configType,
                connectionConfig: {
                    user: CONNECTIONS.mssql.user,
                    server: CONNECTIONS.mssql.host,
                    password: CONNECTIONS.mssql.password,
                },
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem.connectionConfig).to.has.property('server').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.connectorType);

            connections.advancedMssqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMssqlConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.connectionId);
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem.connectionConfig).to.has.property('server').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMssqlConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.advancedMssqlConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedMssqlConnection.connectionId);

            // expect(connectionItem).to.be.a('object');
            // expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.connectionId);
            // expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            // expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            // expect(connectionItem.connectionConfig).to.has.property('server').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);

            // expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            // expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.configType);
            // expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            // expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedMssqlConnection.connectorType);

            return Promise.resolve();
        });
    });


    describe('Connection-Invoke: MSSQL connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.mssqlConnection.name,
                host: CONNECTIONS.mssql.host,
                password: CONNECTIONS.mssql.password,
                user: CONNECTIONS.mssql.user,
                connectorType: connections.mssqlConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            connections.mssqlConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mssqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId, { invoke: true });
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId, { connectionData: { ...connectionItem } }, { invoke: true });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.mssqlConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.mssqlConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.mssqlConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.mssql.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.mssql.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.mssql.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.mssqlConnection.connectorType);

            return Promise.resolve();
        });
    });

    // Oracle connection

    describe('Connection-Basic: Oracle connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.oracleConnection.name,
                host: CONNECTIONS.oracle.host,
                password: CONNECTIONS.oracle.password,
                user: CONNECTIONS.oracle.user,
                serviceName: CONNECTIONS.oracle.serviceName,
                connectorType: connections.oracleConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('serviceName').to.be.a('string').to.be.eq(CONNECTIONS.oracle.serviceName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            connections.oracleConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.oracleConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.oracleConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            return Promise.resolve();
        });
    });

    describe('Connection-Basic: Oracle-Advanced connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.advancedOracleConnection.name,
                connectorType: connections.advancedOracleConnection.connectorType,
                configType: connections.advancedOracleConnection.configType,
                connectionConfig: {
                    user: CONNECTIONS.oracle.user,
                    connectString: CONNECTIONS.oracle.connectString,
                    password: CONNECTIONS.oracle.password,
                },
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedOracleConnection.name);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.connectorType);

            connections.advancedOracleConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedOracleConnection.connectionId);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedOracleConnection.connectionId);
            expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedOracleConnection.name);
            expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.configType);
            expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedOracleConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.advancedOracleConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.advancedOracleConnection.connectionId);

            // expect(connectionItem).to.be.a('object');
            // expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.advancedOracleConnection.connectionId);
            // expect(connectionItem).to.has.property('connectionConfig').to.be.a('object');
            // expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.advancedOracleConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            // expect(connectionItem.connectionConfig).to.has.propertyhostserver').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);

            // expect(connectionItem.connectionConfig).to.has.property('password').to.be.a('string').to.be.eq('******');
            // expect(connectionItem).to.has.property('configType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.configType);
            // expect(connectionItem.connectionConfig).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            // expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.advancedOracleConnection.connectorType);

            return Promise.resolve();
        });
    });

    describe('Connection-Invoke: Oracle connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.oracleConnection.name,
                host: CONNECTIONS.oracle.host,
                password: CONNECTIONS.oracle.password,
                user: CONNECTIONS.oracle.user,
                serviceName: CONNECTIONS.oracle.serviceName,
                connectorType: connections.oracleConnection.connectorType,
            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('serviceName').to.be.a('string').to.be.eq(CONNECTIONS.oracle.serviceName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            connections.oracleConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.oracleConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId, { invoke: true });
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId, { connectionData: { ...connectionItem } }, { invoke: true });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.oracleConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.oracleConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.oracleConnection.name);
            // expect(connectionItem).to.has.property('port').to.be.a('number').to.be.eq(CONNECTIONS.oracle.port);
            expect(connectionItem).to.has.property('host').to.be.a('string').to.be.eq(CONNECTIONS.oracle.host);
            expect(connectionItem).to.has.property('user').to.be.a('string').to.be.eq(CONNECTIONS.oracle.user);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.oracleConnection.connectorType);

            return Promise.resolve();
        });
    });

    // ATHENA connection

    describe('Connection-Basic: Athena connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.athenaConnection.name,
                accessKey: CONNECTIONS.athena.user,
                awsRegion: CONNECTIONS.athena.awsRegion,
                // catalogName: CONNECTIONS.athena.catalogName,
                connectorType: connections.athenaConnection.connectorType,
                hasHeader: true,

                secretKey: CONNECTIONS.athena.password,
                workGroup: CONNECTIONS.athena.workGroup,

                outputLocation: CONNECTIONS.athena.outputLocation,
                database: CONNECTIONS.athena.databases.general.name,
                table: CONNECTIONS.athena.databases.general.tables.client.tableName,
                tableKey: CONNECTIONS.athena.databases.general.tables.client.tableName,

            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails);

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.name);

            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            connections.athenaConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId);

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.name);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            // expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId);

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId);
            connectionItem.name = connections.athenaConnection.updatedName;
            connectionItem = await ConnectionService.updateConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, connectionItem);

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.updatedName);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            // expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId);
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { connectionData: { ...connectionItem } });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId);

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.updatedName);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });
    });

    describe('Connection-Invoke: Athena connection', function () {
        it('It should create a connection', async function () {
            let connectionDetails = {
                name: connections.athenaConnection.name,
                accessKey: CONNECTIONS.athena.user,
                awsRegion: CONNECTIONS.athena.awsRegion,
                catalogName: CONNECTIONS.athena.catalogName,
                connectorType: connections.athenaConnection.connectorType,
                hasHeader: true,

                secretKey: CONNECTIONS.athena.password,
                workGroup: CONNECTIONS.athena.workGroup,

                outputLocation: CONNECTIONS.athena.outputLocation,
                database: CONNECTIONS.athena.databases.general.name,
                table: CONNECTIONS.athena.databases.general.tables.client.tableName,
                tableKey: CONNECTIONS.athena.databases.general.tables.client.tableName,

            };

            let connectionItem = await ConnectionService.createConnection(GENERAL_USER_ID, applications.app1.appId, connectionDetails, { invoke: true });

            expect(connectionItem).to.be.a('object');
            expect(connectionItem).to.has.property('connectorid').to.be.a('string');
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.name);

            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            connections.athenaConnection.connectionId = connectionItem.connectorid;
            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.name);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            // expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });

        it('It should return a list of connections', async function () {
            let connectionListItem = await ConnectionService.getConnections(GENERAL_USER_ID, applications.app1.appId, null, { invoke: true });

            expect(connectionListItem).to.be.a('object');
            expect(connectionListItem).to.has.property('Count').to.be.a('number').to.be.eq(1);
            expect(connectionListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(1);

            return Promise.resolve();
        });

        it('It should get a connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { invoke: true });
            connectionItem.name = connections.athenaConnection.updatedName;
            connectionItem = await ConnectionService.updateConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, connectionItem, { invoke: true });

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.updatedName);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            // expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });

        it('It should test an existing connection', async function () {
            let connectionItem = await ConnectionService.getConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { invoke: true });
            let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { connectionData: { ...connectionItem } }, { invoke: true });

            expect(result).to.be.a('object');

            expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
            // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection established successfully');

            return Promise.resolve();
        });

        it('It should delete a connection', async function () {
            let connectionItem = await ConnectionService.deleteConnection(GENERAL_USER_ID, applications.app1.appId, connections.athenaConnection.connectionId, { invoke: true });

            expect(connectionItem).to.be.a('object');

            expect(connectionItem).to.has.property('connectorid').to.be.a('string').to.be.eq(connections.athenaConnection.connectionId);
            expect(connectionItem).to.has.property('name').to.be.a('string').to.be.eq(connections.athenaConnection.updatedName);
            expect(connectionItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(connectionItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(connectionItem).to.has.property('accessKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.user);
            expect(connectionItem).to.has.property('awsRegion').to.be.a('string').to.be.eq(CONNECTIONS.athena.awsRegion);
            // expect(connectionItem).to.has.property('catalogName').to.be.a('string').to.be.eq(CONNECTIONS.athena.catalogName);
            expect(connectionItem).to.has.property('connectorType').to.be.a('string').to.be.eq(connections.athenaConnection.connectorType);
            expect(connectionItem).to.has.property('hasHeader').to.be.a('boolean').to.be.eq(true);
            // expect(connectionItem).to.has.property('secretKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.password);
            expect(connectionItem).to.has.property('workGroup').to.be.a('string').to.be.eq(CONNECTIONS.athena.workGroup);
            expect(connectionItem).to.has.property('outputLocation').to.be.a('string').to.be.eq(CONNECTIONS.athena.outputLocation);
            expect(connectionItem).to.has.property('table').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);
            expect(connectionItem).to.has.property('tableKey').to.be.a('string').to.be.eq(CONNECTIONS.athena.databases.general.tables.client.tableName);

            return Promise.resolve();
        });
    });

    after(async function () {
        await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);
    });
});
