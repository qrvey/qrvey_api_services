'use strict';
const { expect } = require('chai');

const ApplicationService = require('../../../../lib/applicationService');
const {
    webFormsService: WebFormsService,
} = require('../../../../lib/preData');

const {
    GENERAL_USER_ID,
    WRONG_APP_ID,
    WRONG_USER_ID,
} = require('../../../config/data');



describe('Web form', function () {
    this.timeout(1000 * 6 * 10 * 15);

    let applications = {
        app1: {
            name: 'My app',
            appId: undefined,
        },
    };

    before(async function () {
        let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);
        applications.app1.appId = applicationItem.appid;
    });

    describe('Quiz: Creating basic quiz', function () {
        let qrveys = {
            quiz: {
                name: 'My quiz',
                description: 'Quiz description',
                qrveyId: null,
                questions: {
                    question1: {
                        answers: [
                            {
                                answer: 'OPTION 1',
                                answerid: 'OPTION_1'
                            },
                            {
                                answer: 'OPTION 2',
                                answerid: 'OPTION_2'
                            }
                        ],
                        text: 'Question 1',
                        type: 'YES_NO',
                        question: 'optional',
                        right_answer: 'OPTION 1',
                        incomplete: false,
                        visibility: 'visible',
                        score: 1,
                        questionIndex: 1
                    }
                }
            }
        };

        it('It should create a quiz', async function () {
            let qrveyItem = await WebFormsService.createWebForm(GENERAL_USER_ID, applications.app1.appId, {
                name: qrveys.quiz.name,
                description: qrveys.quiz.description,
                appType: 'QUIZ',
            });

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string');
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');
            qrveys.quiz.qrveyId = qrveyItem.qrveyid;
        });

        it('It should get an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');
        });

        it('It should update an existing quiz by qrveyId', async function () {
            let qrveyItem = await WebFormsService.getWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            qrveyItem.questions = {
                data: [
                    {...qrveys.quiz.questions.question1}
                ]
            };

            qrveyItem = await WebFormsService.updateWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId, {...qrveyItem});

            expect(qrveyItem).to.be.a('object');
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string').to.be.eq(qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('IN_PROGRESS');

            expect(qrveyItem).to.has.property('questions').to.be.a('object').to.has.property('data').to.be.a('array').to.has.lengthOf(1);
        });

        it('It should activate a quiz', async function () {
            let qrveyItem = await WebFormsService.activateWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            expect(qrveyItem).to.has.property('qrveyid').to.be.a('string');
            expect(qrveyItem).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(qrveyItem).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(qrveyItem).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(qrveyItem).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(qrveyItem).to.has.property('status').to.be.eq('RUNNING');
        });

        it('It should delete a quiz', async function () {
            let response = await WebFormsService.deleteWebForm(GENERAL_USER_ID, applications.app1.appId, qrveys.quiz.qrveyId);
            expect(response).to.be.a('object');
            expect(response.qrvey).to.be.a('object');
            expect(response.qrvey).to.has.property('qrveyid').to.be.a('string');
            expect(response.qrvey).to.has.property('appid').to.be.eq(applications.app1.appId);
            expect(response.qrvey).to.has.property('userid').to.be.eq(GENERAL_USER_ID);
            expect(response.qrvey).to.has.property('name').to.be.eq(qrveys.quiz.name);
            expect(response.qrvey).to.has.property('description').to.be.eq(qrveys.quiz.description);
            expect(response.qrvey).to.has.property('status').to.be.eq('RUNNING');
        });
    });

    after(async function () {
        await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);
    });
});
