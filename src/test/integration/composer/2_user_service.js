'use strict';
const { expect } = require('chai');

const UserService = require('../../../lib/userService');
const {
    GENERAL_API_KEY,
    GENERAL_USER_ID,
    WRONG_API_KEY,
    WRONG_USER_ID,
} = require('../../config/data');

describe('User service', function () {
    this.timeout(1000 * 6 * 10 * 15);
    describe('Get user service', function () {

        it('It should get an user', async function () {
            let userItem = await UserService.getUser(GENERAL_USER_ID);

            expect(userItem).to.be.a('object');
            expect(userItem).to.has.property('userid').be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(userItem).to.has.property('email').be.a('string');

            return Promise.resolve();
        });

        it('It should get an user using custom api key', async function () {
            let userItem = await UserService.getUser(GENERAL_USER_ID, { apiKey: GENERAL_API_KEY });

            expect(userItem).to.be.a('object');
            expect(userItem).to.has.property('userid').be.a('string').to.be.eq(GENERAL_USER_ID);
            expect(userItem).to.has.property('email').be.a('string');

            return Promise.resolve();
        });

        it('It should not get an user: Wrong user', async function () {
            return UserService.getUser(WRONG_USER_ID)
                .catch((e) => {
                    expect(e).to.be.a('object');
                    expect(e.errors).to.be.a('array').to.has.lengthOf(1);
                    return Promise.resolve();
                });
        });

        it('It should not get an user using custom api key', async function () {
            return UserService.getUser(GENERAL_USER_ID, { apiKey: WRONG_API_KEY })
                .then(() => {
                    return Promise.reject();
                })
                .catch((error) => {
                    expect(error).to.be.a('string').to.be.eq('Unauthorized');
                    return Promise.resolve();
                });
        });

    });
});