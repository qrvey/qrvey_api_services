'use strict';
const { expect } = require('chai');

const ApplicationService = require('../../../lib/applicationService');
const {
    GENERAL_USER_ID,
    WRONG_API_KEY,
    WRONG_APP_ID,
    WRONG_USER_ID,
} = require('../../config/data');

describe('Application service', function () {
    this.timeout(1000 * 6 * 10 * 15);

    describe('Get applications', function () {

        it('It should return a list of applications', async function () {
            let appListItem = await ApplicationService.getApplications(GENERAL_USER_ID);

            expect(appListItem).to.be.a('object');
            // expect(appListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            // expect(appListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should return a list of applications', async function () {
            let appListItem = await ApplicationService.getApplications(WRONG_USER_ID);
            expect(appListItem).to.be.a('object');
            // expect(appListItem).to.has.property('Count').to.be.a('number').to.be.eq(0);
            // expect(appListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(0);

            return Promise.resolve();
        });

        it('It should return a list of applications', async function () {
            return ApplicationService.getApplications(WRONG_USER_ID, null, { apiKey: WRONG_API_KEY })
                .then(() => {
                    return Promise.reject();
                })
                .catch((error) => {
                    expect(error).to.be.a('string').to.be.eq('Unauthorized');
                    return Promise.resolve();
                });
        });

    });

    describe('Application basic use case', function () {
        let applications = {
            app1: {
                name: 'My app',
                appId: undefined,
            },
            app2: {
                name: 'My app',
                description: 'App description',
                appId: undefined,
            },
        };

        it('It should create an application', async function () {
            let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app1);

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.not.has.property('description');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(applications.app1.name);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            applications.app1.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should create an application with description', async function () {

            let applicationItem = await ApplicationService.createApplication(GENERAL_USER_ID, applications.app2);

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('appid').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(applications.app2.name);
            expect(applicationItem).to.has.property('description').to.be.a('string').to.be.eq(applications.app2.description);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            applications.app2.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should get an application with description', async function () {

            let applicationItem = await ApplicationService.getApplication(GENERAL_USER_ID, applications.app2.appId);

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(applications.app2.name);
            expect(applicationItem).to.has.property('description').to.be.a('string').to.be.eq(applications.app2.description);
            expect(applicationItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app2.appId);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            applications.app2.appId = applicationItem.appid;

            return Promise.resolve();
        });

        it('It should not get an application', async function () {
            return Promise.resolve();

            // return ApplicationService.getApplication(GENERAL_USER_ID, WRONG_APP_ID)
            //     .then((data) => {
            //         console.log('APP_ID', data);
            //         return Promise.reject();
            //     })
            //     .catch((error) => {
            //         expect(error).to.be.a('object');
            //         expect(error).to.has.property('errors').to.be.a('array').to.has.lengthOf(1);
            //         expect(error.errors[0]).to.has.property('message').to.be.a('string').to.be.eq('Application not found');
            //         return Promise.resolve();
            //     });
        });

        it('It should return a list of applications', async function () {
            let appListItem = await ApplicationService.getApplications(GENERAL_USER_ID);

            expect(appListItem).to.be.a('object');
            // expect(appListItem).to.has.property('Count').to.be.a('number').to.be.eq(2);
            // expect(appListItem).to.has.property('Items').to.be.a('array').to.has.lengthOf(2);

            return Promise.resolve();
        });

        it('It should delete an application', async function () {
            let applicationItem = await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app1.appId);

            expect(applicationItem).to.be.a('object');
            expect(applicationItem).to.not.has.property('description');
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(applications.app1.name);
            expect(applicationItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app1.appId);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);

            return Promise.resolve();
        });

        it('It should delete an application with description', async function () {

            let applicationItem = await ApplicationService.deleteApplication(GENERAL_USER_ID, applications.app2.appId);
            expect(applicationItem).to.has.property('endUserLink').to.be.a('string');
            expect(applicationItem).to.has.property('name').to.be.a('string').to.be.eq(applications.app2.name);
            expect(applicationItem).to.has.property('description').to.be.a('string').to.be.eq(applications.app2.description);
            expect(applicationItem).to.has.property('appid').to.be.a('string').to.be.eq(applications.app2.appId);
            expect(applicationItem).to.has.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
            
            expect(applicationItem).to.be.a('object');

            return Promise.resolve();
        });

    });
});