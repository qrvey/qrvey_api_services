'use strict';
const { expect } = require('chai');

const {
    authentication: Authentication
} = require('../../../lib/adminCenter');

const {
    ADMIN_BACKEND_DOMAIN,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
} = require('../../config/data');

describe('Admin center', function () {
    this.timeout(1000 * 6 * 10 * 15);
    describe('Authentication', function () {

        it('It should return a JWT if valid credentials', async function () {
            let response = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);

            expect(response).to.be.a('object');
            expect(response).to.has.property('status').be.a('boolean').to.be.eq(true);
            expect(response).to.has.property('token').be.a('string');

            return Promise.resolve();
        });

        it('It should return a JWT if valid credentials', async function () {
            let response = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD, {
                domain: ADMIN_BACKEND_DOMAIN
            });

            expect(response).to.be.a('object');
            expect(response).to.has.property('status').be.a('boolean').to.be.eq(true);
            expect(response).to.has.property('token').be.a('string');

            return Promise.resolve();
        });

        it('It should not return the authentication token if missing credentials', async function () {

            let response = await Authentication.login();

            expect(response).to.be.a('object');
            expect(response).to.has.property('status').be.a('boolean').to.be.eq(false);
            expect(response).to.not.has.property('token').be.a('string');
        });

        it('It should not return the authentication token if wrong credentials', async function () {

            let response = await Authentication.login('WRONG', 'WRONG');

            expect(response).to.be.a('object');
            expect(response).to.has.property('status').be.a('boolean').to.be.eq(false);
            expect(response).to.not.has.property('token').be.a('string');
        });

    });
});