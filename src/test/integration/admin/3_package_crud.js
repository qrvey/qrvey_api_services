'use strict';
const { expect } = require('chai');
const { DateTime } = require('luxon');

const {
    authentication: Authentication,
} = require('../../../lib/adminCenter');

const CommonHelper = require('../../../lib/helpers/commonHelper');

const {
    packageService: PackageService,
    packageVersionService: PackageVersionService,
} = require('../../../lib/contentDeployment');

const UserService = require('../../../lib/userService');


const AdminHelper = require('../../helpers/adminHelper');
const ComposerHelper = require('../../helpers/composerHelper');

const {
    GENERAL_USER_ID,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    SERVERS,
} = require('../../config/data');

describe('Content deployment: Packages', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    /** @type {AdminHelper} */
    let token = undefined; // Authorization token
    let serverId, composerHelperItem, userEmail;

    let appName = 'Basic load process';

    before(async function () {
        let authenticationResponse = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);
        token = `jwt ${authenticationResponse.token}`;

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
        };

        let adminItem = await AdminHelper.findOrCreateServer(serverDetails, {
            authorization: token,
        });

        serverId = adminItem.serverId;

        let userItem = await UserService.getUser(GENERAL_USER_ID);
        userEmail = userItem.email;

        composerHelperItem = await ComposerHelper.createBasicApplication(GENERAL_USER_ID, { name: appName });
        expect(composerHelperItem).to.be.a('object');
        expect(composerHelperItem).to.has.property('userId').to.be.a('string').to.be.eq(GENERAL_USER_ID);
        expect(composerHelperItem).to.has.property('appId').to.be.a('string');

    });

    describe('Package service CRUD', function () {

        let packageVersionJobTrackerId;
        let packages = {
            basicAppPackage: {
                packageId: undefined,
                name: `Basic app package: ${now.toMillis()}`,
                description: 'Package description',
                status: 'DRAFT',
            },
        };

        it('It should create a server', async function () {
            let packageDetails = {
                name: packages.basicAppPackage.name,
            };

            let packageItem = await PackageService.createPackage(packageDetails, {
                authorization: token,
            });

            expect(packageItem).to.be.a('object');
            expect(packageItem).to.has.property('packageId').be.a('string');
            expect(packageItem).to.has.property('name').be.a('string').to.be.eq(packages.basicAppPackage.name);
            expect(packageItem).to.has.property('status').be.a('string').to.be.eq(packages.basicAppPackage.status);

            packages.basicAppPackage.packageId = packageItem.packageId;
            return Promise.resolve();
        });

        it('It should update a package', async function () {
            let packageDetails = {
                appName,
                packageId: packages.basicAppPackage.packageId,
                name: packages.basicAppPackage.name,
                description: packages.basicAppPackage.description,
                status: packages.basicAppPackage.status,
                adminServerId: serverId,
                applicationId: composerHelperItem.appId,
                userId: composerHelperItem.userId,
                email: userEmail,
            };

            let packageItem = await PackageService.updatePackage(packages.basicAppPackage.packageId, packageDetails, {
                authorization: token,
            });

            expect(packageItem).to.be.a('object');
            expect(packageItem).to.has.property('description').be.a('string').to.be.eq(packages.basicAppPackage.description);
            expect(packageItem).to.has.property('name').be.a('string').to.be.eq(packages.basicAppPackage.name);
            expect(packageItem).to.has.property('packageId').be.a('string').to.be.eq(packages.basicAppPackage.packageId);
            expect(packageItem).to.has.property('status').be.a('string').to.be.eq(packages.basicAppPackage.status);

            return Promise.resolve();
        });

        it('It should get a package', async function () {

            let [packageItem] = await PackageService.getPackage(packages.basicAppPackage.packageId, {
                authorization: token,
            });

            expect(packageItem).to.be.a('object');
            expect(packageItem).to.has.property('description').be.a('string').to.be.eq(packages.basicAppPackage.description);
            expect(packageItem).to.has.property('name').be.a('string').to.be.eq(packages.basicAppPackage.name);
            expect(packageItem).to.has.property('packageId').be.a('string').to.be.eq(packages.basicAppPackage.packageId);
            expect(packageItem).to.has.property('status').be.a('string').to.be.eq(packages.basicAppPackage.status);

            return Promise.resolve();
        });

        it('It should create a package version', async function () {
            let packageVersionDetails = {
                name: 'v1',
            };

            let packageVersionItem = await PackageVersionService.createPackageVersion(packages.basicAppPackage.packageId, packageVersionDetails, {
                authorization: token,
            });

            expect(packageVersionItem).to.be.a('object');
            expect(packageVersionItem).to.has.property('jobTrackerId').be.a('string');
            expect(packageVersionItem).to.has.property('message').be.a('string').to.be.eq('When The version has been created, It will be to see  in the version list.');
            packageVersionJobTrackerId = packageVersionItem.jobTrackerId;
            return Promise.resolve();
        });

        it('It should wait until package version gets created', async function () {

            let stopProcess = false, count = 0;

            while (stopProcess !== true) {

                let loadDatasetTrackerItem = await PackageVersionService.getPackageVersionJobByJobId(packageVersionJobTrackerId, {
                    authorization: token,
                });

                if (loadDatasetTrackerItem.status === 'COMPLETED' || loadDatasetTrackerItem.status === 'FAILED') {
                    stopProcess = true;
                }

                if (count === 18) {
                    stopProcess = true;
                }

                count++;
                await CommonHelper.sleep(3000);
            }

            let loadDatasetTrackerItem = await PackageVersionService.getPackageVersionJobByJobId(packageVersionJobTrackerId, {
                authorization: token,
            });

            expect(loadDatasetTrackerItem).to.be.a('object');
            expect(loadDatasetTrackerItem).to.has.property('jobTrackerId').be.a('string').to.be.eq(packageVersionJobTrackerId);
            expect(loadDatasetTrackerItem).to.has.property('jobType').be.a('string').to.be.eq('EXPORT_PACKAGE');
            expect(loadDatasetTrackerItem).to.has.property('status').be.a('string').to.be.eq('COMPLETED');
            expect(loadDatasetTrackerItem).to.has.property('parameters').be.a('object');
            expect(loadDatasetTrackerItem.parameters).to.has.property('packageId').be.a('string').to.be.eq(packages.basicAppPackage.packageId);
            expect(loadDatasetTrackerItem.parameters).to.has.property('applicationId').be.a('string').to.be.eq(composerHelperItem.appId);
            expect(loadDatasetTrackerItem.parameters).to.has.property('userId').be.a('string').to.be.eq(composerHelperItem.userId);
            return Promise.resolve();
        });

        it('It should get a list of versions', async function () {
            let body = {
                limit: 10,
            };

            let result = await PackageVersionService.getPackageVersionList(packages.basicAppPackage.packageId, body, {
                authorization: token,
            });

            expect(result).to.be.a('object');
            expect(result).to.has.property('items').be.a('array').to.have.lengthOf(1);
            expect(result.items[0]).to.has.property('name').be.a('string').to.be.eq('v1');
            expect(result.items[0]).to.has.property('packageVersionId').be.a('string');
            // packageVersionId = result.items[0].packageVersionId;
            return Promise.resolve();
        });

        it('It should delete a server', async function () {
            let packageItem = await PackageService.deletePackage(packages.basicAppPackage.packageId, {
                authorization: token,
            });

            expect(packageItem).to.be.a('object');
            expect(packageItem).to.has.property('message').to.be.a('string').to.be.eq('Deleted succesfully');

            return Promise.resolve();
        });
    });

    after(async function() {
        await composerHelperItem.deleteAll();
    });
});