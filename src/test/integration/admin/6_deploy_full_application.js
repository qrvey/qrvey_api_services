'use strict';
const { expect } = require('chai');
const { DateTime } = require('luxon');
const { countBy: _countBy} = require('lodash');

const CommonHelper = require('../../../lib/helpers/commonHelper');
const DatasetHelper = require('../../helpers/datasetHelper');

const {
    authentication: Authentication,
} = require('../../../lib/adminCenter');

const {
    deploymentJobService: DeploymentJobService,
    deploymentJobBlockService: DeploymentJobBlockService,
} = require('../../../lib/contentDeployment');

const {
    connectionService: ConnectionService,
    datasetService: DatasetService,
} = require('../../../lib/preData');

const {
    applicationService: ApplicationService,
} = require('../../../lib');

const AdminHelper = require('../../helpers/adminHelper');
const UserService = require('../../../lib/userService');

const {
    GENERAL_USER_ID,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    SERVERS,
} = require('../../config/data');

describe('Content deployment: Full deployment', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    /** @type {AdminHelper} */
    let adminItem;
    let token, params, jobs;

    before(async function () {
        let authenticationResponse = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);
        token = `jwt ${authenticationResponse.token}`;

        params = {
            authorization: token,
        };

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
        };

        let definitionDetails = {
            userId: GENERAL_USER_ID,
            name: `CD2.0 full app ${now.toMillis()}`,
            description: 'CD2.0 full app',
        };

        adminItem = await AdminHelper.createFullDefinition(serverDetails, definitionDetails, params);

        let userItem = await UserService.getUser(GENERAL_USER_ID);
        const userEmail = userItem.email;

        jobs = {
            basicJob: {
                name: `Basic definition: ${now.toMillis()}`,
                description: 'Deployment job description',
                deploymentJobId: undefined,
                deploymentJobBlockId: undefined,
                packageId: adminItem.packageId,
                packageVersionId: adminItem.packageVersionId,
                definitionId: adminItem.definitionId,
                serverId: adminItem.serverId,
                jobTrackerId: null,
                users: [
                    {
                        email: userEmail,
                        userid: GENERAL_USER_ID,
                        parameters: [],
                    }
                ]
            },
        };

        return Promise.resolve();
    });

    describe('Testing full application', function () {

        let targetAppId;

        describe('Creating, packing and deploying application', function() {
            it('It should create a job definition', async function () {
                let jobDetails = {
                    name: jobs.basicJob.name,
                    description: jobs.basicJob.description,
                };
    
                let jobItem = await DeploymentJobService.createDeploymentJob(jobDetails, params);
    
                expect(jobItem).to.be.a('object');
                expect(jobItem).to.has.property('deploymentJobId').be.a('string');
    
    
                jobs.basicJob.deploymentJobId = jobItem.deploymentJobId;
                return Promise.resolve();
            });
            
            it('It should create a job definition', async function () {
    
                let blockItem = await DeploymentJobBlockService.createDeploymentJobBlock(jobs.basicJob.deploymentJobId, null, params);
    
                expect(blockItem).to.be.a('object');
                expect(blockItem).to.has.property('deploymentJobId').be.a('string');
                expect(blockItem).to.has.property('deploymentJobBlockId').be.a('string');
    
    
                jobs.basicJob.deploymentJobId = blockItem.deploymentJobId;
                jobs.basicJob.deploymentJobBlockId = blockItem.deploymentJobBlockId;
                return Promise.resolve();
            });
            
            it('It should update a deployment block job', async function () {
    
                let blockDetails = {
                    name: 'Block 1',
                    adminserverid: jobs.basicJob.serverId,
                    adminserverHost: SERVERS.contentDeployment1.domain,
                    adminserverApiKey: SERVERS.contentDeployment1.apiKey,
                    definitionId: jobs.basicJob.definitionId,
                    selectAllUsers: false,
                };
    
                let blockItem = await DeploymentJobBlockService.updateDeploymentJobBlock(jobs.basicJob.deploymentJobId, jobs.basicJob.deploymentJobBlockId, blockDetails, params);
    
                expect(blockItem).to.be.a('object');
                expect(blockItem).to.has.property('createDate').be.a('string');
                expect(blockItem).to.has.property('updateDate').be.a('string');
                expect(blockItem).to.has.property('selectAllUsers').be.a('boolean').to.be.eq(false);
                expect(blockItem).to.has.property('deploymentJobId').be.a('string').to.be.eq(jobs.basicJob.deploymentJobId);
                expect(blockItem).to.has.property('deploymentJobBlockId').be.a('string').to.be.eq(jobs.basicJob.deploymentJobBlockId);
                expect(blockItem).to.has.property('adminserverid').be.a('string').to.be.eq(jobs.basicJob.serverId);
                // expect(blockItem).to.has.property('adminserverHost').be.a('string').to.be.eq(SERVERS.contentDeployment1.domain);
                // expect(blockItem).to.has.property('adminserverApiKey').be.a('string').to.be.eq(SERVERS.contentDeployment1.apiKey);
    
    
                jobs.basicJob.deploymentJobId = blockItem.deploymentJobId;
                jobs.basicJob.deploymentJobBlockId = blockItem.deploymentJobBlockId;
                return Promise.resolve();
            });
    
            it('It should create a job definition', async function () {
    
                let recipients = {
                    users: [...jobs.basicJob.users]
                };
    
                let blockItem = await DeploymentJobBlockService.updateRecipientJobBlockData(jobs.basicJob.deploymentJobId, jobs.basicJob.deploymentJobBlockId, recipients, params);
    
                expect(blockItem).to.be.a('object');
                // expect(blockItem).to.has.property('deploymentJobId').be.a('string');
                // expect(blockItem).to.has.property('deploymentJobBlockId').be.a('string');
    
    
                // jobs.basicJob.deploymentJobId = blockItem.deploymentJobId;
                // jobs.basicJob.deploymentJobBlockId = blockItem.deploymentJobBlockId;
                return Promise.resolve();
            });
    
            it('It should update a deployment job', async function () {
    
                let jobResult = await DeploymentJobService.getDeploymentJob(jobs.basicJob.deploymentJobId, params);
                let jobItem = jobResult[0];
                jobItem = await DeploymentJobService.updateDeploymentJob(jobs.basicJob.deploymentJobId, {
                    ...jobItem,
                    status: 'COMPLETE',
                    lastRun: 'NEVER',
                }, params);
    
                expect(jobItem).to.be.a('object');
                // expect(jobItem).to.has.property('message').be.a('string').to.be.eq('Deleted succesfully');
    
                return Promise.resolve();
            });
    
            it('It should execute a deployment job', async function () {
    
                let jobTrackerData = await DeploymentJobService.executeDeploymentJob(jobs.basicJob.deploymentJobId, null, params);
    
                expect(jobTrackerData).to.be.a('object');
                expect(jobTrackerData).to.has.property('message').be.a('string').to.be.eq('When the deploy is going finish, Its status will be to update.');
                expect(jobTrackerData).to.has.property('jobTrackerId').be.a('string');
    
                jobs.basicJob.jobTrackerId = jobTrackerData.jobTrackerId;
    
                return Promise.resolve();
            });
    
            it('It should update a deployment job and assign the jobTrackerId', async function () {
    
                let jobResult = await DeploymentJobService.getDeploymentJob(jobs.basicJob.deploymentJobId, params);
                let jobItem = jobResult[0];
                jobItem = await DeploymentJobService.updateDeploymentJob(jobs.basicJob.deploymentJobId, {
                    ...jobItem,
                    jobTrackerId: jobs.basicJob.jobTrackerId,
                    status: 'DEPLOYING',
                }, params);
    
                expect(jobItem).to.be.a('object');
                // expect(jobItem).to.has.property('message').be.a('string').to.be.eq('Deleted succesfully');
    
                return Promise.resolve();
            });
            
            it('It should execute a deployment job', async function () {
    
                let stopProcess = false, count = 0;
    
                while (stopProcess !== true) {
    
                    let trackerItem = await DeploymentJobService.getDeploymentJobExecutionProgressByJobTrackerId(jobs.basicJob.jobTrackerId, params);
    
                    if (trackerItem.status === 'COMPLETED' || trackerItem.status === 'FAILED') {
                        stopProcess = true;
                    }
    
                    if (count === 20 * 15) {
                        stopProcess = true;
                    }
    
                    count++;
                    await CommonHelper.sleep(3000);
                }
    
                let trackerItem = await DeploymentJobService.getDeploymentJobExecutionProgressByJobTrackerId(jobs.basicJob.jobTrackerId, params);
                expect(trackerItem).to.be.a('object');
                expect(trackerItem).to.has.property('jobTrackerId').be.a('string').to.be.eq(jobs.basicJob.jobTrackerId);
                expect(trackerItem).to.has.property('status').be.a('string').to.be.eq('COMPLETED');
                return Promise.resolve();
            });
        });
        
        describe('Validating deployment', function() {
            it('It should find the installed application', async function() {
                let query = {
                    'limit': 1,
                    'filters': [{
                        'filterType': 'EQUAL',
                        'column': 'name',
                        'value': adminItem.appName,
                    }]
                };
                let appList = await ApplicationService.getApplications(GENERAL_USER_ID, query);
                expect(appList).to.be.a('object');
                expect(appList).to.have.property('Count').to.be.a('number').to.be.equal(1);
                expect(appList).to.have.property('Items').to.be.a('array').to.has.lengthOf(1);
                expect(appList.Items[0]).to.be.a('object');
                expect(appList.Items[0]).to.have.property('appid').to.be.a('string');
                expect(appList.Items[0]).to.have.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
                
                targetAppId = appList.Items[0].appid;
                return Promise.resolve();
            });

            it('It should get the installed application', async function() {
                let appIem = await ApplicationService.getApplication(GENERAL_USER_ID, targetAppId);

                expect(appIem).to.have.property('appid').to.be.a('string');
                expect(appIem).to.have.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
                expect(appIem).to.have.property('name').to.be.a('string').to.be.eq(adminItem.appName);

                return Promise.resolve();
            });

            it('It should validate the installed connections', async function() {
                let connectionList = await ConnectionService.getConnections(GENERAL_USER_ID, targetAppId, {loadAll: true});

                expect(connectionList).to.be.a('object');
                expect(connectionList).to.have.property('Count').to.be.a('number').to.be.equal(7);
                expect(connectionList).to.have.property('Items').to.be.a('array').to.has.lengthOf(7);

                let countSummary = _countBy(connectionList.Items, 'connectorType');
                expect(countSummary).to.be.a('object');
                expect(countSummary).to.have.property('MYSQL_LIVE').to.be.a('number').to.be.equal(2);
                expect(countSummary).to.have.property('ATHENA').to.be.a('number').to.be.equal(1);
                expect(countSummary).to.have.property('MS_SQL_LIVE').to.be.a('number').to.be.equal(2);
                expect(countSummary).to.have.property('ORACLE_LIVE').to.be.a('number').to.be.equal(2);

                for (let connectionItem of connectionList.Items) {
                    let connectionDetails = await ConnectionService.getConnection(GENERAL_USER_ID, targetAppId, connectionItem.connectorid);
                    let result = await ConnectionService.testConnectionByConnectionId(GENERAL_USER_ID, targetAppId, connectionItem.connectorid, { connectionData: { ...connectionDetails } });
        
                    expect(result).to.be.a('object');
        
                    expect(result).to.has.property('status').to.be.a('boolean').to.be.eq(true);
                    // expect(result).to.has.property('message').to.be.a('string').to.be.eq('Connection test was successful.');
                }


                return Promise.resolve();
            });

            it('It should validate the the installed datasets', async function() {
                let datasetList = await DatasetService.getDatasets(GENERAL_USER_ID, targetAppId, {loadAll: true});

                expect(datasetList).to.be.a('object');
                expect(datasetList).to.have.property('Count').to.be.a('number').to.be.equal(7);
                expect(datasetList).to.have.property('Items').to.be.a('array').to.has.lengthOf(7);

                for (let datasetItem of datasetList.Items) {
                    let result = await DatasetHelper.loadDataset(GENERAL_USER_ID, targetAppId, datasetItem.datasetId, {wait: true});
        
                    expect(result).to.be.a('object');
                    expect(result).to.has.property('status').to.be.a('string').to.be.eq('LOADED');
                }

                return Promise.resolve();
            });
        });

        describe('Cleaning all up', function () {
            it('It should delete the installed application', async function() {
                let appItem = await ApplicationService.deleteApplication(GENERAL_USER_ID, targetAppId);
                expect(appItem).to.be.a('object');
                expect(appItem).to.have.property('appid').to.be.a('string').to.be.eq(targetAppId);
                expect(appItem).to.have.property('userid').to.be.a('string').to.be.eq(GENERAL_USER_ID);
                return Promise.resolve();
            });
    
            it('It should delete a deployment block job', async function () {
    
                let blockItem = await DeploymentJobBlockService.deleteDeploymentJobBlock(jobs.basicJob.deploymentJobId, jobs.basicJob.deploymentJobBlockId, params);
    
                expect(blockItem).to.be.a('object');
                expect(blockItem).to.has.property('message').be.a('string').to.be.eq('Deleted succesfully');
    
                return Promise.resolve();
            });
    
            it('It should delete a deployment job', async function () {
    
                let jobItem = await DeploymentJobService.deleteDeploymentJob(jobs.basicJob.deploymentJobId, params);
    
                expect(jobItem).to.be.a('object');
                expect(jobItem).to.has.property('message').be.a('string').to.be.eq('Deleted succesfully');
    
                return Promise.resolve();
            });
        });
        
    });

    after(async function () {
        await adminItem.deleteAll(params);
        return Promise.resolve();
    });
});