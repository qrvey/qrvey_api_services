'use strict';
const { expect } = require('chai');
const { DateTime } = require('luxon');

const {
    authentication: Authentication,
} = require('../../../lib/adminCenter');

const {
    definitionService: DefinitionService,
    packageVersionService: PackageVersionService,
} = require('../../../lib/contentDeployment');

const AdminHelper = require('../../helpers/adminHelper');
const DefinitionHelper = require('../../helpers/definitionHelper');

const {
    GENERAL_USER_ID,
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    SERVERS,
} = require('../../config/data');

describe('Content deployment: Packages', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    /** @type {AdminHelper} */
    let adminItem;
    let token, params, definitions;

    let appName = `CD2.0 basic app ${now.toMillis()}`;
    const TAG_APP_NAME = `TG - ${appName}`;

    before(async function () {
        let authenticationResponse = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);
        token = `jwt ${authenticationResponse.token}`;

        params = {
            authorization: token,
        };

        let serverDetails = {
            apiKey: SERVERS.general.apiKey,
            host: SERVERS.general.domain,
            name: SERVERS.general.name,
        };

        let packageDetails = {
            userId: GENERAL_USER_ID,
            appName,
            packageName: `PK - ${appName}`,
        };

        adminItem = await AdminHelper.createBasicPackage(serverDetails, packageDetails, params);

        definitions = {
            basicDefinition: {
                definitionId: undefined,
                name: `Basic definition: ${now.toMillis()}`,
                description: 'Definition description',
                appName: TAG_APP_NAME,
                recipientApp: 'NEW',
                packageId: adminItem.packageId,
                packageVersionId: adminItem.packageVersionId,
                definitionItem: null,
            },
        };

        return Promise.resolve();
    });

    describe('Definition service New application CRUD', function () {

        it('It should create a deployment definition', async function () {
            let definitionDetails = {
                name: definitions.basicDefinition.name,
                description: definitions.basicDefinition.description,
            };

            let definitionItem = await DefinitionService.createDefinition(definitionDetails, params);

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('definitionId').be.a('string');
            expect(definitionItem).to.has.property('clientId').be.a('string').to.be.eq('default');
            expect(definitionItem).to.has.property('name').be.a('string').to.be.eq(definitions.basicDefinition.name);
            expect(definitionItem).to.has.property('nameLower').be.a('string').to.be.eq(definitions.basicDefinition.name.toLowerCase());
            expect(definitionItem).to.has.property('description').be.a('string').to.be.eq(definitions.basicDefinition.description);
            // expect(definitionItem).to.has.property('usePackageAppName').be.a('string').to.be.eq(false);
            // expect(definitionItem).to.has.property('nameFilter').be.a('string').to.be.eq(`${definitions.basicDefinition.name.toLowerCase()} ${definitions.basicDefinition.description.toLowerCase()}`);
            expect(definitionItem).to.has.property('updateDate').be.a('string');
            expect(definitionItem).to.has.property('createDate').be.a('string');

            definitions.basicDefinition.definitionId = definitionItem.definitionId;
            definitions.basicDefinition.definitionItem = { ...definitionItem };
            return Promise.resolve();
        });

        it('It should not update a deployment definition if missing packageVersionId', async function () {
            let definitionDetails = {
                ...definitions.basicDefinition.definitionItem,
                appName: TAG_APP_NAME,
                recipientApp: definitions.basicDefinition.recipientApp,
                packageId: adminItem.packageId,
            };

            try {
                await DefinitionService.updateDefinition(definitions.basicDefinition.definitionId, definitionDetails, params);
                return Promise.reject('Expected to throw an error');
            } catch (error) {
                expect(error).to.be.a('object');
            }
            return Promise.resolve();
        });

        it('It should get a deployment definition', async function () {

            let result = await DefinitionService.getDefinition(definitions.basicDefinition.definitionId, params);

            expect(result).to.be.a('array').to.have.lengthOf(1);
            let definitionItem = result[0];

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('createDate').be.a('string');
            expect(definitionItem).to.has.property('updateDate').be.a('string');

            expect(definitionItem).to.has.property('clientId').be.a('string').to.be.eq('default');
            expect(definitionItem).to.has.property('definitionId').be.a('string').to.be.eq(definitions.basicDefinition.definitionId);
            expect(definitionItem).to.has.property('description').be.a('string').to.be.eq(definitions.basicDefinition.description);
            expect(definitionItem).to.has.property('nameLower').be.a('string').to.be.eq(definitions.basicDefinition.name.toLowerCase());
            expect(definitionItem).to.has.property('name').be.a('string').to.be.eq(definitions.basicDefinition.name);



            definitions.basicDefinition.definitionItem = { ...definitionItem };
            return Promise.resolve();
        });

        it('It should update a deployment definition', async function () {
            let definitionDetails = {
                ...definitions.basicDefinition.definitionItem,
                appName: TAG_APP_NAME,
                recipientApp: 'NEW',
                packageId: adminItem.packageId,
                packageVersionId: adminItem.packageVersionId,
            };

            let definitionItem = await DefinitionService.updateDefinition(definitions.basicDefinition.definitionId, definitionDetails, params);

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('createDate').be.a('string');
            // expect(definitionItem).to.has.property('nameFilter');
            expect(definitionItem).to.has.property('updateDate').be.a('string');

            expect(definitionItem).to.has.property('clientId').be.a('string').to.be.eq('default');
            expect(definitionItem).to.has.property('usePackageAppName').be.a('boolean').to.be.eq(false);

            expect(definitionItem).to.has.property('appName').be.a('string').to.be.eq(TAG_APP_NAME);
            expect(definitionItem).to.has.property('definitionId').be.a('string').to.be.eq(definitions.basicDefinition.definitionId);
            expect(definitionItem).to.has.property('description').be.a('string').to.be.eq(definitions.basicDefinition.description);
            expect(definitionItem).to.has.property('nameLower').be.a('string').to.be.eq(definitions.basicDefinition.name.toLowerCase());
            expect(definitionItem).to.has.property('name').be.a('string').to.be.eq(definitions.basicDefinition.name);
            expect(definitionItem).to.has.property('packageVersionId').be.a('string').to.be.eq(adminItem.packageVersionId);
            expect(definitionItem).to.has.property('packageId').be.a('string').to.be.eq(adminItem.packageId);
            expect(definitionItem).to.has.property('recipientApp').be.a('string').to.be.eq(definitions.basicDefinition.recipientApp);

            definitions.basicDefinition.definitionItem = { ...definitionItem };

            return Promise.resolve();
        });

        it('It should add the dependencies', async function () {
            
            let result = await DefinitionService.getDefinition(definitions.basicDefinition.definitionId, params);
            expect(result).to.be.a('array').to.have.lengthOf(1);
            let definitionItem = result[0];

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('createDate').be.a('string');
            expect(definitionItem).to.has.property('nameFilter');
            expect(definitionItem).to.has.property('updateDate').be.a('string');

            expect(definitionItem).to.has.property('clientId').be.a('string').to.be.eq('default');
            expect(definitionItem).to.has.property('usePackageAppName').be.a('boolean').to.be.eq(false);

            expect(definitionItem).to.has.property('appName').be.a('string').to.be.eq(TAG_APP_NAME);
            expect(definitionItem).to.has.property('definitionId').be.a('string').to.be.eq(definitions.basicDefinition.definitionId);
            expect(definitionItem).to.has.property('description').be.a('string').to.be.eq(definitions.basicDefinition.description);
            expect(definitionItem).to.has.property('nameLower').be.a('string').to.be.eq(definitions.basicDefinition.name.toLowerCase());
            expect(definitionItem).to.has.property('name').be.a('string').to.be.eq(definitions.basicDefinition.name);
            expect(definitionItem).to.has.property('packageVersionId').be.a('string').to.be.eq(adminItem.packageVersionId);
            expect(definitionItem).to.has.property('packageId').be.a('string').to.be.eq(adminItem.packageId);
            expect(definitionItem).to.has.property('recipientApp').be.a('string').to.be.eq(definitions.basicDefinition.recipientApp);

            let packageVersionResult = await PackageVersionService.getPackageVersion(adminItem.packageId, adminItem.packageVersionId, params);
            
            expect(packageVersionResult).to.be.a('array').to.have.lengthOf(1);
            let packageVersionItem = packageVersionResult[0];

            let contentPackageVersionSelected = DefinitionHelper.mapAssetsByPackageVersionItem(packageVersionItem);

            await DefinitionService.updateDefinition(definitions.basicDefinition.definitionId, { ...definitionItem, contentPackageVersionSelected }, params);

            result = await DefinitionService.getDefinition(definitions.basicDefinition.definitionId, params);
            expect(result).to.be.a('array').to.have.lengthOf(1);
            definitionItem = result[0];

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('createDate').be.a('string');
            expect(definitionItem).to.has.property('nameFilter');
            expect(definitionItem).to.has.property('updateDate').be.a('string');

            expect(definitionItem).to.has.property('clientId').be.a('string').to.be.eq('default');
            expect(definitionItem).to.has.property('usePackageAppName').be.a('boolean').to.be.eq(false);

            expect(definitionItem).to.has.property('appName').be.a('string').to.be.eq(TAG_APP_NAME);
            expect(definitionItem).to.has.property('definitionId').be.a('string').to.be.eq(definitions.basicDefinition.definitionId);
            expect(definitionItem).to.has.property('description').be.a('string').to.be.eq(definitions.basicDefinition.description);
            expect(definitionItem).to.has.property('nameLower').be.a('string').to.be.eq(definitions.basicDefinition.name.toLowerCase());
            expect(definitionItem).to.has.property('name').be.a('string').to.be.eq(definitions.basicDefinition.name);
            expect(definitionItem).to.has.property('packageVersionId').be.a('string').to.be.eq(adminItem.packageVersionId);
            expect(definitionItem).to.has.property('packageId').be.a('string').to.be.eq(adminItem.packageId);
            expect(definitionItem).to.has.property('recipientApp').be.a('string').to.be.eq(definitions.basicDefinition.recipientApp);

            expect(definitionItem).to.has.property('contentPackageVersionSelected').be.a('object');
            expect(definitionItem.contentPackageVersionSelected).to.be.have.property('connections').to.be.a('array').to.have.lengthOf(4);

            for (let connectionItem of definitionItem.contentPackageVersionSelected.connections) {
                expect(connectionItem).to.be.a('object');
                expect(connectionItem).to.have.property('_type').to.be.a('string').to.be.eq('connection');
                expect(connectionItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(packageVersionItem.contentPackageVersion.connections.map(item => item.connectionId));
                expect(connectionItem).to.have.property('connectionClassType').to.be.a('string').to.be.eq('database');
                expect(connectionItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(connectionItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
            }

            expect(definitionItem.contentPackageVersionSelected).to.be.have.property('datasets').to.be.a('array').to.have.lengthOf(5);
            for (let datasetItem of contentPackageVersionSelected.datasets) {
                expect(datasetItem).to.be.a('object');
                expect(datasetItem).to.have.property('_type').to.be.a('string').to.be.eq('dataset');
                expect(datasetItem).to.have.property('_assetId').to.be.a('string').to.be.oneOf(packageVersionItem.contentPackageVersion.datasets.map(item => item.datasetId));
                expect(datasetItem).to.have.property('datasetType').to.be.a('string').to.be.eq('DATASET');
                expect(datasetItem).to.have.property('saveStatus').to.be.a('string').to.be.eq('new');
                expect(datasetItem).to.have.property('updateToken').to.be.a('string').to.be.eq('');
                expect(datasetItem).to.have.property('loadData').to.be.a('boolean').to.be.eq(false);
                
                expect(datasetItem).to.have.property('charts').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('metrics').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('formulas').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('buckets').to.be.a('array').to.has.lengthOf(0);
                expect(datasetItem).to.have.property('transformations').to.be.a('array').to.has.lengthOf(0);
            }

            definitions.basicDefinition.definitionItem = { ...definitionItem };
            return Promise.resolve();
        });

        it('It should delete a deployment definition', async function () {

            let definitionItem = await DefinitionService.deleteDefinition(definitions.basicDefinition.definitionId, params);

            expect(definitionItem).to.be.a('object');
            expect(definitionItem).to.has.property('message').be.a('string').to.be.eq('Deleted succesfully');

            return Promise.resolve();
        });
    });

    after(async function () {
        await adminItem.deleteAll(params);
        return Promise.resolve();
    });
});