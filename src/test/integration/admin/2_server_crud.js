'use strict';
const { expect } = require('chai');
const { DateTime } = require('luxon');

const {
    authentication: Authentication,
} = require('../../../lib/adminCenter');

const {
    serverService: ServerService,
} = require('../../../lib/contentDeployment');

const {
    GENERAL_ADMIN_USER_USERNAME,
    GENERAL_ADMIN_USER_PASSWORD,
    SERVERS,
} = require('../../config/data');

let token = undefined; // Authorization token

describe('Admin center', function () {
    this.timeout(1000 * 6 * 10 * 15);
    const now = DateTime.now();

    before(async function () {
        let authenticationResponse = await Authentication.login(GENERAL_ADMIN_USER_USERNAME, GENERAL_ADMIN_USER_PASSWORD);
        token = `jwt ${authenticationResponse.token}`;
    });

    describe('Server CRUD', function () {

        let servers = {
            server1: {
                name: `Server 1 - TEST: ${now.toMillis()}`,
                composerDomain: `http://${now.toMillis()}.qrvey.com`,
                apiKey: SERVERS.general.apiKey,
                serverId: undefined,
            },
        };

        it('It should create a server', async function () {
            let serverDetails = {
                apiKey: servers.server1.apiKey,
                host: servers.server1.composerDomain,
                name: servers.server1.name,
            };

            let response = await ServerService.createServer(serverDetails, {
                authorization: token,
            });

            expect(response).to.be.a('object');
            expect(response).to.has.property('adminserverid').be.a('string');
            expect(response).to.has.property('name').be.a('string').to.be.eq(servers.server1.name);
            expect(response).to.has.property('host').be.a('string').to.be.eq(servers.server1.composerDomain);
            expect(response).to.has.property('apiKey').be.a('string').to.be.eq(servers.server1.apiKey);

            servers.server1.serverId = response.adminserverid;
            return Promise.resolve();
        });

        it('It should get a server', async function () {
            let response = await ServerService.getServer(servers.server1.serverId, {
                authorization: token,
            });

            expect(response).to.be.a('array');
            expect(response[0]).to.be.a('Object');
            expect(response[0]).to.has.property('adminserverid').be.a('string').to.be.eq(servers.server1.serverId);
            expect(response[0]).to.has.property('name').be.a('string').to.be.eq(servers.server1.name);
            expect(response[0]).to.has.property('host').be.a('string').to.be.eq(servers.server1.composerDomain);
            expect(response[0]).to.has.property('apiKey').be.a('string').to.be.eq(servers.server1.apiKey);

            return Promise.resolve();
        });

        it('It should get a list of servers', async function () {
            let response = await ServerService.getServers({
                authorization: token,
            });

            expect(response).to.be.a('array');
            expect(response[0]).to.be.a('Object');
            expect(response[0]).to.has.property('adminserverid').be.a('string');

            return Promise.resolve();
        });

        it('It should delete a server', async function () {
            let response = await ServerService.deleteServer(servers.server1.serverId, {
                authorization: token,
            });

            expect(response).to.be.a('object');

            return Promise.resolve();
        });
    });
});